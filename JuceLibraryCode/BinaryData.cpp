/* ==================================== JUCER_BINARY_RESOURCE ====================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

namespace BinaryData
{

//================== OVERVIEW.md ==================
static const unsigned char temp_binary_data_0[] =
"#### Heirarchy\n"
"\n"
"- PluginProcessor\n"
"\t- SourceManager\n"
"\t\t- WaveSource\n"
"\t\t\t- ::StableWaveSource\n"
"\t\t\t\t- ::SmoothWaveSource\n"
"\t\t\t- UserInputMod\n"
"\t\t\t- WaveSourceVoice\n"
"\t\t\t\t- ModulationEndpoint\n"
"\t\t\t\t\n"
"\t- RoutingManager\n"
"\t\t- RoutingSet\n"
"\t\t\t- ModulationRouting\n"
"\t\t- TCParameterManager\n"
"\t\t\t- TCParameter\n"
"\t\t\t- ModulationTarget\n"
"\t\t- ModBundleCatergories\n"
"\n"
"\t- ModifierManager\n"
"\t\t- Modifier\n"
"\t\t\t- ::ADSR\n"
"\t\t\t- ::LFO\n"
"\t\t\t\t- ::LFOSequence\n"
"\t\t\t- ModSampleSource\n"
"\t\t\t\t- ::ModChain\n"
"\t\t\t\t- ModLocation\n"
"\t\t\t\t- ReadonlyModBuffer";

const char* OVERVIEW_md = (const char*) temp_binary_data_0;


const char* getNamedResource (const char* resourceNameUTF8, int& numBytes)
{
    unsigned int hash = 0;

    if (resourceNameUTF8 != nullptr)
        while (*resourceNameUTF8 != 0)
            hash = 31 * hash + (unsigned int) *resourceNameUTF8++;

    switch (hash)
    {
        case 0x85b0761d:  numBytes = 482; return OVERVIEW_md;
        default: break;
    }

    numBytes = 0;
    return nullptr;
}

const char* namedResourceList[] =
{
    "OVERVIEW_md"
};

const char* originalFilenames[] =
{
    "OVERVIEW.md"
};

const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8)
{
    for (unsigned int i = 0; i < (sizeof (namedResourceList) / sizeof (namedResourceList[0])); ++i)
    {
        if (namedResourceList[i] == resourceNameUTF8)
            return originalFilenames[i];
    }

    return nullptr;
}

}
