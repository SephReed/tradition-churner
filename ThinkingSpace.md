Voice
	has mirrors -> voice num mods

Source Manager
	holds voice num mods

Voice updates mods
	by observing -> source man mods array for it's voice num




----------


The Modifier can create ui elements and ModSampleSources
ModSampleSources can be mirrored or cloned





----------

+- Synth
	+- All
		+- Amps
		+- Freq
		+- C1
	+- [1] SmoothWave
		+- Amps
		+- C1
+- FX
	+- [1] LPF
		+- Freq



-----------

The voice asks the source manager for a mod chain, using it's voice number and synth
The source manager creates a mod chain made up of any "all" modchains and any modchains for that synth number
  - Sometimes a modchain can separate synthwide effects to be later (for effeciency)
It does not need to listen for changes because the source manager will update the mod chain for it


-----
Targets register with a string id made up of slashes (ie source/amp)
- They also may choose to optionally separate "all source" modulation from being included in the "per voice" mod chains




-----
If readonly, get a pointer to the topmost cache with mirrors
If not readonly, allocate a vector then pass it by reference to the topmost cache with mirrors, and copy the values in.

