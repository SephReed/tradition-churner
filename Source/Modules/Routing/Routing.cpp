#include "RoutingManager.h"
#include "ModulationRouting.h"
#include "UserInput/ModulationTarget.h"
#include <assert.h>
#include <vector>

#include "../../Components/ComponentStyler.h"
using namespace ComponentStyling;
using namespace std;



// /************** MANAGER **************************/
#include "UserInput/TCParameterManager.h"
#include "../Modifier/ModSampleSource/KnobSampleSource.h"

RoutingManager::RoutingManager(PluginProcessor* plugin)
: plugin(plugin),
  routingSets {*this, *this, *this, *this, *this, *this}
{
	for (auto& modTarget : TCParameterManager::modTargets()) {
		auto target = modTarget.second;
		if (targets.count(string(target->legacyId)) == 0) {
			addTarget(target);
		}
		cout << "Target: " << target->fullname << "\n";
	}
}

void RoutingManager::addTargets(vector<ModulationTarget*> targets) {
	for (auto target : targets) { addTarget(target); }
}

void RoutingManager::addTarget(const ModulationTarget* target) {
		// Targets should only be added once
	if (targets.count(string(target->legacyId)) != 0) { assert(false); }
	targets[string(target->legacyId)] = target;
}

void RoutingManager::updateVoiceCount(int newCount) {
	voiceMods.updateVoiceCount(newCount);
}

ModulationRouting* RoutingManager::createRouting(Modifier* mod, string targetId, bool perVoice, int synthNum) {
	auto target = targets[targetId];
	assert(target != NULL && mod != NULL);
	return createRouting(mod, target, perVoice, synthNum);
}

ModulationRouting* RoutingManager::createRouting(Modifier* mod, const ModulationTarget* target, bool perVoice, int synthNum) {
	// This is now deprecated.  Update an existing route.
	assert(false);
	return NULL;
}

void RoutingManager::addModToLocation(ModulationRouting* route, ModLocation loc) {
	addRemoveModAtLocation(route, loc, true);
}
void RoutingManager::removeModFromLocation(ModulationRouting* route, ModLocation loc) {
	addRemoveModAtLocation(route, loc, false);
}


void RoutingManager::addRemoveModAtLocation(ModulationRouting* route, ModLocation loc, bool addNotRemove) {
	ModBundle* targetBundle = getBundle(loc);

	auto targetId = string(loc.target->legacyId);
	auto updateBundle = [&](ModBundle& bundle){ addNotRemove ? (
		bundle.getModChain(targetId)->acknowledgeRoute(route)
	) : (
		bundle.getModChain(targetId)->forgetRoute(route)
	);};

	if (targetBundle != NULL) {
		updateBundle(*targetBundle);
	} else {
		std::vector<ModBundle>* bundles;
		if(loc.type == ModLocationType::VOICE_TOGETHER_FOR_VOICENUM) {
			bundles = &(voiceMods.byVoiceNum);
		} else if (loc.type == ModLocationType::VOICE_ISOLATED) {
			assert(loc.synthNum != -1);
			bundles = &(voiceMods.perSynthVoice[loc.synthNum]);
		} else {
			assert(false);
		}
		
		for (auto &bundle : *bundles) {
			updateBundle(bundle);
		}
	} 
}

ModBundle* RoutingManager::getBundle(ModLocation loc) {
	// Make sure to check this yourself before asking for a bundle
	assert(loc.specifiesAModChain());

	ModBundle* targetBundle;
	auto type = loc.type;
	switch (type) {
		case ModLocationType::MASTER_ALL: targetBundle = &(masterMods.all);  break;
		case ModLocationType::VOICE_ALL_TOGETHER: targetBundle = &(voiceMods.all);  break;
		case ModLocationType::VOICE_TOGETHER_PER_SYNTH: targetBundle = &(voiceMods.perSynthIsolated[loc.synthNum]);  break;
		case ModLocationType::MASTER_PER_SYNTH:
		case ModLocationType::VOICE_TOGETHER_FOR_VOICENUM:
		case ModLocationType::VOICE_ISOLATED:
		default: targetBundle = NULL;
	}

	if (targetBundle != NULL) {
		return targetBundle;

	} else {
		auto synthNum = loc.synthNum;
		auto voiceNum = loc.voiceNum;
		if(type == ModLocationType::MASTER_PER_SYNTH) {
			return (synthNum != -1) ? &masterMods.perSynth[synthNum] : NULL;

		} else if(type == ModLocationType::VOICE_TOGETHER_FOR_VOICENUM) {
			return (voiceNum != -1) ? &voiceMods.byVoiceNum[voiceNum] : NULL;

		} else if(type == ModLocationType::VOICE_ISOLATED) {
			return (synthNum != -1 && voiceNum != -1) ? &voiceMods.perSynthVoice[synthNum][voiceNum] : NULL;
		}
		assert(false);
		return NULL;
	}
}

void RoutingManager::registerUserInputMods(vector<UserInputMod*> userInputs) {
	for (auto& userInput : userInputs) {
		registerUserInputMod(userInput);
	}
}

void RoutingManager::registerUserInputMod(UserInputMod* userInput) {
	auto bundle = getBundle(userInput->modChainLoc);
	assert(bundle);
	// TODO: make this removable
	bundle->getModChain(string(userInput->target.legacyId))->addSampleSource(
		make_unique<KnobSampleSource>(userInput)
	);

	auto modLoc = userInput->modChainLoc;
	if (modLoc.synthNum >= 0 && modLoc.target->ideControl) {
		auto ideParam = parameterManager.getSourceParam(modLoc.synthNum, modLoc.target);
		assert(ideParam != NULL);
		userInput->commandParameter(ideParam);
	}
}


void RoutingManager::registerEndpoints(vector<ModulationEndpoint*> endpoints) {
	for (auto endpoint : endpoints) { registerEndpoint(endpoint); }
}

void RoutingManager::registerEndpoint(ModulationEndpoint* endpoint) {
	auto bundle = getBundle(endpoint->modChainLoc);
	assert(bundle);
	auto targetId = string(endpoint->target->legacyId);
	assert(targets[targetId]);  // Make sure to register your targets (add them to a modTarget.list)
	endpoint->addSampleSource(bundle->getModChain(targetId)->createMirror());
//	targets[targetId]->endpointCount++;
}

// TODO: finish this
void RoutingManager::unregisterEndpoint(ModulationEndpoint* endpoint) {
//	auto bundle = getBundle(endpoint->modChainLoc);
//	auto targetId = endpoint->target->id;
//	endpoint.removeSampleSource(bundle.get(targetId).createMirror());
//	targets[targetId]->endpointCount--;
}


void RoutingManager::resetModChainCaches() {
	masterMods.resetCaches();
	voiceMods.resetCaches();
}


// Retrigger/Release Voice Mod

void RoutingManager::retriggerVoiceModChains(int voiceNum) {
	retriggerVoicelessModChains();
	updateVoiceModChains(voiceNum, true);
}

void RoutingManager::releaseVoiceModChains(int voiceNum) {
	updateVoiceModChains(voiceNum, false);
}

void RoutingManager::updateVoiceModChains(int voiceNum, bool retriggerNotRelease) {
	if (voiceNum > voiceMods.byVoiceNum.size()) {
		assert(retriggerNotRelease == false);
		return;
	}

	auto update = [&](ModBundle bundle) {
		if (retriggerNotRelease) { bundle.retrigger(); } 
		else { bundle.release(); }
	};

	update(voiceMods.byVoiceNum[voiceNum]);
	for (int s = 0; s < Globals::NUM_SOURCES; s++) {
		update(voiceMods.perSynthVoice[s][voiceNum]);
	}
}

// Retrigger/Release Voiceless Mods

void RoutingManager::retriggerVoicelessModChains() {
	updateVoicelessModChains(true);
}

void RoutingManager::releaseVoicelessModChains() {
	updateVoicelessModChains(false);
}

void RoutingManager::updateVoicelessModChains(bool retriggerNotRelease) {
	auto update = [&](ModBundle bundle) {
		if (retriggerNotRelease) { bundle.retrigger(); } 
		else { bundle.release(); }
	};

	update(masterMods.all);
	update(voiceMods.all);
	for (int s = 0; s < Globals::NUM_SOURCES; s++) {
		update(masterMods.perSynth[s]);
		update(voiceMods.perSynth[s]);
		update(voiceMods.perSynthIsolated[s]);
	}
}


// /************** ROUTING SET **************************/

RoutingSet::RoutingSet(RoutingManager& routeMgmt): routingManager(routeMgmt), mod(NULL) {
	for (int i = 0; i < NUM_ROUTES; i++) {
		push_back(new ModulationRouting(*this));
	}
}

void RoutingSet::setModifier(Modifier* i_mod) {
	mod.set(i_mod);
	// for (auto& route : *this) {
	// 	route->mod.set(mod);
	// }
}



// /************** ROUTING **************************/

ModulationRouting::ModulationRouting(
	RoutingSet& routingSet,
	const ModulationTarget* i_target,
	bool i_perVoice,
	int i_synthNum
) :	routingSet(routingSet),
		target(i_target),
		perVoice(i_perVoice),
		synthNum(i_synthNum)
{
	// if (target.get()) {
	// 	range = target.get()->defaultModRange;
	// 	direction = target.get()->defaultModDirection;
	// }
	auto updateLoc = [&](){ updateModLocation(); };
	perVoice.onChange(updateLoc);
	synthNum.onChange(updateLoc);
	target.onChange(updateLoc);

	target.observe([&](auto newTarget){
		if (newTarget) {
			range = newTarget->defaultModRange;
			direction = newTarget->defaultModDirection;
		} else {
			range = 1;
			direction = ModDirection::OUT;
		}
	});
	routingSet.mod.onChange([&](){ updateModLocation(true); });
	updateLoc();
}

// unique_ptr<ModSampleSource> ModulationRouting::createModSource() {
// 	auto modifier = mod.get();
// 	// modifier for a routing must be set before getting a sample source
// 	assert(modifier);
// 	return modifier->createModSource(*this);
// }

void ModulationRouting::updateModLocation(bool force) {
	auto shouldBeLocation = findCorrectModLocation();
	if (shouldBeLocation == currentModLocation && force == false) { return; }

	auto& routeMgmt = routingSet.routingManager;
	if (currentModLocation.specifiesAModChain()) {
		routeMgmt.removeModFromLocation(this, currentModLocation);
	}
	currentModLocation = shouldBeLocation;
	if (currentModLocation.specifiesAModChain()) {
		routeMgmt.addModToLocation(this, currentModLocation);
	}
}

ModLocation ModulationRouting::findCorrectModLocation() {
	if (target.get() == NULL) { return ModLocationType::UNSET; }

	ModLocationType type;
	if (isPerSynth()) {
		if (isPerVoice()) {
			type = ModLocationType::VOICE_ISOLATED;
		} else if (target.get()->optimizePerSynth) {
			type = ModLocationType::MASTER_PER_SYNTH;
		} else {
			type = ModLocationType::VOICE_TOGETHER_PER_SYNTH;
		}

	} else {
		if (isPerVoice()) {
			type = ModLocationType::VOICE_TOGETHER_FOR_VOICENUM;
		} else if (target.get()->optimizeForAllSynth) {
			type = ModLocationType::MASTER_ALL;
		} else {
			type = ModLocationType::VOICE_ALL_TOGETHER;
		}
	}
	return ModLocation{type, target.get(), synthNum.get()};
}

Modifier* ModulationRouting::getMod() {
	return  routingSet.mod.get();
}

float ModulationRouting::getScaledRange() {
	return range * routingSet.scaleAll.get();
}

bool ModulationRouting::canBeIgnored() { 
	return range == 0 || routingSet.scaleAll.get() == 0; 
}


// /************** USER INPUT MODULATION **************************/
#import "../WaveSource/WaveSource.h"
#import "../../PluginProcessor.h"

UserInputMod::UserInputMod(
	const ModulationTarget& target,
	WaveSource& source
) : UserInputMod(
	target,
	ModLocation(ModLocationType::VOICE_TOGETHER_PER_SYNTH, &target, source.sourceNum),
	source.plugin->routingManager
){}

UserInputMod::UserInputMod(
	const ModulationTarget& target, 
	ModLocation modChainLoc,
	RoutingManager& routeMgmt
): UserInputMod (
	target,
	modChainLoc,
	[&](){ routeMgmt.registerUserInputMod(this); },
	[](){ }
){}


// target(target), 
// 	modChainLoc(modChainLoc),
// 	Observable<double>(target.defaultValue),
// 	routeMgmt(routeMgmt)
// {
// 	// routeMgmt.registerUserInputMod(this);
// }

UserInputMod::UserInputMod(
	const ModulationTarget& target, 
	ModLocation modChainLoc,
	function<void()> registerSelf,
	function<void()> unregisterSelf
): RangedUserInput(target),
// Observable<double>(target.defaultValue),
	target(target),
	modChainLoc(modChainLoc),
	registerSelf(registerSelf),
	unregisterSelf(unregisterSelf)
{
	// routeMgmt.registerUserInputMod(this);
	registerSelf();
}

// unique_ptr<Slider> UserInputMod::createRotary() {
// 	return make_unique<ModSlider>(this, UserInputModType::ROTARY);
// }

ModulationEndpoint UserInputMod::createEndpoint(int voiceNum) {
	return ModulationEndpoint(&target, modChainLoc.synthNum, voiceNum);
}





// /************** MODULATION ENDPOINT **************************/

ModulationEndpoint::ModulationEndpoint(const ModulationTarget* target, int synthNum, int voiceNum, bool alsoCreateUserInput)
: ModulationEndpoint(
		target, 
		ModLocation(ModLocationType::VOICE_ISOLATED, target, synthNum, voiceNum),
	  alsoCreateUserInput
	)
{}

ModulationEndpoint::ModulationEndpoint(
	const ModulationTarget* target, 
	ModLocation i_modChainLoc, 
  bool alsoCreateUserInput
):target(target), modChainLoc(i_modChainLoc) {
	modChainLoc.target = target;
	allowMirror = false;
	debugName = string(target->fullname) + " Endpoint";
	if (alsoCreateUserInput) {
//		assert(false);  // fix this
		userInput = make_unique<UserInputMod>(*target, modChainLoc, [](){}, [](){});
		addSampleSource(make_unique<KnobSampleSource>(userInput.get()));
	}
}




//************** TARGET CHOOSER **************************/
vector<const ModulationTarget*> TargetChooser::indexToTarget() {
  static auto _idToTarget = new vector<const ModulationTarget*>;
  if (_idToTarget->size() == 0) {
  	for (auto& target : TCParameterManager::modTargets()) {
			_idToTarget->push_back(target.second);
		}
  }
  return *_idToTarget;
}

map<string_view, int>& TargetChooser::targetIdToIndex() {
	static auto out = new map<string_view, int>;
  if (out->size() == 0) {
  	int index = 0;
  	for (auto& target : TCParameterManager::modTargets()) {
			(*out)[target.second->legacyId] = index++;
		}
  }
  return *out;
}

TargetChooser::TargetChooser(): ComboBox("TargetChooser") {
	int uselessId = 1;
	for (auto& target : TCParameterManager::modTargets()) {
		addItem(string(target.second->fullname), uselessId++);
	}
}

const ModulationTarget* TargetChooser::getSelectedTarget() {
	auto index = getSelectedItemIndex();
	if (index == -1) { return NULL; }
	return TargetChooser::indexToTarget()[index];
}




// /************** ROUTING EDITOR **************************/
RoutingEditor::RoutingEditor(ModulationRouting& routing) : routing(routing) {
	addAndMakeVisible(modTarget);
	addAndMakeVisible(perVoice);

	modTarget.onChange = [&](){
		routing.target.set(modTarget.getSelectedTarget());
	};
	routing.target.observe([&](auto newTarget){
		if (newTarget == NULL) {
			modTarget.setSelectedId(-1, NotificationType::dontSendNotification);
		} else {
			auto index = TargetChooser::targetIdToIndex()[newTarget->legacyId];
			modTarget.setSelectedItemIndex(index, NotificationType::dontSendNotification);
		}
	});

	perVoice.onClick = [&](){
		routing.perVoice.set(perVoice.getToggleState());
	};
	routing.perVoice.observe([&](bool isPerVoice){
		perVoice.setToggleState(isPerVoice, NotificationType::dontSendNotification);
	});
}

void RoutingEditor::resized() {
	modTarget.setBounds(0, 0, 100, getHeight());
	perVoice.setBounds(100, 0, getHeight(), getHeight());
}
