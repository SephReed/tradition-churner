#pragma once
#include <map>
#include <assert.h>
#include "../../../Globals.h"
#include "TCParameter.h"

using namespace std;

class TCParameterManager {
public:
	static inline map<string, const ModulationTarget*>& modTargets() {
		static auto staticModTargets = new map<string, const ModulationTarget*>();
 		return *staticModTargets;
	}

	static inline void addModTargetList(vector<const ModulationTarget*> addUs) {
		for (auto& addMe : addUs) {
			// all targets must have legacyId set
			auto id = string(addMe->legacyId);
			assert(id.size());
			modTargets()[id] = addMe;
		}
	}


	struct ParameterInfo {
		const ModulationTarget* target;
		TCParameter* param;
		int endpointCount = 0;
	};

	vector<map<string, ParameterInfo>> sourceParamSets {Globals::NUM_SOURCES};

	TCParameterManager() {
		for (int s = 0; s < sourceParamSets.size(); s++) {
			auto& sourceParamSet = sourceParamSets[s];
			for (auto& modTarget : TCParameterManager::modTargets()) {
				auto target = modTarget.second;
				if(target->ideControl) {
					cout << "Target: " << target->fullname << "\n";
					sourceParamSet[string(target->legacyId)] = {
						target, 
						new TCParameter(target, "S" + to_string(s+1))
					};
				}
			}
		}
	}

	TCParameter* getSourceParam(int sourceNum, const ModulationTarget* target) {
		assert(target);
		return getSourceParam(sourceNum, string(target->legacyId));
	}

	TCParameter* getSourceParam(int sourceNum, string legacyId) {
		return sourceParamSets[sourceNum][legacyId].param;
	}


	void forEachParameter(function<void(TCParameter*)> cb) {
		for (auto& sourceParameSet : sourceParamSets) {
			for(auto& entry : sourceParameSet) {
				cb(entry.second.param);
			}
		}
	}
};
