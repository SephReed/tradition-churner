#pragma once

//************** USER INPUT MOD EDITOR **************************/
#import "TCParameter.h"
#import "../../RangedUserInput/RangedUserInput.h"
#import "../../RangedUserInput/RangeEditor.h"


//************** USER INPUT MOD **************************/
#import "../RoutingManager.h"

class WaveSource;

class UserInputMod : public RangedUserInput {
public:
	const ModulationTarget& target;
	ModLocation modChainLoc;
	function<void()> registerSelf;
	function<void()> unregisterSelf;
	TCParameter* ideParam = NULL;

	UserInputMod(
		const ModulationTarget& target,
		WaveSource& source
	);

	UserInputMod(
		const ModulationTarget& target,
		ModLocation modChainLoc,
		RoutingManager& routeMgmt
	);

	UserInputMod(
		const ModulationTarget& target, 
		ModLocation modChainLoc,
		function<void()> registerSelf,
		function<void()> unregisterSelf
	);

	const ModulationTarget* getTarget() {
		return modChainLoc.target;
	}

	// unique_ptr<Slider> createRotary();
	ModulationEndpoint createEndpoint(int voiceNum);
	void commandParameter(TCParameter* i_ideParam) {
		assert(ideParam == NULL && modChainLoc.target->ideControl);
		ideParam = i_ideParam;
		ideParam->value.onChange([&](){
			set(ideParam->getRangedValue());
		}, true);
		onChange([&]() {
			ideParam->setRangedValueNotifyingHost(get());
		});
	}

	unique_ptr<Slider> createRotary() final;
};


//************** USER INPUT MOD EDITOR **************************/

class UserInputModEditor: public RangeEditor {
public:
	UserInputMod* userInputMod;
	UserInputModEditor(UserInputMod* userInput, UserInputModType type)
	: RangeEditor(userInput, type),
		userInputMod(userInput)
	{}

	virtual void startedDragging() {
		if (userInputMod->ideParam) {
			userInputMod->ideParam->beginChangeGesture();
		}
	}

	virtual void stoppedDragging() {
		if (userInputMod->ideParam) {
			userInputMod->ideParam->endChangeGesture();
		}
	}
};





//************** RESOLVE CIRCULARITY **************************/

inline unique_ptr<Slider> UserInputMod::createRotary() {
	return make_unique<UserInputModEditor>(this, UserInputModType::ROTARY);
}
