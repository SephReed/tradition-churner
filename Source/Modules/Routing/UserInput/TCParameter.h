#pragma once
#include <string>
#include "../../../Util/Observable.h"
#include "../../../../JuceLibraryCode/JuceHeader.h"

class TCParameter : public AudioProcessorParameter {
public:
//	UserInputMod& uiMod;
//	TCParameter(UserInputMod& uiMod): uiMod(uiMod) {}

	float range;
	const ModulationTarget* target;
	Observable<float> value;
	String prefix;
	
	TCParameter(const ModulationTarget* target, String prefix = "")
	:	value(0),
    range(target->max - target->min),
		target(target),
    prefix(prefix)
	{
		setRangedValueNotifyingHost(target->defaultValue);
	}

	float getValue() const override {
		return value.get();
	}

	float getRangedValue() const {
		return denormalize(value.get());
	}
	
	void setValue(float newValue) override {
		value.set(newValue);
	}
	
	float getDefaultValue() const override {
		return normalize(target->defaultValue);
	}
	
	juce::String getName(int maximumStringLength) const override {
		auto prefixLength = prefix.length() + 2;
		auto name = (target->fullname.length() + prefixLength < maximumStringLength) ? target->fullname : target->shortname;
		return prefix + "::" + String(&name[0], name.length());
	}
	
	juce::String getLabel() const override {
		return "";
	}
	
	float getValueForText(const juce::String &text) const override {
		assert(false);
		return 0;
	}

	float normalize(float value) const {
		return (value - target->min)/range;
	}

	float denormalize(float value) const {
		return (value * range) + target->min;
	}

	void setRangedValueNotifyingHost(float newValue) {
		setValueNotifyingHost(normalize(newValue));
	}
	
};
