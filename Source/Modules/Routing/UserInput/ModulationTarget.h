#pragma once
#include <string>
using namespace std;

#include "../../Modifier/ModRangeAndDirection.h"
#include "../../../MasterEnums.h"
#include "../ModDirection.h"
#include "../../RangedUserInput/RangeParams.h"


struct ModulationTarget: public RangeParams {
	int idNum = 0;
	bool ideControl = true;
	bool inactive = false;
	bool canBePerSynth = true;
	bool canBePerVoice = false;
	bool optimizePerSynth = false;
	bool optimizeForAllSynth = false;
	
	// int endpointCount = 0;

	ModDirection defaultModDirection = ModDirection::OUT;
	double defaultModRange = 1;
};
