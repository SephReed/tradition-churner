#pragma once
using namespace std;

//************** ROUTING SET **************************/
#import <vector>
#import "ModulationRouting.h"
#import "../../Util/DesignatedInit.h"
#import "../RangedUserInput/RangedUserInput.h"
class RoutingManager;

class RoutingSet: public vector<ModulationRouting*> {
public:
	static constexpr int NUM_ROUTES = 6;
	static inline RangeParams scaleAllParams = DesignatedInit(ModulationTarget,
    $.legacyId = "ALL_ROUTES_SCALE",
    $.fullname = "Modifier All Routes Scale",
    $.shortname = "Scale",
    $.min = 0,
    $.baseline = 1,
    $.defaultValue = 1,
    $.max = 2
  );

	Observable<Modifier*> mod;
	RangedUserInput scaleAll {RoutingSet::scaleAllParams};
	RoutingManager& routingManager;
	
	RoutingSet(RoutingManager& routeMgmt);
	Component* createEditor();
	void setModifier(Modifier* mod);
};


//************** ROUTING SET EDITOR **************************/
#import "../../Util/SectionedRectangle.h"

class RoutingSetEditor: public Component {
public:
	unique_ptr<Slider> scaleAll;
	RoutingSetEditor(RoutingSet& routeSet)
	: routeSet(routeSet),
		scaleAll(routeSet.scaleAll.createRotary())
	{
		addAndMakeVisible(scaleAll.get());
		for (auto& routing : routeSet) {
			routeEditors.push_back(routing->createEditor());
		}
		for (auto& editor : routeEditors) {
			addAndMakeVisible(editor);
		}
	}

	void resized() override {
		SectionedRectangle bounds {getLocalBounds()};
		auto knobsAndList = bounds.splitVerticallyAtRatio(1.0/6.0, 3);
		scaleAll->setBounds(knobsAndList.top);
		auto rowBounds = knobsAndList.bottom.splitRows(routeEditors.size());
		for (int e = 0; e < routeEditors.size(); e++) {
			routeEditors[e]->setBounds(rowBounds[e]);
		}
	}

private:
	RoutingSet& routeSet;
	vector<Component*> routeEditors;
};


//************** ROUTING SET CPP **************************/
inline Component* RoutingSet::createEditor() {
	return new RoutingSetEditor(*this);
}
