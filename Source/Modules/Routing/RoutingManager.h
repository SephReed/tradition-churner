#pragma once
#include <map>
using namespace std;

// //. TODO TODO TODO TODO
// // TODO!!!
// // Make sure different ModTargetId with matching retriggers and source use mirroring


//************** ROUTING MANAGER **************************/
#import <list>
#import "RoutingSet.h"
#import "UserInput/ModulationTarget.h"
#import "ModulationEndpoint.h"
#import "ModChainBundling/ModBundleCategories.h"
#import "UserInput/TCParameterManager.h"


class PluginProcessor;

class RoutingManager {
public:
	// static inline map<string, const ModulationTarget*>& modTargets() {
	// 	static auto staticModTargets = new map<string, const ModulationTarget*>();
 // 		return *staticModTargets;
	// }

	// static inline void addModTargetList(vector<const ModulationTarget*> addUs) {
	// 	for (auto& addMe : addUs) {
	// 		// all targets must have legacyId set
	// 		auto id = string(addMe->legacyId);
	// 		assert(id.size());
	// 		modTargets()[id] = addMe;
	// 	}
	// }

	PluginProcessor* plugin;
	VoiceMods voiceMods;
	MasterMods masterMods;
	TCParameterManager parameterManager;
	
	RoutingManager(PluginProcessor* plugin);

	void addTargets(vector<ModulationTarget*> targets);
	void addTarget(const ModulationTarget* target);
	map<string, const ModulationTarget*> targets;
	// Observable<ModulationTarget*> targetAdded = {NULL};

	void updateVoiceCount(int newCount);

	ModulationRouting* createRouting(Modifier* mod, string target, bool perVoice = false, int synthNum = -1);
	ModulationRouting* createRouting(Modifier* mod, const ModulationTarget* target, bool perVoice = false, int synthNum = -1);

	array<RoutingSet, 6> routingSets;
	// vector<ModulationRouting*> routings;

	void addModToLocation(ModulationRouting* route, ModLocation loc);
	void removeModFromLocation(ModulationRouting* route, ModLocation loc);

	void registerUserInputMods(vector<UserInputMod*> userInputs);
	void registerUserInputMod(UserInputMod* userInput);

	void registerEndpoints(vector<ModulationEndpoint*> endpoints);
	void registerEndpoint(ModulationEndpoint* endpoint);
	void unregisterEndpoint(ModulationEndpoint* endpoint);

	ModBundle* getBundle(ModLocation loc);
	void resetModChainCaches();

	void retriggerVoiceModChains(int voiceNum);
	void releaseVoiceModChains(int voiceNum);
	void releaseVoicelessModChains();

protected:
	void addRemoveModAtLocation(ModulationRouting* route, ModLocation loc, bool addNotRemove);
	void retriggerVoicelessModChains();
	void updateVoiceModChains(int voiceNum, bool retriggerNotRelease);
	void updateVoicelessModChains(bool retriggerNotRelease);
	
};
