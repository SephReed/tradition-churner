#pragma once
using namespace std;


//************** MODULATION BUNDLE **************************/
//. TODO TODO TODO TODO
// TODO!!!
// Make sure different ModTargetId with matching retriggers and source use mirroring
#include <map>
#import "../ModulationRouting.h"
#import "../../../MasterEnums.h"
#import "../../Modifier/ModSampleSource/ModChain.h"

class ModBundle {
public:
	vector<ModBundle*> parentBundles;
	ModLocationType locationType;
	ModBundle(ModLocationType locationType): locationType(locationType){}
	// ModBundle();

	ModChain* getModChain(string targetId) {
		if(modChains.count(targetId) == 0) {
			auto addMe = new ModChain();
			for (const auto& parent: parentBundles) {
				addMe->addSampleSource(parent->getModChain(targetId)->createMirror());
			}
			addMe->debugName = targetId + " " + ModBundle::modLocationTypeToString(locationType);
			modChains[targetId] = addMe;
			modChainList.push_back(addMe);
		}
		return modChains[targetId];
	}

	void retrigger() {
		for (auto const& modChain : modChainList) { modChain->retrigger(); }
	}

	void release() {
		for (auto const& modChain : modChainList) { modChain->release(); }
	}

	void resetCaches() {
		for (auto const& modChain : modChainList) { modChain->resetCaching(); }
	}
	
	static inline string modLocationTypeToString(ModLocationType locationType) {
		switch(locationType) {
			case ModLocationType::MASTER_ALL: return "Master All";
			case ModLocationType::MASTER_PER_SYNTH: return "Master Per Synth";
			
			case ModLocationType::VOICE_ALL_TOGETHER: return "Voice All Together";
			case ModLocationType::VOICE_TOGETHER_FOR_VOICENUM: return "Voice Num Together";
			case ModLocationType::VOICE_TOGETHER_PER_SYNTH: return "Voice Together Per Synth";
			case ModLocationType::VOICE_ISOLATED: return "Voice Isolated";
			default: assert(false); return "";
		}
	}
private:
	// use list for faster iteration
	vector<ModChain*> modChainList;
	map<string, ModChain*> modChains;
};



//************** MODULATION BUNDLE CATEGORY **************************/
struct ModBundleCategory {
	virtual ~ModBundleCategory(){}
	void resetCaches() {
		forEachBundle([](ModBundle& bundle){ bundle.resetCaches(); });
	}
	virtual void forEachBundle(function<void(ModBundle& bundle)> cb) = 0;
};



//************** VOICE MOD BUNDLE CATEGORY **************************/
// voice mods are chained together perSynthVoice->(perSynthIsolated, perVoice);  (perSynth, perVoice) -> allSynthVoices
// these are used for things that must be applied on a per voice basis (ie tune), but can still share modulation curves
//#import "../../WaveSource/SourceManager.h"
#import "../../../Globals.h"
using ModBundleVector = vector<ModBundle>;

struct VoiceMods : public ModBundleCategory {
	ModBundle all {ModLocationType::VOICE_ALL_TOGETHER};
	vector<ModBundle> byVoiceNum;
	// Used to keep allSynth from appearing twice in perSynthVoice
	ModBundleVector perSynthIsolated {Globals::NUM_SOURCES, ModBundle(ModLocationType::VOICE_TOGETHER_PER_SYNTH)};
	// TODO: Make sure perSynth is locked after adding its two mirrors
	ModBundleVector perSynth{Globals::NUM_SOURCES, ModBundle(ModLocationType::VOICE_TOGETHER_PER_SYNTH)};
	vector<ModBundleVector> perSynthVoice {Globals::NUM_SOURCES};
	
	VoiceMods() {
		for (int s = 0; s < Globals::NUM_SOURCES; s++) {
			perSynth[s].parentBundles.push_back(&all);
			perSynth[s].parentBundles.push_back(&perSynthIsolated[s]);
//			perSynth[s].lock();
		}
	}

	void updateVoiceCount(int newCount) {
		byVoiceNum.resize(newCount, ModBundle(ModLocationType::VOICE_TOGETHER_FOR_VOICENUM));
		for (int v = 0; v < byVoiceNum.size(); v++) {
			auto parents = &(byVoiceNum[v].parentBundles);
			if (parents->empty()) {
				parents->push_back(&all);
			}
		}

		for (int s = 0; s < perSynthVoice.size(); s++) {
			auto bundleVector = &(perSynthVoice[s]);
			bundleVector->resize(newCount, ModBundle(ModLocationType::VOICE_ISOLATED));
			for (int v = 0; v < bundleVector->size(); v++) {
				auto parents = &((*bundleVector)[v].parentBundles);
				if (parents->empty()) {
						parents->push_back(&byVoiceNum[v]);
					parents->push_back(&perSynthIsolated[s]);
				}
			}
		}
	}

	void forEachVoicedBundle(function<void(ModBundle& bundle)> cb, int voiceNum = -1) {
		for (int v = 0; v < byVoiceNum.size(); v++) {
			if (voiceNum == -1 || voiceNum == v) {
				cb(byVoiceNum[v]);
			}
		}
		for (auto& bundleVector : perSynthVoice) {
			for (int v = 0; v < bundleVector.size(); v++) {
				if (voiceNum == -1 || voiceNum == v) {
					cb(bundleVector[v]);
				}
			}
		}
	}

	void forEachVoicelessBundle(function<void(ModBundle& bundle)> cb) {
		cb(all);
		for (int s = 0; s < Globals::NUM_SOURCES; s++) {
			cb(perSynthIsolated[s]);
			cb(perSynth[s]);
		}
	}
	
	void forEachBundle(function<void(ModBundle& bundle)> cb) override {
		forEachVoicelessBundle(cb);
		forEachVoicedBundle(cb);
	}
};


//************** MASTER MOD BUNDLE CATEGORY **************************/
// optimized mods are not applied to voices individually, but instead at some other optimal point.
// these are used for things that can be applied after mixing voices or synths (ie amplitude)
struct MasterMods : public ModBundleCategory {
	ModBundleVector perSynth{Globals::NUM_SOURCES, ModBundle(ModLocationType::MASTER_PER_SYNTH)};
	ModBundle all {ModLocationType::MASTER_ALL};
	
	void forEachBundle(function<void(ModBundle& bundle)> cb) override {
		cb(all);
		for (int s = 0; s < Globals::NUM_SOURCES; s++) {
			cb(perSynth[s]);
		}
	}
};



// //************** MASTER MOD BUNDLE CATEGORY **************************/
// // optimized mods are not applied to voices individually, but instead at some other optimal point.
// // these are used for things that can be applied after mixing voices or synths (ie amplitude)
// struct ModifierSources : public ModBundleCategory {
// 	ModBundleVector perVoice{Globals::NUM_SOURCES, ModBundle(ModLocationType::MASTER_PER_SYNTH)};
// 	ModBundle eachNote {ModLocationType::MASTER_ALL};
// };
