#pragma once

#import "../UserInput/ModulationTarget.h"

//************** MOD LOCATION TYPE **************************/
enum class ModLocationType {
	UNSET,

	VOICE_ALL_TOGETHER,
	VOICE_TOGETHER_PER_SYNTH,
	VOICE_TOGETHER_FOR_VOICENUM,
	VOICE_ISOLATED,

	MASTER_ALL,
	MASTER_PER_SYNTH
};


//************** MOD LOCATION **************************/
struct ModLocation {
	ModLocationType type;
	int synthNum;
	int voiceNum;
	const ModulationTarget* target = NULL;

	ModLocation(ModLocationType type = ModLocationType::UNSET, const ModulationTarget* target = NULL, int synthNum = -1, int voiceNum = -1)
	: type(type), synthNum(synthNum), target(target), voiceNum(voiceNum) {}

	bool specifiesAModChain() {
		return type != ModLocationType::UNSET && target != NULL;
	}

	friend bool operator==(const ModLocation& lhs, const ModLocation& rhs) {
	  return (
	  	lhs.type == rhs.type 
	  	&& lhs.synthNum == rhs.synthNum
	  	&& lhs.target == rhs.target
	  	&& lhs.voiceNum == rhs.voiceNum
	  );
	}
};
