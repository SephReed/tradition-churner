#pragma once
using namespace std;

// #include "RoutingManager.h"
#include "UserInput/TCParameterManager.h"
#include "UserInput/ModulationTarget.h"
#include "../../Util/StaticallyRun.h"


template<class CREATOR_TYPE>
class RegisterTargetsInRoutingManager {
public:
  struct StaticRunProxy {
    StaticRunProxy() {
      TCParameterManager::addModTargetList(CREATOR_TYPE::modTargetList());
    }
  };

  static StaticRunProxy staticProxy;
  typedef ForceStaticProxyInit<StaticRunProxy&, staticProxy> __dummy;
};

template<class CREATOR_TYPE>
typename RegisterTargetsInRoutingManager<CREATOR_TYPE>::StaticRunProxy RegisterTargetsInRoutingManager<CREATOR_TYPE>::staticProxy;

