#pragma once






//************** MODULATION ROUTING **************************/
#include "UserInput/ModulationTarget.h"
#include "ModChainBundling/ModLocation.h"
#include "../Modifier/Modifier.h"
#include "../../../JuceLibraryCode/JuceHeader.h"
#include "../Modifier/ModSampleSource/ModSampleSource.h"
#include "ModDirection.h"

class RoutingSet;
class Modifier;
class ModSampleSource;

class ModulationRouting {
public:
	RoutingSet& routingSet;
	float range {1};
	ModDirection direction {ModDirection::UP};
	// Observable<Modifier*> mod;
	Observable<const ModulationTarget*> target;
	Observable<bool> perVoice;
	Observable<int> synthNum;

	ModulationRouting(
		RoutingSet& routingSet, 
		const ModulationTarget* target = NULL,
		bool perVoice = false,
		int synthNum = -1
	);

	unique_ptr<ModSampleSource> createModSource();

	void updateModLocation(bool force = false);

	bool isPerVoice() { return perVoice.get(); }
	bool isPerSynth() { return synthNum.get() != -1; }
	bool canBeIgnored();
	float getScaledRange();
	Modifier* getMod();

	Component* createEditor();

private:
	ModLocation findCorrectModLocation();
	ModLocation currentModLocation;
};


//************** ROUTING SAMPLE SOURCE **************************/
#include "../Modifier/ModSampleSource/RangedModSampleSource.h"

class RoutingSampleSource : public RangedModSampleSource {
public:
	ModulationRouting& routing;
	RoutingSampleSource(ModulationRouting& routing)
	: RangedModSampleSource(routing.getMod()->createModSource()),
		routing(routing)
	{}

	void scaleSamples(int numSamples, float* modMe) override {
		auto range = routing.getScaledRange();
		auto dir = routing.direction;
		float offset = 0;

		if (dir == ModDirection::DOWN) {
			range *= -1;
		} else if (dir == ModDirection::OUT) {
			offset = range / 2;
		}

		for (int s = 0; s < numSamples; s++) {
			modMe[s] *= range;
			if (offset) { modMe[s] -= offset; }
		}
	}

	bool canBeIgnored() override { return routing.canBeIgnored() || RangedModSampleSource::canBeIgnored(); } 
};


//************** TARGET CHOOSER **************************/
#import <map>

class TargetChooser : public ComboBox {
public:
	static vector<const ModulationTarget*> indexToTarget();
	static map<string_view, int>& targetIdToIndex();
	TargetChooser();
	const ModulationTarget* getSelectedTarget();
};


//************** MODULATION ROUTING EDITOR **************************/
#include "../../MasterEnums.h"
#include "../../Components/BacklitButton.h"

class RoutingEditor : public Component {
public:
	TargetChooser modTarget;
	BacklitButton perVoice;

	RoutingEditor(ModulationRouting& routing);
	void resized() override;

private:
	ModulationRouting& routing;
};



//************** MODULATION ROUTING CPP **************************/
inline Component* ModulationRouting::createEditor() { return new RoutingEditor(*this); }

inline unique_ptr<ModSampleSource> ModulationRouting::createModSource() { return make_unique<RoutingSampleSource>(*this); }

