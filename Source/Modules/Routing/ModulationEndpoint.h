#pragma once
using namespace std;


//************** MODULATION ENDPOINT **************************/
#include "UserInput/ModulationTarget.h"
#include "ModulationRouting.h"
#include "../../Util/DesignatedInit.h"
#include "../Modifier/ModSampleSource/ModChain.h"

class UserInputMod;

class ModulationEndpoint
	: public ModChain
{
public:
	const ModulationTarget* target;
	ModLocation modChainLoc;
	unique_ptr<UserInputMod> userInput;

	ModulationEndpoint(
		const ModulationTarget* target, 
		ModLocation modChainLoc = DesignatedInit(ModLocation,
			$.type = ModLocationType::VOICE_ISOLATED
	  ), 
	  bool alsoCreateUserInput = false
	);
	
	ModulationEndpoint(const ModulationTarget* target, int synthNum, int voiceNum = -1, bool alsoCreateUserInput = false);
};


#import "UserInput/UserInputMod.h"
