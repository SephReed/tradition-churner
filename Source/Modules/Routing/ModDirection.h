#pragma once

enum class ModDirection {
	UP,
	DOWN,
	OUT,
};