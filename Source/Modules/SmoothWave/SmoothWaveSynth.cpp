//#include "SmoothWaveSynth.h"
//#include "SmoothWaveEditor.h"
//#include "SmoothWaveVoice.h"
//
//
//SmoothWaveSynth::SmoothWaveSynth() {
//	for (auto i = 0; i < numVoices; ++i) {
//		addVoice(new SmoothWaveVoice(*this, i));
//	}
//	addSound(new SmoothWaveSound());
//	voiceStolenFade.clear();
//}
//
//
//void SmoothWaveSynth::setTranspose(float i_transpose) {
//	for (auto i = 0; i < numVoices; ++i) {
//		auto voice = dynamic_cast<SmoothWaveVoice*> (getVoice(i));
//		voice->waveAngleDeltasDirty = true;
//	}
//	transpose = i_transpose;
//}
//
//void SmoothWaveSynth::setDetune(float i_detune) {
//	for (auto i = 0; i < numVoices; ++i) {
//		auto voice = dynamic_cast<SmoothWaveVoice*> (getVoice(i));
//		voice->waveAngleDeltasDirty = true;
//	}
//	detune = i_detune;
//}
//
//CurvedADSR::Parameters SmoothWaveSynth::getGainEnvParams() { return gainEnvParams; }
//void SmoothWaveSynth::setGainEnvParams(CurvedADSR::Parameters params) {
//	for (auto i = 0; i < numVoices; ++i) {
//		auto voice = dynamic_cast<SmoothWaveVoice*> (getVoice(i));
//		voice->gainEnvDirty = true;
//	}
//	gainEnvParams = params;
//}
//
//CurvedADSR::Parameters SmoothWaveSynth::getFilterEnvParams() { return filterEnvParams; }
//void SmoothWaveSynth::setFilterEnvParams(CurvedADSR::Parameters params) {
//	for (auto i = 0; i < numVoices; ++i) {
//		auto voice = dynamic_cast<SmoothWaveVoice*> (getVoice(i));
//		voice->filterEnvDirty = true;
//	}
//	filterEnvParams = params;
//}
//
//Component* SmoothWaveSynth::createEditor() {
//	return new SmoothWaveEditor(this);
//}
//
//void SmoothWaveSynth::renderNextBlock(
//	AudioBuffer< float > &    outputAudio,
//	const MidiBuffer &    inputMidi,
//	int   startSample,
//	int   numSamples
//) {
//	Synthesiser::renderNextBlock(outputAudio, inputMidi, startSample, numSamples);
//	if (voiceStolenFadeNumSamples > 0) {
//		for (auto i = outputAudio.getNumChannels(); --i >= 0;) {
//			for (int s = 0; s < numSamples; s++) {
//				int destSampleNum = startSample + s;
//				int sourceSampleNum = (voiceStolenFadeCurrentSample + s) % FRAMES_TO_FADE;
//				outputAudio.addSample(
//					i,
//					destSampleNum,
//					voiceStolenFade.getSample(i, sourceSampleNum)
//				);
//			}
//		}
//		voiceStolenFadeNumSamples -= numSamples;
//		if (voiceStolenFadeCurrentSample + numSamples > FRAMES_TO_FADE) {
//			int tailSamples = FRAMES_TO_FADE - voiceStolenFadeCurrentSample;
//			voiceStolenFade.clear(voiceStolenFadeCurrentSample, tailSamples);
//			voiceStolenFade.clear(0, numSamples - tailSamples);
//		} else {
//			voiceStolenFade.clear(voiceStolenFadeCurrentSample, numSamples);
//		}
//		voiceStolenFadeCurrentSample += numSamples;
//		voiceStolenFadeCurrentSample %= FRAMES_TO_FADE;
//
////    for (int i = 0; i < numSamples; i += 1) {
////      float sample = outputAudio.getSample(0, (startSample+i)%numSamples);
////      for (float dot = -1; dot < sample; dot += 0.05) {
////        std::cout << " ";
////      }
////      std::cout << ".\n";
////    }
//	}
//}
//
