//#include "SmoothWaveVoice.h"
//#include "SmoothWaveSound.h"
//#include "SmoothWaveSynth.h"
//#include "../../Util/ADSR/ADSR.h"
//
//SmoothWaveVoice::SmoothWaveVoice(SmoothWaveSynth &i_waveSource, int i_voiceNum) {
//	voiceNum = i_voiceNum;
//	synth = &i_waveSource;
//
//	CurvedADSR::Parameters gainParams;
//	gainParams.attack = .1;
//	gainParams.release = 25;
//	gainAdsr.setParameters(gainParams);
//
//	CurvedADSR::Parameters twangParams;
//	twangParams.attack = .01;
//	twangParams.decay = .05;
//	twangParams.sustain = 0;
//	twangParams.release = 5;
//	filterAdsr.setParameters(twangParams);
//}
//
//bool SmoothWaveVoice::canPlaySound (SynthesiserSound* sound) {
//	assert(dynamic_cast<SmoothWaveSound*> (sound) != nullptr);
//	return true;
//}
//
//void SmoothWaveVoice::startNote (
//	int midiNoteNumber,
//	float velocity,
//	SynthesiserSound* sound,
//	int /*currentPitchWheelPosition*/
//) {
//	currentAngle[0] = 0.0;
//	level = velocity * 0.15;
//	currentHz = MidiMessage::getMidiNoteInHertz (midiNoteNumber);
//
//	filterAdsr.noteOn();
//	gainAdsr.noteOn();
//	for (int i = 0; i < 4; i++) {
//		filters[i].reset();
//	}
//
//  calculateWaveAngleDeltas();
//  calculateWaveAmps();
//}
//
//void SmoothWaveVoice::calculateWaveAngleDeltas() {
//	auto cyclesPerSample = currentHz / getSampleRate();
//
//	angleDelta[0] = cyclesPerSample;
//	double transpose = synth->transpose;
//	double detune = synth->detune;
//	if (transpose != -1) {
//		angleDelta[2] = cyclesPerSample * transpose;
//  } else {
//    angleDelta[2] = 0;
//  }
//	if (detune != -1) {
//		angleDelta[1] = angleDelta[0] * detune;
//		angleDelta[3] = angleDelta[2] * detune;
//  } else {
//    angleDelta[1] = 0;
//    angleDelta[3] = 0;
//  }
//
//	waveAngleDeltasDirty = false;
//}
//
//void SmoothWaveVoice::calculateWaveAmps() {
//	double transpose = synth->transpose;
//	double detune = synth->detune;
//
//	float portion = 1;
//	if (transpose != -1) { portion /= 2.0; }
//	if (detune != -1) { portion /= 2.0; }
//
//	for (int w = 0; w < MAX_WAVES; w++) {
//		float pan = wavePans[w] * 5;
//		waveAmps[w][0] = portion * fmin(1 - pan, 1.0);  // max gain = 1.0, pan = 0: left and right unchanged)
//		waveAmps[w][1] = portion * fmin(1 + pan, 1.0);
//    // std::cout << waveAmps[w][0] << ":" << waveAmps[w][1] << std::endl;
//	}
//}
//
//void SmoothWaveVoice::stopNote (float /*velocity*/, bool allowTailOff) {
//	filterAdsr.noteOff();
//	gainAdsr.noteOff();
//
//	if (allowTailOff == false) {
//		int numSamples = SmoothWaveSynth::FRAMES_TO_FADE;
//		synth->voiceStolenFadeNumSamples = numSamples;
//		renderNextBlock(synth->voiceStolenFade, synth->voiceStolenFadeCurrentSample, numSamples, true);
//		gainAdsr.reset();
//		filterAdsr.reset();
//		clearCurrentNote();
//	}
//}
//
//bool SmoothWaveVoice::isVoiceActive() const {
//	return gainAdsr.isActive();
//}
//
//void SmoothWaveVoice::pitchWheelMoved (int) {}
//void SmoothWaveVoice::controllerMoved (int, int) {}
//
//void SmoothWaveVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples) {
//	renderNextBlock(outputBuffer, startSample, numSamples, false);
//}
//
//void SmoothWaveVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples, bool stolen) {
//	if (isVoiceActive() == false) {
//		clearCurrentNote();
//		return;
//	}
//
//	if (waveAngleDeltasDirty) {
//		calculateWaveAngleDeltas();
//	}
//
//	if (gainEnvDirty) {
//		gainAdsr.setParameters(synth->getGainEnvParams());
//		gainEnvDirty = false;
//	}
//
//	if (filterEnvDirty) {
//		filterAdsr.setParameters(synth->getFilterEnvParams());
//		filterEnvDirty = false;
//	}
//
//	float gainEnv[numSamples];
//	int s = 0;
//	for (; s < numSamples; s++) {
//		if (gainAdsr.isActive() == false) {
//			if (s == 0) { return; }
//			numSamples = s;
//			break;
//		}
//		gainEnv[s] = gainAdsr.getNextSample();
//	}
//
//	float gainLossRate = 0.0f;
//	if (stolen) {
//		gainLossRate = gainEnv[s-1]/numSamples;
//	}
//
//
//	double filterHz = synth->filterHz;
//	double filterQ = synth->filterQ;
//	double filterEnvAmt = synth->filterEnvAmt;
//	IIRCoefficients filterEnvSettings[numSamples];
//	for (int s = 0; s < numSamples; s++) {
//		float envPos = filterAdsr.getNextSample();
//		filterEnvSettings[s] = IIRCoefficients::makeLowPass(44000, filterHz + (envPos * filterEnvAmt), filterQ);
//	}
//
//	for (int v = 0; v < 4; v++) {
//		if (angleDelta[v] != 0.0) {
//			float stolenGainLoss = 0.0f;
//
//			for (int s = 0; s < numSamples; s++) {
//				int sampleNum = startSample + s;
//				float sampleAmp = synth->waveSource.getFrameAtCyclePos(currentAngle[v]);
//
//				if (stolen) {
//					stolenGainLoss += gainLossRate;
//					sampleAmp *= (gainEnv[s] - stolenGainLoss);
//				} else {
//					sampleAmp *= gainEnv[s];
//				}
//
//				// sampleAmps should never get out of the range -1 to 1
//				assert(sampleAmp <= 1 && sampleAmp >= -1);
//				assert(outputBuffer.getNumChannels() == 2);
//
//				filters[v].setCoefficients(filterEnvSettings[s]);
//				sampleAmp = filters[v].processSingleSampleRaw(sampleAmp);
//
//				for (auto i = outputBuffer.getNumChannels(); --i >= 0;) {
//          float panAmp = waveAmps[v][i];
//          if (panAmp > 0) {
//            if (stolen) { sampleNum %= numSamples; }
//            outputBuffer.addSample(i, sampleNum, sampleAmp * panAmp);
//          }
//				}
//
//				currentAngle[v] += angleDelta[v];
//			}
//		}
//	}
//}

