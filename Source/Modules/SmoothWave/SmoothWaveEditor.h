// #pragma once

// #import "SmoothWaveSynth.h"
// #include "../../Components/ThrottleKnobLookAndFeel.h"
// #include "../../Components/ComponentStyler.h"

// using namespace ComponentStyling;

// class SmoothWaveEditor
// 	: public Component,
// 		private Slider::Listener
// {
// 	public:
// 		SmoothWaveEditor(SmoothWaveSynth* i_synth) {
// 			synth = i_synth;
// 			symmetry.setRange(0.0, 100.0, 1.0);
// 			symmetry.setTextValueSuffix("%");
// 			symmetry.setValue(50);
// 			setupSliderCommon(symmetry, 1);

// 			q1Curve.setRange(-100.0, 100.0, 1.0);
// 			q1Curve.setTextValueSuffix("%");
// 			q1Curve.setValue(25);
// 			setupSliderCommon(q1Curve, 1);

// 			transpose.setRange(1.0, 16.0, 0.125);
// 			transpose.setTextValueSuffix("* freq");
// 			transpose.setValue(1.5);
// 			setupSliderCommon(transpose, 1);

// 			detune.setRange(1.0, 1.125, 0.001);
// 			detune.setTextValueSuffix("* freq");
// 			detune.setValue(1.5);
// 			setupSliderCommon(detune, 1);
		  
// 		  waveViewer = synth->waveSource.createViewer();
// 		  addAndMakeVisible(waveViewer);
// 		}

// 		// TODO: remove this... it's very inefficient
// 		void paint (Graphics& g) override {
// 			// g.fillAll (Colours::lightblue);
// 		}

// 		void resized() override {
//  			waveViewer->setBounds(0, 0, 200, 100);
// 			symmetry.setBounds(200, 0, 50, 50);
//  			q1Curve.setBounds(200, 50, 50, 50);
//  			transpose.setBounds(10, 150, 50, 50);
//  			detune.setBounds(70, 150, 50, 50);

// 		}

// 	private:
// 		SmoothWaveSynth* synth;
// 	  Component* waveViewer;
// 		Slider symmetry;
// 		Slider q1Curve;
// 		Slider q2Curve;
// 		Slider q3Curve;
// 		Slider q4Curve;
// 		Slider transpose;
// 		Slider detune;
// 		ThrottleKnobLookAndFeel feels;

// 		void sliderValueChanged (Slider* slider) override {
// 			auto &wave = synth->waveSource;
// 			if (slider == &symmetry) {
// 		   	wave.setSymmetry(symmetry.getValue());
// 		  } else if (slider == &transpose) {
// 		  	synth->setTranspose(transpose.getValue());
// 		  } else if (slider == &detune) {
// 		  	synth->setDetune(detune.getValue());
// 			} else if (slider == &q1Curve) {
// 				bool joinCurves = true;
// 				if (joinCurves) {
// 					wave.setAllCurves(q1Curve.getValue());
// 				} else {
// 		   		wave.setCurve(0, q1Curve.getValue());
// 				}
// 			} else {
// 				std::cerr << "unknown slider accessed\n";
// 			}
// 		}

// 		void setupSliderCommon(Slider &setMeUp, int type) {
//       setupCommonThrottle(setMeUp);
// 			// setMeUp.setSliderStyle(Slider::RotaryVerticalDrag);
//    //    // ComponentColorSettings::tealKnob.copyAllExplicitColoursTo(setMeUp);
//       // setMeUp.setLookAndFeel(&feels);
// 			// setMeUp.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
// 			setMeUp.setPopupDisplayEnabled(true, false, this);
// 			setMeUp.addListener(this);
// 			addAndMakeVisible(setMeUp);
// 		}
// };
