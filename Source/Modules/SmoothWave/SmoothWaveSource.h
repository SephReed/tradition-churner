// #pragma once

// #include "../../../JuceLibraryCode/JuceHeader.h"
// using namespace juce::dsp;

// struct SmoothWaveTables {
//   SmoothWaveTables() {
//     int steps = 200;
//     for (int c = 0; c < steps; ++c) {
//       double curveConstant;
//       if (c == 0) {
//         curveConstant = 0.000001;
//       } else {
//         curveConstant = curveMap(abs(c-100)/100.0);
//         if (c < 100) {
//           curveConstant = 1/curveConstant;
//         }
//       }
//       waveLookup[c].initialise(
//         [curveConstant] (float x) {
//           return 1 - pow(1 - x, curveConstant);
//         }, 
//         0.0f, 1.0f, 250
//       );
//     }
//   }

//   float getWaveSampleUnita(float curveKnob, float sampleUnita) const {
//     if (curveKnob == 0) { return sampleUnita; }
//     else if (curveKnob == 100) { return 1.0; } 
//     return waveLookup[(int)(curveKnob) + 100](sampleUnita);
//   }

//   LookupTableTransform<float> waveLookup[200];
//   LookupTableTransform<double> curveMap = {
//     [] (float x) {
//       if (x >= 1) { return 1.0; }
//       // x += pow(x, 5)/100.0;  // added to get more extreme curves towards the end
//       // (log((1-x)/2)/log(1-((1-x)/2))^.762207
//       double mx = (1-x)/2.0;
//       double nume = log(mx);
//       double denom = log(1-mx);
//       return pow(nume / denom, .762207);
//     }, 
//     0.0f, 1.0f, 100
//   };
// };

// class SmoothWaveSource
//  : public SynthesiserSound
// {
//   public:
//   static const SmoothWaveTables tables;
//   static const int curveMax;
//   static const int numFrames;

// 	SmoothWaveSource();
//   bool appliesToNote (int midiNoteNumber) { return true; }
//   bool appliesToChannel (int midiChannel) { return true; }

//   void setSymmetry(int new_symmetry);
//   int getSymmetry();
//   void setCurve(int curveNum, int curveVal);
//   void setAllCurves(int curveVal);

//   Component* createViewer();

//   float getFrame(int frameNum);
//   float getFrameAtCyclePos(double cyclePos);
//   void assertFrames(int startFrame, int endFrame);

//   private:
//   Component* viewer = nullptr;
//   int symmetry;
//   float* frames;
//   bool* frameDirty;
//   bool allFramesDirty;
//   float getCurveAmp(double curveVal, double cyclePos, int curveKnob);
//   void markFramesDirty();
//   // int curveKnobs[4] = {0, 0, 0, 0};
//   int curves[4] = {25, 25, 25, 25};
// //  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SmoothWaveForm);
// };

// // x101e2
