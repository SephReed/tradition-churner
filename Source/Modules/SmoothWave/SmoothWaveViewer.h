// #pragma once

// #include "SmoothWaveSource.h"

// class SmoothWaveViewer
//  : public Component
// {
// public:
// 	SmoothWaveViewer(SmoothWaveSource* i_wave) {
// 		wave = i_wave;
// 	}
  
//   void paint (Graphics& g) override {
//     int width = getWidth();
//     auto height = getHeight();
//     auto halfHeight = height /2;

//     Path myPath;
//     myPath.startNewSubPath (0, halfHeight);    
//     for (int x = 1; x < width; x++) {
//       double ratio = x / (double)width;
//       double y = (double)(wave->getFrameAtCyclePos(ratio) * halfHeight) + halfHeight;
//       myPath.lineTo(x, y);
//     }
//     g.setColour (Colours::white);      
//     g.strokePath (myPath, PathStrokeType(5.0f));
//   }

//   void resized() override {}

// private:
//   SmoothWaveSource* wave;
// //  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SmoothWaveViewer);
// };
