#pragma once

//#include "SmoothWaveSource.h"
//#include "SmoothWaveVoice.h"
//#include "../../Util/ADSR/CurvedADSR.h"
//
//
//
//
////---------------------------------
//
//
//class SmoothWaveSynth
//	: public Synthesiser
//{
//	public:
//		static const int MAX_VOICES = 32;
//		static const int FRAMES_TO_FADE = 2048;
//
//		SmoothWaveSynth();
//		SmoothWaveSource waveSource;
//		double transpose = 1.5;
//		double detune = 1.001;
//		double filterHz = 3000;
//		double filterQ = 3;
//		double filterEnvAmt = 200;
//		int numVoices = 4;
//		AudioBuffer<float> voiceStolenFade = {2, FRAMES_TO_FADE};
//    int voiceStolenFadeCurrentSample = 0;
//    int voiceStolenFadeNumSamples = 0;
//
//    void setTranspose(float i_transpose);
//  	void setDetune(float i_detune);
//		// bool voiceStolenBufferFull = false;
//
//		CurvedADSR::Parameters getGainEnvParams();
//		void setGainEnvParams(CurvedADSR::Parameters params);
//
//		CurvedADSR::Parameters getFilterEnvParams();
//		void setFilterEnvParams(CurvedADSR::Parameters params);
//
//		Component* createEditor();
//
//		void renderNextBlock(
//			AudioBuffer< float > &  	outputAudio,
//			const MidiBuffer &  	inputMidi,
//			int  	startSample,
//			int  	numSamples
//		);
//
//
//
//	private:
////    CurvedADSR::Parameters gainEnvParams = { .1, 0, 1, 1, 0.2 };
////    CurvedADSR::Parameters filterEnvParams = { .1, 0, 1, 1, 1 };
//
//	  CurvedADSR::Parameters gainEnvParams;
//	  CurvedADSR::Parameters filterEnvParams;
//};

