// #include "SmoothWaveSource.h"
// #include "SmoothWaveViewer.h"

// using namespace juce::dsp;


// /************** STATIC **************************/

// const int SmoothWaveSource::curveMax = 100;
// const int SmoothWaveSource::numFrames = 1024; //no grainularity down to 42.9Hz
// const SmoothWaveTables SmoothWaveSource::tables = {};





// /************** SMOOTH WAVE CLASS **************************/

// SmoothWaveSource::SmoothWaveSource() {
// 	symmetry = 50;
//   frames = new float[SmoothWaveSource::numFrames];
//   frameDirty = new bool[SmoothWaveSource::numFrames];
//   markFramesDirty();
//   assertFrames(0, SmoothWaveSource::numFrames);
// }

// /************** GET/SET **************************/

// void SmoothWaveSource::setSymmetry(int new_symmetry) { 
// 	symmetry = new_symmetry; 

// 	markFramesDirty();
// }
// int SmoothWaveSource::getSymmetry() { return symmetry; }

// void SmoothWaveSource::setCurve(int curveNum, int curveVal) {
// 	curves[curveNum] = curveVal;
// 	markFramesDirty();
// }

// void SmoothWaveSource::setAllCurves(int curveVal) {
// 	for (int i = 0; i < 4; i++) {
// 		curves[i] = curveVal;
// 	}
// 	markFramesDirty();
// }

// /************** GUI **************************/

// Component* SmoothWaveSource::createViewer() {
// 	if (viewer == nullptr) {
// 		viewer = new SmoothWaveViewer(this);
// 	}
//   return viewer;
// }

// /************** FRAMES **************************/

// void SmoothWaveSource::markFramesDirty() {
// 	if (allFramesDirty) { return; }
//   for (int i = 0; i < SmoothWaveSource::numFrames; i++) {
//     frameDirty[i] = true;
//   }
//   allFramesDirty = true;
//   if (viewer != nullptr && viewer->isShowing()) {
//   	viewer->repaint();
//   }
// }

// float SmoothWaveSource::getFrame(int frameNum) {
// 	frameNum %= SmoothWaveSource::numFrames;
// 	if (frameDirty[frameNum]) {
// 		assertFrames(frameNum, frameNum + 1);
// 	}
// 	return frames[frameNum];
// }

// float SmoothWaveSource::getFrameAtCyclePos(double cyclePos) {
// 	int frameNum = (int)(cyclePos * SmoothWaveSource::numFrames);
// 	return getFrame(frameNum);
// }

// void SmoothWaveSource::assertFrames(int startFrame, int endFrame) {
// 	bool mirror = true;
// 	// keep frames in bounds
// 	auto length = SmoothWaveSource::numFrames;
// 	startFrame %= length;
// 	endFrame %= length + 1;
// 	// if cycling over edge, split into two functions
// 	if (endFrame < startFrame) { 
// 		assertFrames(0, endFrame);
// 		endFrame = length; 
// 	} 
// 	// move the startFrame to the first dirty frame
// 	for (; startFrame < endFrame; startFrame++) {
// 		if (frameDirty[startFrame]) { break; }
// 	}
// 	// if no frames were dirty, we're done
// 	if (startFrame >= endFrame) { return; }

// 	// TODO: calculate this only once
//   auto halfLength = length /2;

//   auto q1endFrame = (halfLength * symmetry)/100;
//   // auto q1endFrame = halfLength /2;
//   auto q2endFrame = halfLength;
//   auto q3endFrame = halfLength;
//   if (mirror) {
//   	q3endFrame += (halfLength - q1endFrame);
//   } else {
//   	q3endFrame += q1endFrame;
//   }
//   auto q4endFrame = length;

  

//   int frame = startFrame;
//   while (frame < endFrame) {
// 		int q;	
//   	int curveStartX;
// 		int curveEndX;

// 		if (frame < q1endFrame) {
// 			q = 1;
// 			curveStartX = 0;
// 			curveEndX = q1endFrame;

// 		} else if (frame < q2endFrame) {
// 			q = 2;
// 			curveStartX = q1endFrame;
// 			curveEndX = q2endFrame;

// 		} else if (frame < q3endFrame) {
// 			q = 3;
// 			curveStartX = q2endFrame;
// 			curveEndX = q3endFrame;

// 		} else {
// 			q = 4;
// 			curveStartX = q3endFrame;
// 			curveEndX = q4endFrame;
// 		}

// 		int width = curveEndX - curveStartX;
// 		int curveKnob = curves[q-1];
// 		// double curveVal = SmoothWaveSource::tables.curveMap(abs(curveKnob/100.0));
// 		// if (curveKnob < 1) {
// 		// 	curveVal = 1 / curveVal;
// 		// }

// 		for (; frame < curveEndX && frame < endFrame; frame++) {
// 			int pos = frame - curveStartX;
// 			float cyclePos = pos/(float)width;
// 			if (q == 2 || q == 4) {
// 				cyclePos = 1 - cyclePos;
// 			}
// 			// float amp = getCurveAmp(curveVal, cyclePos, curveKnob);
// 			float amp = SmoothWaveSource::tables.getWaveSampleUnita(curveKnob, cyclePos);
// 			if (q >= 3) {
// 				amp *= -1;
// 			}
// 			frames[frame] = amp;
// 			frameDirty[frame] = false;
// 			allFramesDirty = false;
// 		}
//   }
// }


// float SmoothWaveSource::getCurveAmp(double curveVal, double cyclePos, int curveKnob) {
// 	if (curveKnob == 0) {
// 		std::cout << "knob at 0 -> " << cyclePos << std::endl;
// 		return cyclePos;
// 	} else if (curveKnob == 100) {
// 		return 1;
// 	} else {
// 		// 1 - (1 - cyclePos)^C
// 		return 1 - pow(1 - cyclePos, curveVal);
// 		// return SmoothWaveSource::table.waveLookup(curveKnob, cyclePos)
// 	}
// }
