// #pragma once

// #include "SmoothWaveSource.h"
// #include "SmoothWaveSound.h"
// #include "../../Util/ADSR/ADSR.h"
// #include "../../Util/ADSR/CurvedADSR.h"
// // #include "SmoothWaveSynth.h"
// class SmoothWaveSynth;

// class SmoothWaveVoice 
// 	: public SynthesiserVoice
// {
// 	public:
// 		static const int MAX_WAVES = 4;
// 		bool gainEnvDirty = true;
//     bool filterEnvDirty = true;
//     bool waveAngleDeltasDirty = true;
// 		SmoothWaveVoice(SmoothWaveSynth &i_waveSource, int i_voiceNum);
// 		bool canPlaySound(SynthesiserSound* sound) override;
// 		void startNote(
// 			int midiNoteNumber, 
// 			float velocity,
// 			SynthesiserSound* sound, 
// 			int /*currentPitchWheelPosition*/
// 		) override;

// 		void stopNote(float /*velocity*/, bool allowTailOff) override;
// 		void pitchWheelMoved(int) override;
// 		void controllerMoved(int, int) override;
// 		bool isVoiceActive() const override;
// 		void renderNextBlock(AudioSampleBuffer& outputBuffer, int sampleNum, int numSamples) override;
// 		void renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples, bool stolen);

// 	private:
// 		float currentHz = 220.0;
// 		int voiceNum;
// 		float lastSample = 0;
// 		SmoothWaveSynth* synth;
// 		double currentAngle[MAX_WAVES] = {0, 0, 0, 0};
// 		double angleDelta[MAX_WAVES] = {0, 0, 0, 0};
// 		double level = 0.0;
//     CurvedADSR gainAdsr;
//     CurvedADSR filterAdsr;
//     IIRFilter filters[MAX_WAVES];
//     float waveAmps[MAX_WAVES][2] = {{0, 0}, {0, 0}, {0, 0}, {0, 0}};
//     float wavePans[MAX_WAVES] = {-0.05, 0.05, -0.1, 0.1};
//     void calculateWaveAmps();
//     void calculateWaveAngleDeltas();

// };
