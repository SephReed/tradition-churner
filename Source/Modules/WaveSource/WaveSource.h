#pragma once

// /************** MODULATION TARGETS **************************/
#import "../../Util/DesignatedInit.h"
#import "../Routing/UserInput/ModulationTarget.h"
#import "../Routing/RoutingManager.h"
#import "../Routing/RegisterTargetsInRoutingManager.h"
#include <vector>

struct WaveSourceModTargets: public RegisterTargetsInRoutingManager<WaveSourceModTargets> {
	static inline constexpr ModulationTarget voiceAmps = DesignatedInit(ModulationTarget,
    $.legacyId = "VOICE_AMPS",
		$.fullname = "Voice Amplitude",
		$.shortname = "Voice Amps",
		$.canBePerVoice = true,
		$.ideControl = false,
		$.min = 0,
		$.defaultModDirection = ModDirection::UP,
		$.defaultModRange = 1
	);

	static inline constexpr ModulationTarget gainPreFx = DesignatedInit(ModulationTarget,
    $.legacyId = "SYNTH_GAIN_PRE_FX",
		$.fullname = "Gain (Pre fx)",
		$.shortname = "Pre Gain",
		$.canBePerVoice = true,
		$.min = -70,
	  $.max = 10
	);

	static inline std::vector<const ModulationTarget*>& modTargetList() {
		static std::vector<const ModulationTarget*> output = {
      &voiceAmps,
			&gainPreFx
    };
		return output;
	};
};



// /************** WAVE SOURCE **************************/
#import "NumberedVoiceSynth.h"
#import "../../Util/Observable.h"
#import "../../../JuceLibraryCode/JuceHeader.h"

class PluginProcessor;

class WaveSource : public NumberedVoiceSynth {
public:
	static const int FRAMES_TO_FADE = 512;
	PluginProcessor* plugin;
	int sourceNum;

	Observable<float> amp = {0.75};
	Observable<float> tune = {1.0};

	UserInputMod gainPreFx;// = { &WaveSource::modTargets().gainPreFx };

	WaveSource(PluginProcessor* i_plugin, int sourceNum);

	virtual Component* getFullView() { 
		assert(false);
		return nullptr; 
	}

protected:
	UserInputMod createUserInputMod(const ModulationTarget& modTarget);

private:
	AudioBuffer<float> stolenVoiceFades = {2, FRAMES_TO_FADE};
  int stolenVoiceFadesCurrentSample = 0;
  int stolenVoiceFadesNumSamples = 0;
};



// /************** WAVE SOURCE VOICE **************************/
#import "WaveRenderingBuffer.h"

class WaveSourceVoice : public NumberedVoice {
public:
	// ModulationEndpoint voiceAmps { &WaveSource::modTargets.voiceAmps };
	// ModulationEndpoint gainPreFx { &WaveSource::modTargets.gainPreFx };
	ModulationEndpoint voiceAmps;
	ModulationEndpoint gainPreFx;

	WaveSourceVoice(WaveSource* i_synth, int voiceNum);
	void startNote(int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition) override;
	virtual void updateCurrentHz(int midiNum);
	void setEnpointsVoiceSynthNums(std::vector<ModulationEndpoint*> endpoints);
	virtual WaveSource* getSynth();

	bool isVoiceActive() const;
	void renderNextBlock(AudioBuffer<float> &outputBuffer, int startSample, int numSamples) override;
	virtual void renderNextWavesToBlock(AudioBuffer<float> &outputBuffer, int numSamples);
	virtual void applyAmpEnvelope(WaveRenderingBuffer& modMe, ModBuffer& voiceAmpModulation);

protected:
	WaveSource* synth;
	AudioBuffer<float> buffer;
	AudioBuffer<float> overlap;

	float currentHz = 220.0;
	float currentHzAngleDelta = 0.0;

private:
	std::vector<ModulationEndpoint*> endpoints = {
		&voiceAmps,
		&gainPreFx
	};
};
