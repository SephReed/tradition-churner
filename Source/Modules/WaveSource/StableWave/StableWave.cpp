#import "StableWaveSynth.h"
using namespace std;



//************** STABLE WAVE SYNTH **************************/

#include "../../../../JuceLibraryCode/JuceHeader.h"
#import "../WaveSource.h"
#import "../../../PluginProcessor.h"



StableWaveSynth::StableWaveSynth(PluginProcessor* i_plugin, int sourceNum)
: WaveSource(i_plugin, sourceNum),
	upperMix(createUserInputMod(StableWaveSynth::modTargets().upperMix)),
	upper(createUserInputMod(StableWaveSynth::modTargets().upper)),
	detune(createUserInputMod(StableWaveSynth::modTargets().detune)),
	spread(createUserInputMod(StableWaveSynth::modTargets().spread)),
	lowerWidth(createUserInputMod(StableWaveSynth::modTargets().lowerWidth)),
	upperWidth(createUserInputMod(StableWaveSynth::modTargets().upperWidth))
{
	// plugin->routingManager.addTargets(StableWaveSynth::modTargets().list);

	// for (auto userInput : userInputs) {
	// 	userInput->modChainLoc.synthNum = sourceNum;
	// }
	// plugin->routingManager.registerUserInputMods(userInputs);

	addSound(new StableWaveSound());
}

// void StableWaveSynth::addCropper() {
// 	if (cropper == nullptr) {
// 		cropper = new Cropper();
// 		// synth.thingline.add(cropper);
// 	}
// }

// https://stackoverflow.com/questions/54375820/mismatched-type-error-between-basechildbaseparent-and-derivedchildderived
NumberedVoice* StableWaveSynth::createVoice(int voiceNum) {
	return new StableWaveVoice(this, voiceNum);
}



//************** STABLE WAVE VOICE **************************/
#import "StableWaveVoice.h"
#include <math.h>


StableWaveVoice::StableWaveVoice(StableWaveSynth* i_synth, int voiceNum)
: WaveSourceVoice(i_synth, voiceNum),
	upperMix(i_synth->upperMix.createEndpoint(voiceNum)),
	upper(i_synth->upper.createEndpoint(voiceNum)),
	detune(i_synth->detune.createEndpoint(voiceNum)),
	spread(i_synth->spread.createEndpoint(voiceNum)),
	lowerWidth(i_synth->lowerWidth.createEndpoint(voiceNum)),
	upperWidth(i_synth->upperWidth.createEndpoint(voiceNum))
{
	// setEnpointsVoiceSynthNums(endpoints);
	auto& routeMgmt = synth->plugin->routingManager;
	routeMgmt.registerEndpoints(endpoints);

	auto synth = getSynth();
	for (auto& factory : synth->ampFxFactories) {
		ampFx.push_back(factory->createProcessingLayer(voiceNum));
		ampFx[ampFx.size() - 1]->registerEndpoints(routeMgmt);
	}
}

StableWaveSynth* StableWaveVoice::getSynth() {
	return static_cast<StableWaveSynth*>(synth);
}


void StableWaveVoice::stopNote(float velocity, bool allowTailOff) {
	// do nothing
	// for (int w = 0; w < MAX_WAVES; w++) {
	// 	waveAngleDelta[w] = 0;
	// }

//	filterAdsr.noteOff();
//	gainAdsr.noteOff();
//
//	if (allowTailOff == false) {
//		int numSamples = SmoothWaveSynth::FRAMES_TO_FADE;
//		synth->voiceStolenFadeNumSamples = numSamples;
//		renderNextBlock(synth->voiceStolenFade, synth->voiceStolenFadeCurrentSample, numSamples, true);
//		gainAdsr.reset();
//		filterAdsr.reset();
//		clearCurrentNote();
//	}
}



bool StableWaveVoice::isVoiceActive() const {
	return voiceAmps.endingIncomplete();
}
//
//void SmoothWaveVoice::pitchWheelMoved (int) {}
//void SmoothWaveVoice::controllerMoved (int, int) {}
//
// void StableWaveVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples) {
// 	renderNextBlock(outputBuffer, startSample, numSamples, false);
// }

// void StableWaveVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples, bool stolen) {

// 	// float gainEnv[numSamples];
// 	// int s = 0;
// 	// for (; s < numSamples; s++) {
// 	// 	if (gainAdsr.isActive() == false) {
// 	// 		if (s == 0) { return; }
// 	// 		numSamples = s;
// 	// 		break;
// 	// 	}
// 	// 	gainEnv[s] = gainAdsr.getNextSample();
// 	// }

// 	// float gainLossRate = 0.0f;
// 	// if (stolen) {
// 	// 	gainLossRate = gainEnv[s-1]/numSamples;
// 	// }


// 	// double filterHz = synth->filterHz;
// 	// double filterQ = synth->filterQ;
// 	// double filterEnvAmt = synth->filterEnvAmt;
// 	// IIRCoefficients filterEnvSettings[numSamples];
// 	// for (int s = 0; s < numSamples; s++) {
// 	// 	float envPos = filterAdsr.getNextSample();
// 	// 	filterEnvSettings[s] = IIRCoefficients::makeLowPass(44000, filterHz + (envPos * filterEnvAmt), filterQ);
// 	// }

// 	// for (int w = 0; w < 4; w++) {
// 	// 	if (angleDeltaSource[w].size() == 0) {
// 	// 		assert(w != 0);
// 	// 	} else {
// 	// 		// float stolenGainLoss = 0.0f;

// 	// 		for (int s = 0; s < numSamples; s++) {

// 	// 			// if (stolen) {
// 	// 			// 	stolenGainLoss += gainLossRate;
// 	// 			// 	sampleAmp *= (gainEnw[s] - stolenGainLoss);
// 	// 			// } else {
// 	// 			// 	sampleAmp *= gainEnw[s];
// 	// 			// }

// 	// 			// filters[w].setCoefficients(filterEnwSettings[s]);
// 	// 			// sampleAmp = filters[w].processSingleSampleRaw(sampleAmp);
// 	// 		}
// 	// 	}
// 	// }
// }



void StableWaveVoice::renderNextWavesToBlock(AudioBuffer<float> &outputBuffer, int numSamples) {
	auto voiceAmpModulation = voiceAmps.getNextSamplesReadonly(numSamples);
 	if (voiceAmpModulation->canBeIgnored(0.0)) { return; }
	
	// WaveSamples waveSamples{4};

	auto upperSamples = upper.getNextSamplesReadonly(numSamples);	
	auto upperMixSamples = upperMix.getNextSamplesReadonly(numSamples);
	auto detuneSamples = detune.getNextSamplesReadonly(numSamples);

	auto mixOutLower = upperMixSamples->canBeIgnored(1.0);
	auto mixOutUpper = upperMixSamples->canBeIgnored(0);
	auto nonIntervalDetune = detuneSamples->canBeIgnored(1);
	auto nonIntervalUpper = upperSamples->canBeIgnored(1);

	auto& hasWave = waveSamples.hasWave;
	hasWave[0] = !mixOutLower;
	hasWave[1] = !mixOutLower && !nonIntervalDetune;
	hasWave[2] = !mixOutUpper && !nonIntervalUpper;
	hasWave[3] = !mixOutUpper && !nonIntervalUpper && !nonIntervalDetune;

	waveSamples.resizeSamples(numSamples);
//	for (int w = 0; w < 4; w++) {
//		if (hasWave[w]) {
//			waveSamples[w].resize(numSamples, {2});
//		}
//	}

	fillWithAngleDeltas(waveSamples, upperSamples, detuneSamples);
	convertAngleDeltasToAmps(waveSamples);
	applyAmpFx(waveSamples);
	applyAmpEnvelope(waveSamples, voiceAmpModulation);
	mixAndSpreadAmps(waveSamples, upperMixSamples);

	int rawIndex = 0;
	auto& raw = waveSamples.raw();
	for (int s = 0; s < numSamples; s++) {
		for (int w = 0; w < waveSamples.numWaves; w++) {
			if (hasWave[w]) {
				for (auto c = outputBuffer.getNumChannels(); --c >= 0;) {
					// outputBuffer.addSample(c, s, waveSamples.get(w, s, c));
					outputBuffer.addSample(c, s, raw[rawIndex]);
					rawIndex++;
				}
			} else {
				rawIndex += waveSamples.numChannels;
			}
		}
	}
}


// // TODO: give detune even split (multiply root?); 
void StableWaveVoice::fillWithAngleDeltas(
	WaveSamples& fillMe, 
	ModBuffer& upperSamples, 
	ModBuffer& detuneSamples
) {
	auto numSamples = fillMe.numSamples;
	auto& hasWave = fillMe.hasWave;
	
	auto tune = 1.0;
	float root = tune * currentHzAngleDelta;
	float angleDeltas[4] = {root, 0, 0 , 0};
	bool updateDelta[4] = {true, false, false, false};

	auto shouldUpdate = [&](int waveNum) {
		auto out = updateDelta[waveNum];
		updateDelta[waveNum] = false;
		return out;
	};

	// auto addAndProgressAngle = [&](int waveNum, int sampleNum) {
	// 	fillMe.set(waveNum, sampleNum, 0, waveAngle[waveNum]);
	// 	waveAngle[waveNum] += angleDeltas[waveNum];
	// 	waveAngle[waveNum] -= (int)waveAngle[waveNum];
	// };

	auto& raw = fillMe.raw();
	auto addAndProgressAngle = [&](int waveNum, int rawIndex) {
		raw[rawIndex] = waveAngle[waveNum];
		waveAngle[waveNum] += angleDeltas[waveNum];
		waveAngle[waveNum] -= (int)waveAngle[waveNum];
	};
	
	int rawIndex = 0;
	for (int s = 0; s < numSamples; s++) {
		if (upperSamples->sampleIsUnique(s)) { 
			updateDelta[2] = hasWave[2]; 
			updateDelta[3] = hasWave[3]; 
		}
		if (detuneSamples->sampleIsUnique(s)) { 
			updateDelta[1] = hasWave[1]; 
			updateDelta[3] = hasWave[3]; 
		}

		if (shouldUpdate(0)) { angleDeltas[0] = root; }
		if (shouldUpdate(1)) { angleDeltas[1] = root * detuneSamples->get(s); }
		if (shouldUpdate(2)) { angleDeltas[2] = root * upperSamples->get(s); }
		if (shouldUpdate(3)) { angleDeltas[3] = root * upperSamples->get(s) * detuneSamples->get(s); }
		
		for (int w = 0; w < fillMe.numWaves; w++) { 
			// if (hasWave[w]) { addAndProgressAngle(w, s); }
			if (hasWave[w]) { addAndProgressAngle(w, rawIndex); }
			rawIndex += fillMe.numChannels;
		}
	}
}
void StableWaveVoice::convertAngleDeltasToAmps(WaveSamples& modMe) {
	assert(false);
//	for (int w = 0; w < modMe.size(); w++) {
//		if (hasWave[w]) {
//			auto& wave = modMe[w];
//			for (int s = 0; s < numSamples; s++) {
//				wave[s][0] = getSynth()->getAmpUnita(wave[s][0]);
//			}
//		}
//	}
}

void StableWaveVoice::applyAmpFx(WaveSamples& modMe) {
	for (auto& fx : ampFx) {
		fx->resetCache(modMe.numSamples);
		fx->processSamples(modMe);
	}
}

// void StableWaveVoice::applyAmpEnvelope(WaveSamples& modMe, ModBuffer& voiceAmpModulation) {
// 	int rawIndex = 0;
// 	auto& raw = modMe.raw();
// 	for (int s = 0; s < modMe.numSamples; s++) {
// 		auto gain = voiceAmpModulation->get(s);
// 		for (int w = 0; w < modMe.numWaves; w++) {
// 			if (modMe.hasWave[w]) {
// 				raw[rawIndex] *= gain;
// 			}
// 			rawIndex += modMe.numChannels;
// 		}
// 	}
// }


void StableWaveVoice::mixAndSpreadAmps(WaveSamples& modMe, ModBuffer& upperMixSamples) {
	auto& hasWave = modMe.hasWave;
	auto numSamples = modMe.numSamples;

	bool hasDetune = hasWave[1] || hasWave[3];
	bool hasUpper = hasWave[2];
	bool hasLower = hasWave[0];

	auto getNextSamplesOrNull = [numSamples](bool dontIgnore, ModulationEndpoint& source) {
		auto out = dontIgnore ? source.getNextSamplesReadonly(numSamples) : NULL;
		if (out && out->canBeIgnored()) { out = NULL; }
		return out;
	};

	ModBuffer spreadSamples = getNextSamplesOrNull(hasDetune, spread);
	ModBuffer lowerWidthSamples = getNextSamplesOrNull(hasLower && hasDetune, lowerWidth);
	ModBuffer upperWidthSamples = getNextSamplesOrNull(hasUpper && hasDetune, upperWidth);

	int waveCount = 1 + hasDetune + hasUpper + (hasUpper && hasDetune);
	float portion = 1.0 / waveCount;

	static const auto limitUnita = [](float num){ return num > 1 ? 1 : (num < 0 ? 0 : num); };

	float lowerGains[2] = {1, 1};
	float upperGains[2] = {1, 1};
	bool updateLowerGains = true;
	bool updateUpperGains = true;
	float lowerPortion = portion;
	float upperPortion = portion;

	int rawIndex = 0;
	auto& raw = modMe.raw();
	const auto doWavePanning = [&](int waveNum) {
		auto& gains = waveNum < 2 ? lowerGains : upperGains;
		bool flipside = waveNum % 2;
		float* channels = &raw[rawIndex];
		channels[1] = channels[0] * gains[flipside ? 1 : 0];
		channels[0] *= gains[flipside ? 0 : 1];
	};

	for (int s = 0; s < numSamples; s++) {
		if (upperMixSamples->sampleIsUnique(s)) {
			auto mix = upperMixSamples->get(s);
			upperPortion = (portion * 2) * mix;
			lowerPortion = (portion * 2) - upperPortion;
			updateUpperGains = updateLowerGains = true;
		}
		if (spreadSamples && spreadSamples->sampleIsUnique(s)) {
			updateUpperGains = updateLowerGains = true;
		}
		updateLowerGains = updateLowerGains || (lowerWidthSamples && lowerWidthSamples->sampleIsUnique(s));
		updateUpperGains = updateUpperGains || (upperWidthSamples && upperWidthSamples->sampleIsUnique(s));

		if (updateLowerGains) {
			auto diff = spreadSamples ? (
				lowerPortion * spreadSamples->get(s) * (lowerWidthSamples ? lowerWidthSamples->get(s) : 1)
			) : 0;
			lowerGains[0] = limitUnita(lowerPortion - diff);
			lowerGains[1] = limitUnita(lowerPortion + diff);
		}

		if (updateUpperGains) {
			auto diff = spreadSamples ? (
				upperPortion * spreadSamples->get(s) * (upperWidthSamples ? upperWidthSamples->get(s) : 1)
			) : 0;
			upperGains[0] = limitUnita(upperPortion - diff);
			upperGains[1] = limitUnita(upperPortion + diff);
		}

		for (int w = 0; w < modMe.numWaves; w++) {
			if (hasWave[w]) {
				doWavePanning(w);
			}
			rawIndex += modMe.numChannels;
		}
	}
}







//************** STABLE WAVE MINI VIEW **************************/



// StableWaveMiniView::StableWaveMiniView(StableWaveSynth* i_synth): synth(i_synth) {
// 	waveGraph = synth->createGraph();
// 	addAndMakeVisible(waveGraph);

// 	setupCommonThrottle(amp);
// 	amp.addListener(this);
// 	amp.setRange(0.0, 1.0, 0.01);
// 	addAndMakeVisible(amp);

// 	auto updateComponentStates = [](float){
// 		auto dontUpdate = NotificationType::dontSendNotification;
// 		// auto c1val = synth->C1.get();
// 		// C1.setValue(c1val*100, dontUpdate);
// 		// auto c2val = synth->C2.get();
// 		// C2.setValue(c2val*100, dontUpdate);
// 	};

// 	synth->amp.observe(updateComponentStates);
// 	synth->pan.observe(updateComponentStates);
// 	synth->tune.observe(updateComponentStates);
// }

// void StableWaveMiniView::paint (Graphics& g) {}
// void StableWaveMiniView::resized() {
// 	auto halfWidth = getWidth()/2.0;
// 	auto halfHeight = getHeight()/2.0;
// 	auto pad = 3;

// 	// Half 1 
// 	waveGraph->setBounds(pad, pad, halfWidth -pad, halfHeight -pad);
// }


// void StableWaveMiniView::sliderValueChanged (Slider* slider) {
// 	auto value = slider->getValue();
// 	if (slider == &amp) {}
// }

