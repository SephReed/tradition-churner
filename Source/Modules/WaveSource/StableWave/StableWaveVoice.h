#pragma once
using namespace std;

#import "../WaveSource.h"
#import "StableWaveSynth.h"
#import "../WaveRenderingBuffer.h"

class StableWaveVoice
	: public WaveSourceVoice
{
public:
	static const int MAX_WAVES = 4;
	using WaveSamples = WaveRenderingBuffer;
	// class WaveSamples {
	// public:
	// 	int numSamples = 0;
	// 	int numWaves = StableWaveVoice::MAX_WAVES;
	// 	int numChannels = 2;
	// 	int sampleEntrySize = 0;
	// 	vector<float> data;
	// 	vector<bool> hasWave = {false, false, false, false};

	// 	void resizeSamples(int i_numSamples, int i_numChannels = 2) {
	// 		numSamples = i_numSamples;
	// 		numChannels = i_numChannels;
	// 		sampleEntrySize = numWaves * numChannels;
	// 		data.resize(numSamples * sampleEntrySize);
	// 	}

	// 	float get(int waveNum, int sampleNum, int channel) {
	// 		return data[toIndex(waveNum, sampleNum, channel)];
	// 	}

	// 	void set(int waveNum, int sampleNum, int channel, float newVal) {
	// 		data[toIndex(waveNum, sampleNum, channel)] = newVal;
	// 	}

	// 	vector<float>& raw() { return data; }

	// protected:
	// 	int toIndex(int waveNum, int sampleNum, int channel) {
	// 		return (sampleNum * sampleEntrySize) + (waveNum * numChannels) + channel;
	// 	}
	// };

	StableWaveVoice(StableWaveSynth* i_synth, int voiceNum);
	StableWaveSynth* getSynth() override;

protected:
	float waveAngle[MAX_WAVES] = {0, 0, 0, 0};
	WaveSamples waveSamples;

	ModulationEndpoint upperMix;
	ModulationEndpoint upper;
  ModulationEndpoint detune;

  ModulationEndpoint spread;
  ModulationEndpoint lowerWidth;
  ModulationEndpoint upperWidth;

  vector<unique_ptr<ProcessingLayer>> ampFx;

	
private:
	void fillWithAngleDeltas(WaveSamples& fillMe, ModBuffer& upperSamples, ModBuffer& detuneSamples);
	virtual void convertAngleDeltasToAmps(WaveSamples& modMe);
	void applyAmpFx(WaveSamples& modMe);
	// void applyAmpEnvelope(WaveSamples& modMe, ModBuffer& voiceAmpModulation);
	void mixAndSpreadAmps(WaveSamples& modMe, ModBuffer& upperMixSamples);

	bool canPlaySound(juce::SynthesiserSound *) override { return true; }
	
	bool isVoiceActive() const override;
	// void startNote(int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition) override;
	
	void stopNote(float velocity, bool allowTailOff) override;
	
	void pitchWheelMoved(int newPitchWheelValue) override {}
	
	void controllerMoved(int controllerNumber, int newControllerValue) override {}

	void renderNextWavesToBlock(AudioBuffer<float> &outputBuffer, int numSamples) override;

	std::vector<ModulationEndpoint*> endpoints = {
		&upperMix,
		&upper,
		&detune,

		&spread,
		&lowerWidth,
		&upperWidth
	};
	
};
