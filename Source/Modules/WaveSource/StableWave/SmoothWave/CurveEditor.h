#pragma once

//************** SMOOTH WAVE CURVE EDITOR **************************/
//#import "../../../../Components/ComponentStyler.h"
//using namespace ComponentStyling;
class SmoothWaveSynth;

class SmoothWaveCurveEditor: public Component {
public:
	using slider_ptr = unique_ptr<Slider>;

  SmoothWaveCurveEditor(SmoothWaveSynth* synth);
  SmoothWaveCurveEditor(slider_ptr PW, slider_ptr EW, slider_ptr C1, slider_ptr C2);
  void resized() override;

  void paint (Graphics& g) override {
		// g.fillAll(Colour(200, 40, 100));
	}

protected:
  slider_ptr PW;
  slider_ptr EW;
  slider_ptr C1;
  slider_ptr C2;
};
