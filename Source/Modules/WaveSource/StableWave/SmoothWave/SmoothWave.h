#pragma once
#include <vector>
using namespace std;


#import "SmoothWaveTables.h"






//************** MODULATION TARGETS **************************/
#import "../../../Routing/UserInput/ModulationTarget.h"
#import "../../../../Util/DesignatedInit.h"

#import "SmoothWaveModTargets.h"



//************** SMOOTH WAVE SYNTH **************************/
#import "../StableWaveSynth.h"

#import "../../StaticallyRegisterInSourceManager.h"
#import "../../../ProcessingLayer/Cropper.h"

class SmoothWaveSynth 
: public StableWaveSynth, 
  public StaticallyRegisterInSourceManager<SmoothWaveSynth>
{
public:
  static inline SmoothWaveTables tables = {};
  static constexpr int curveMax {100};
  static constexpr int numFrames {1024};

  class MyFactory : public SourceFactory {
  public:
    WaveSource* create(PluginProcessor* i_plugin, int sourceNum) const final { 
      return new SmoothWaveSynth(i_plugin, sourceNum); 
    }
  };

	static inline MyFactory factory = {};

	SmoothWaveSynth(PluginProcessor* i_plugin, int sourceNum);
	float getAmpUnita(float ratio) override;

	Component* getFullView() override;
//	Component* getMiniView() override;

  UserInputMod pulseWidth;
  UserInputMod expWidth;
  UserInputMod curve1;
  UserInputMod curve2;

  Cropper cropper {*this};

  vector<UserInputMod*> userInputs = {
    &pulseWidth,
    &expWidth,
    &curve1,
    &curve2
  };

protected:
  NumberedVoice* createVoice(int voiceNum) final;

};


//************** SMOOTH WAVE VOICE **************************/

class SmoothWaveVoice : public StableWaveVoice {
public:
  SmoothWaveVoice(SmoothWaveSynth* synth, int voiceNum);
  void convertAngleDeltasToAmps(WaveSamples& modMe) final;

protected:
  ModulationEndpoint pulseWidth;
  ModulationEndpoint expWidth;
  ModulationEndpoint curve1;
  ModulationEndpoint curve2;

  vector<ModulationEndpoint*> endpoints = {
    &pulseWidth,
    &expWidth,
    &curve1,
    &curve2
  };
};






//************** SMOOTH WAVE FULL VIEW **************************/
#import "CurveEditor.h"

class SmoothWaveFullView : public Component {
public:
	SmoothWaveFullView(SmoothWaveSynth* i_synth);
	SmoothWaveSynth* synth;

	SmoothWaveCurveEditor curveEditor;
	Component* waveGraph;
	Component* cropEditor;
  DetuneEditor detuneEditor;
  ChordEditor chordEditor;
  CropperEditor cropperEditor;

	void resized() override;
};
