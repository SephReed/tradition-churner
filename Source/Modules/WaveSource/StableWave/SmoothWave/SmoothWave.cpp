#import "SmoothWave.h"


// /************** SYNTH **************************/
#import "../../../../PluginProcessor.h"

SmoothWaveSynth::SmoothWaveSynth(PluginProcessor* i_plugin, int sourceNum) 
: StableWaveSynth(i_plugin, sourceNum), 
	pulseWidth(createUserInputMod(SmoothWaveModTargets::pulseWidth)),
	expWidth(createUserInputMod(SmoothWaveModTargets::expWidth)),
	curve1(createUserInputMod(SmoothWaveModTargets::curve1)),
	curve2(createUserInputMod(SmoothWaveModTargets::curve2))
{
	ampFxFactories.push_back(&cropper);
}


Component* SmoothWaveSynth::getFullView() {
	auto fullView = new SmoothWaveFullView(this);
	return fullView;
}

float SmoothWaveSynth::getAmpUnita(float ratio) {
	auto amp = SmoothWaveSynth::tables.getAmpForParamsUnita(ratio, curve1.get(), curve2.get(), pulseWidth.get(), expWidth.get());
	for (auto& factory : ampFxFactories) {
		amp = factory->processSampleWithoutModulation(amp);
	}
	return amp;
}


NumberedVoice* SmoothWaveSynth::createVoice(int voiceNum) {
	return new SmoothWaveVoice(this, voiceNum);
}


// /************** SMOOTH WAVE VOICE **************************/

SmoothWaveVoice::SmoothWaveVoice(SmoothWaveSynth* synth, int voiceNum)
: StableWaveVoice(synth, voiceNum),
	pulseWidth(synth->pulseWidth.createEndpoint(voiceNum)),
	expWidth(synth->expWidth.createEndpoint(voiceNum)),
	curve1(synth->curve1.createEndpoint(voiceNum)),
	curve2(synth->curve2.createEndpoint(voiceNum))
{
	setEnpointsVoiceSynthNums(endpoints);
	synth->plugin->routingManager.registerEndpoints(endpoints);
}


void SmoothWaveVoice::convertAngleDeltasToAmps(WaveSamples& modMe) {
	auto numSamples = modMe.numSamples;

	auto curve1Samples = curve1.getNextSamplesReadonly(numSamples);
	auto curve2Samples = curve2.getNextSamplesReadonly(numSamples);
	auto pwSamples = pulseWidth.getNextSamplesReadonly(numSamples);
	auto ewSamples = expWidth.getNextSamplesReadonly(numSamples);

	if (pulseWidth.isAudioRate() || expWidth.isAudioRate() || curve1.isAudioRate() || curve2.isAudioRate()) {
		// not yet optimized
		assert(false);
	}

	SmoothWaveSynth::tables.convertAngleDeltasToAmps(
		modMe, 
		curve1Samples->get(0),
		curve2Samples->get(0),
		pwSamples->get(0),
		ewSamples->get(0)
	);
}


// /************** FULL VIEW **************************/

SmoothWaveFullView::SmoothWaveFullView(SmoothWaveSynth* i_synth)
	: synth(i_synth),
		curveEditor(i_synth),
		detuneEditor(i_synth),
		chordEditor(i_synth),
		cropperEditor(i_synth->cropper)
{
	waveGraph = synth->createGraph();
	addAndMakeVisible(waveGraph);
	auto doRepaint = [&](float){
  	const MessageManagerLock mmLock;
  	waveGraph->repaint();
	};
	synth->pulseWidth.observe(doRepaint, false);
	synth->expWidth.observe(doRepaint, false);
	synth->curve1.observe(doRepaint, false);
	synth->curve2.observe(doRepaint, false);
	synth->cropper.cropCeiling.observe(doRepaint, false);
	synth->cropper.ampLoss.observe(doRepaint, false);

	addAndMakeVisible(curveEditor);
	addAndMakeVisible(detuneEditor);
	addAndMakeVisible(chordEditor);
	addAndMakeVisible(cropperEditor);
}

void SmoothWaveFullView::resized() {
	// auto halfWidth = getWidth()/2.0;
	// auto halfHeight = getHeight()/2.0;
	auto pad = 3;

	SectionedRectangle bounds(getLocalBounds());
//	bounds.padInwards(pad);

	auto halveCols = bounds.splitHorizontallyAtRatio(0.5, pad);

	//	Q (0, 0)
	auto col1Rows = halveCols.left.splitVerticallyAtRatio(0.5, pad);
	waveGraph->setBounds(col1Rows.top);

	// Q (0, 1)
	auto waveEditors = col1Rows.bottom.splitHorizontallyAtRatio(0.666, pad);
	curveEditor.setBounds(waveEditors.left);
	cropperEditor.setBounds(waveEditors.right);

	// Q (1, 0)
	auto col2Rows = halveCols.right.splitVerticallyAtRatio(0.5, pad);

	auto layersEditors = col2Rows.top.splitHorizontallyAtRatio(0.5, pad);
	chordEditor.setBounds(layersEditors.left);
	detuneEditor.setBounds(layersEditors.right);
}





// /************** CURVE EDITOR **************************/
#import "CurveEditor.h"
#import "../../../../Util/SectionedRectangle.h"

SmoothWaveCurveEditor::SmoothWaveCurveEditor(SmoothWaveSynth* synth)
: SmoothWaveCurveEditor(
	synth->pulseWidth.createHorizontalFader(),
  synth->expWidth.createHorizontalFader(),
  synth->curve1.createRotary(),
  synth->curve2.createRotary()
) {}

SmoothWaveCurveEditor::SmoothWaveCurveEditor(slider_ptr i_PW, slider_ptr i_EW, slider_ptr i_C1, slider_ptr i_C2)
: PW(move(i_PW)),
	EW(move(i_EW)),
	C1(move(i_C1)),
	C2(move(i_C2))
{
	addAndMakeVisible(PW.get());
  addAndMakeVisible(EW.get());
  addAndMakeVisible(C1.get());
  addAndMakeVisible(C2.get());	
}

void SmoothWaveCurveEditor::resized() {
  auto pad = 3;
	SectionedRectangle bounds(getLocalBounds());
	auto slidersAndKnobs = bounds.splitVerticallyAtRatio(0.5, pad);
	
	auto sliders = slidersAndKnobs.top.splitVerticallyAtRatio(0.5, pad);
	PW->setBounds(sliders.top);
	EW->setBounds(sliders.bottom);
	
	// Curve Knobs
	auto knobs = slidersAndKnobs.bottom.splitHorizontallyAtRatio(0.5, pad);
	C1->setBounds(knobs.left);
	C2->setBounds(knobs.right);
}



