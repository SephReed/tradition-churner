#pragma once

#import "../../../Routing/UserInput/ModulationTarget.h"
#import "../../../../Util/DesignatedInit.h"
#import "../../../Routing/RegisterTargetsInRoutingManager.h"

struct SmoothWaveModTargets : public RegisterTargetsInRoutingManager<SmoothWaveModTargets> {

  static inline constexpr ModulationTarget pulseWidth = DesignatedInit(ModulationTarget,
    $.legacyId = "PULSE_WIDTH",
    $.fullname = "Pulse Width",
    $.shortname = "PW",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0.5,
    $.defaultValue = 0.5,
    $.max = 1
  );

  static inline constexpr ModulationTarget expWidth = DesignatedInit(ModulationTarget,
    $.legacyId = "EXPANSION_WIDTH",
    $.fullname = "Expansion Width",
    $.shortname = "EW",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0.5,
    $.defaultValue = 0.5,
    $.max = 1
  );

  static inline constexpr ModulationTarget curve1 = DesignatedInit(ModulationTarget,
    $.legacyId = "CURVE_1",
    $.fullname = "Curve 1 Shape",
    $.shortname = "C1",
    $.canBePerVoice = true,
    $.min = -100,
    $.baseline = 0,
    $.defaultValue = 33,
    $.max = 100,
    $.defaultModRange = 25
  );

  static inline constexpr ModulationTarget curve2 = DesignatedInit(ModulationTarget,
    $.legacyId = "CURVE_2",
    $.fullname = "Curve 2 Shape",
    $.shortname = "C2",
    $.canBePerVoice = true,
    $.min = -100,
    $.baseline = 0,
    $.defaultValue = 33,
    $.max = 100,

    $.defaultModRange = 25
  );

  static inline std::vector<const ModulationTarget*>& modTargetList() {
    static std::vector<const ModulationTarget*> output = {
      &pulseWidth,
      &expWidth,
      &curve1,
      &curve2
    };
    return output;
  };
};
