#pragma once

//************** SMOOTH WAVE STATIC TABLES **************************/
#include "../../../../../JuceLibraryCode/JuceHeader.h"
#include "../../../../Util/TC_LookupTable.h"
#include "../../WaveRenderingBuffer.h"
using namespace juce::dsp;

struct SmoothWaveTables {
  struct PhaseInfo {
    vector<float> widths;
    vector<float> ends;
  };

  SmoothWaveTables() {
    int curveMax = 200;
    int curveMin = 0;
    waveLookup.reserve(curveMax+1);
    for (int c = curveMin; c <= curveMax; ++c) {
      double curveConstant;
      if (c == 0) {
        curveConstant = 0.000001;
//			} else if (c == 200) {
//				curveConstant =
      } else {
        curveConstant = curveMap[abs(c-100)/100.0];
        if (c < 100) {
          curveConstant = 1/curveConstant;
        }
      }
      waveLookup.push_back({
        [curveConstant] (float x) {
          return 1 - pow(1 - x, curveConstant);
        }, 
        250,
        0.0f, 1.0f,
      });
    }
  }

  PhaseInfo createPhaseInfo(float pw, float ew) {
    vector<float> phaseEnds = {
      ew * pw,
      pw,
      pw + ((1-pw)*(1-ew)),
      1
    };

    return {
      {
        phaseEnds[0],
        phaseEnds[1] - phaseEnds[0],
        phaseEnds[2] - phaseEnds[1],
        phaseEnds[3] - phaseEnds[2]
      },
      move(phaseEnds)
    };
  }

  float getAmpForParamsUnita(float pos, float c1, float c2, float pw, float ew) {
  	auto phaseInfo = createPhaseInfo(pw, ew);
  	return getAmpFromPhaseInfo(pos, phaseInfo, c1, c2);
//    float ratio;
//    bool isC1 = true;
//    bool flip = false;
//    bool negative = false;
//    if (pos < pw) {
//      auto expWidth = ew * pw;
//      if (pos < expWidth) { // Q1
//        ratio = pos/expWidth;
//      } else { // Q2
//        pos -= expWidth;
//        auto conWidth = pw - expWidth;
//        ratio = pos/conWidth;
//        isC1 = false;
//        flip = true;
//      }
//
//    } else {
//      pos -= pw;
//      pw = 1 - pw;
//      auto expWidth = (1-ew) * pw;
//      if (pos < expWidth) { // Q3
//        ratio = pos/expWidth;
//        isC1 = false;
//        negative = true;
//      } else { // Q4
//        pos -= expWidth;
//        auto conWidth = pw - expWidth;
//        ratio = pos/conWidth;
//        negative = true;
//        flip = true;
//      }
//    }
//
//    if (flip) { ratio = 1 - ratio; }
//    auto curve = isC1 ? c1 : c2;
//    auto amp = getWaveSampleUnita(curve, ratio);
//    if (negative) { amp *= -1; }
//
//    return amp;
  }

  void convertAngleDeltasToAmps(WaveRenderingBuffer& modMe, float c1, float c2, float pw, float ew) {
    auto phaseInfo = createPhaseInfo(pw, ew);

    int rawIndex = 0;
    auto& raw = modMe.raw();
    for (int s = 0; s < modMe.numSamples; s++) {
      for (auto w = 0; w < modMe.numWaves; w++) {
        if (modMe.hasWave[w]) {
          raw[rawIndex] = getAmpFromPhaseInfo(raw[rawIndex], phaseInfo, c1, c2);
        }
        rawIndex += modMe.numChannels;
      }
    }
  }

  float getAmpFromPhaseInfo(float pos, PhaseInfo& phaseInfo, float curve1, float curve2) {
    assert(pos <= 1 && pos >= 0);
    if (pos == 1) { return getWaveSampleUnita(curve1, pos); }

    auto phase = 0;
    auto& phaseEnds = phaseInfo.ends;
    auto& phaseWidths = phaseInfo.widths;
    while (pos >= phaseEnds[phase]) { phase++; }
		
		if (phase > 0) { pos -= phaseEnds[phase-1]; }
		assert(phaseWidths[phase] != 0);
		pos /= phaseWidths[phase];
		
    if (phase%2 == 1) { pos = 1 - pos; }

    pos = getWaveSampleUnita(
      (phase%3 == 0) ? curve1 : curve2,
      pos
    );
    if (phase >= 2) { pos *= -1; }
    return pos;
  }

  float getWaveSampleUnita(float curveKnob, float sampleUnita) const {
    // if (curveKnob == 0) { return sampleUnita; }
    // else if (curveKnob >= 100) { return 1.0; }
    // else if (curveKnob <= -100) { return 0; }
    return waveLookup[(int)(curveKnob) + 100][sampleUnita];
    // return sampleUnita;
  }

  vector<TC_LookupTable> waveLookup;
  TC_LookupTable curveMap = {
    [] (float x) {
      if (x >= 1) { return 100000.0; }
      // (log((1-x)/2)/log(1-((1-x)/2))^.762207
      double mx = (1-x)/2.0;
      double nume = log(mx);
      double denom = log(1-mx);
      return pow(nume / denom, .762207);
    }, 
    100,
    0.0f, 1.0f
  };
};
