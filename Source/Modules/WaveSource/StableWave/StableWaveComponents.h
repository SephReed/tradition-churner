#pragma once
using namespace std;


//************** STABLE WAVE DETUNE EDITOR **************************/

class DetuneEditor: public Component {
public:
	DetuneEditor(StableWaveSynth* synth)
	: synth(synth),
		detune(synth->detune.createRotary()),
		spread(synth->spread.createRotary()),
		hiWidth(synth->upperWidth.createRotary()),
		loWidth(synth->lowerWidth.createRotary())
	{
		addAndMakeVisible(detune.get());
		addAndMakeVisible(spread.get());
		addAndMakeVisible(hiWidth.get());
		addAndMakeVisible(loWidth.get());
	}

	void paint (Graphics& g) override {
//		g.fillAll(Colour(250, 40, 40));
	}
	void resized() override {
		auto halfWidth = getWidth()/2.0;
		auto halfHeight = getHeight()/2.0;
		auto pad = 3;

		auto bigKnob = BIG_KNOB_DIAMETER;
		auto medKnob = MED_KNOB_DIAMETER;
		detune->setBounds(pad, pad, bigKnob, bigKnob);
		spread->setBounds(pad, halfHeight + pad, bigKnob, bigKnob);
		hiWidth->setBounds(halfWidth+pad, halfHeight + pad, medKnob, medKnob);
		loWidth->setBounds(halfWidth+pad, (1.5*halfHeight) + pad, medKnob, medKnob);
	}

protected:
	StableWaveSynth* synth;
	unique_ptr<Slider> detune;
	unique_ptr<Slider> spread;
	unique_ptr<Slider> hiWidth;
	unique_ptr<Slider> loWidth;
};



//************** STABLE WAVE CHORD EDITOR **************************/

class ChordEditor: public Component {
public:
	ChordEditor(StableWaveSynth* synth)
	: synth(synth),
		chordMix(synth->upperMix.createRotary()),
		chord(synth->upper.createRotary())
	{
		addAndMakeVisible(chord.get());
		addAndMakeVisible(chordMix.get());
	}

	void paint (Graphics& g) override {
//		g.fillAll(Colour(250, 40, 40));
	}
	void resized() override {
		auto halfWidth = getWidth()/2.0;
//		auto halfHeight = getHeight()/2.0;
		auto pad = 3;

		auto bigKnob = BIG_KNOB_DIAMETER;
		auto medKnob = MED_KNOB_DIAMETER;
		chord->setBounds(pad, pad, bigKnob, bigKnob);
		chordMix->setBounds(halfWidth+pad, pad, medKnob, medKnob);
	}

protected:
	StableWaveSynth* synth;
	unique_ptr<Slider> chordMix;
	unique_ptr<Slider> chord;
};




//************** STABLE WAVE MINI VIEW **************************/

// class StableWaveMiniView : public Component, private Slider::Listener {
// public:
// 	StableWaveMiniView(StableWaveSynth* i_synth);
// 	StableWaveSynth* synth;

// 	Slider amp;
// 	Slider pan;
// 	Slider tune;
// 	Component* waveGraph;

// 	void paint (Graphics& g) override;
// 	void resized() override;

// private:
// 	void sliderValueChanged (Slider* slider) override;
// };
