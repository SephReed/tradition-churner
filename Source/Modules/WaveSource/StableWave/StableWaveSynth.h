#pragma once
#import <vector>
using namespace std;


//************** MODULATION TARGETS **************************/
#import "../../Routing/UserInput/ModulationTarget.h"
#import "../../../Util/DesignatedInit.h"
#import "../../Routing/RegisterTargetsInRoutingManager.h"

struct StableWaveModTargets: public RegisterTargetsInRoutingManager<StableWaveModTargets> {

	static inline constexpr ModulationTarget upperMix = DesignatedInit(ModulationTarget,
    $.legacyId = "UPPER_MIX",
		$.fullname = "Voice Upper Mix",
		$.shortname = "Mix",
		$.canBePerVoice = true,
		$.min = 0,
		$.baseline = 0.5,
		$.defaultValue = 0.25,
	  $.max = 1
	);

	static inline constexpr ModulationTarget upper = DesignatedInit(ModulationTarget,
    $.legacyId = "UPPER_INTERVAL",
		$.fullname = "Voice Upper Interval",
		$.shortname = "Upper",
		$.canBePerVoice = true,
		$.min = 0,
		$.baseline = 1,
		$.defaultValue = 1.5,
		$.max = 16
	);

	static inline constexpr ModulationTarget detune = DesignatedInit(ModulationTarget,
    $.legacyId = "DETUNE",
		$.fullname = "Voice Detuning Interval",
		$.shortname = "Detune",
		$.canBePerVoice = true,
		$.min = 1,
		$.baseline = 1,
		$.defaultValue = 1.01,
	  $.max = 1.1,
	  $.grainularity = .001
	);


	static inline constexpr ModulationTarget spread = DesignatedInit(ModulationTarget,
    $.legacyId = "DETUNE_SPREAD",
		$.fullname = "Voice Detuning Spread Range",
		$.shortname = "Spread",
		$.canBePerVoice = true,
		$.min = -1,
		$.baseline = 0,
		$.defaultValue = 0.33,
	  $.max = 1
	);

	static inline constexpr ModulationTarget lowerWidth = DesignatedInit(ModulationTarget,
    $.legacyId = "LOWER_WIDTH_UNITA",
		$.fullname = "Voice Lower Width (Unita)",
		$.shortname = "Low",
		$.canBePerVoice = true,
		$.min = -1,
		$.baseline = 0,
		$.defaultValue = 0.5,
	  $.max = 1
	);


	static inline constexpr ModulationTarget upperWidth = DesignatedInit(ModulationTarget,
    $.legacyId = "UPPER_WIDTH_UNITA",
		$.fullname = "Voice Upper Width (Unita)",
		$.shortname = "High",
		$.canBePerVoice = true,
		$.min = -1,
		$.baseline = 0,
		$.defaultValue = 1,
	  $.max = 1
	);

	static inline std::vector<const ModulationTarget*>& modTargetList() {
		static std::vector<const ModulationTarget*> output = {
      &upperMix,
			&upper,
			&detune,
			&spread,
			&lowerWidth,
			&upperWidth
    };
		return output;
	};
};



//************** STABLE WAVE SOUND **************************/
#import "../../../../JuceLibraryCode/JuceHeader.h"

class StableWaveSound : public SynthesiserSound {
	public:
    StableWaveSound() {}
    bool appliesToNote (int midiNoteNumber) override { return true; }
    bool appliesToChannel (int midiChannel) override { return true; }
};


//************** STABLE WAVE SYNTH **************************/
#import "../WaveSource.h"
// #import "../../Cropper/Cropper.h"
#import "../../ProcessingLayer/ProcessingLayer.h"
#import "../../../Components/Grapher.h"


class DetuneEditor;
using WaveSamples = vector<vector<vector<float>>>;

class StableWaveSynth
: public WaveSource
	// public RegisterTargetsInRoutingManager<StableWaveSynth>
{
public:
	static inline StableWaveModTargets& modTargets() {
    static auto _modTargets = new StableWaveModTargets();
    return *_modTargets;
  }
	
	StableWaveSynth(PluginProcessor* i_plugin, int sourceNum);
	virtual ~StableWaveSynth(){}

	UserInputMod upperMix;
	UserInputMod upper;
	UserInputMod detune;
	UserInputMod spread;
	UserInputMod lowerWidth;
	UserInputMod upperWidth;

	vector<UserInputMod*> userInputs = {
		&upperMix,
		&upper,
		&detune,
		&spread,
		&lowerWidth,
		&upperWidth
	};

	vector<ProcessingLayerFactory*> ampFxFactories;

	
	Observable<bool> detuneSmear = {true};

	// void addCropper();
	virtual float getAmpUnita(float ratio) {
		return cos(ratio * 3.1415 * 2);
	}

	Component* createGraph() {
		return new Grapher([this](float ratio){
			return this->getAmpUnita(ratio);
		});
	}

protected:
//	Cropper* cropper;
	NumberedVoice* createVoice(int voiceNum) override;
};


//************** STABLE WAVE DEPENDENTS **************************/

#import "StableWaveVoice.h"
#import "StableWaveComponents.h"


