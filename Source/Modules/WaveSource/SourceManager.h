#pragma once
using namespace std;


//************** MINI OVERVIEWS **************************/
#import "../../Util/SectionedRectangle.h"

class MiniOverviews : public Component {
public:
	class AddItem: public Component {
		void paint (Graphics& g) override {
			auto bounds = getLocalBounds();
//			 g.fillAll(Colour(150, 40, 100));
			auto radius = 20;
			auto strokeWidth = 5;
			auto x = bounds.getCentreX();
			auto y = bounds.getCentreY();
			g.setColour(Colours::black);
			g.drawLine(x-radius, y, x+radius, y, strokeWidth);
			g.drawLine(x, y-radius, x, y+radius, strokeWidth);
		}
	};

	vector<unique_ptr<Component>> miniViews;
	int numRows;
	int numCols;
	int numCells;

	MiniOverviews(int i_rows = 2, int i_cols = 2)
	: numCells(i_rows * i_cols),
//		miniViews(numCells),
		numRows(i_rows),
		numCols(i_cols)
	{
		for (int c = 0; c < numCells; c++) {
			miniViews.push_back(make_unique<AddItem>());
		}
		for (auto& view : miniViews) {
			addAndMakeVisible(view.get());
		}
	}


	void paint (Graphics& g) override {
//		auto cells = getCells();
//
//		for (int c = 0; c < cells.size(); c++) {
//			g.drawRect(cells[c]);
//		}
		auto bounds = getLocalBounds();
		auto rowHeight = bounds.getHeight()/numRows;
		auto colWidth = bounds.getWidth()/numCols;
		
		auto pad = 10;
		auto horizontalLineLength = colWidth - (2 * pad);
		auto verticalLineLength = rowHeight - (2 * pad);
		
		g.setColour(Colours::black);
		for (int s = 0; s < numCells; s++) {
			auto row = (int) s/numCols;
			auto col = s % numCols;
			if (row > 0) {
				auto vy = rowHeight * row;
				auto vx = (col * colWidth) + pad;
				g.drawLine(vx, vy, vx+horizontalLineLength, vy, 1);
			}
			auto hx = colWidth * col;
			auto hy = (row * rowHeight) + pad;
			g.drawLine(hx, hy, hx, hy+verticalLineLength, 1);
		}
	}

	void resized() override {
		auto cells = getCells();
		for (int s = 0; s < miniViews.size(); s++) {
			Component* boundMe = miniViews[s].get();
			if (boundMe != NULL) {
				boundMe->setBounds(cells[s]);
			}
		}
	}

	vector<Rectangle<int>> getCells() {
		SectionedRectangle bounds(getLocalBounds());
		auto rowsBoxes = bounds.splitRows(numRows, 3);
		auto row1cols = rowsBoxes[0].splitCols(numCols, 3);
		auto row2cols = rowsBoxes[1].splitCols(numCols, 3);
		vector<Rectangle<int>> cells (numRows*numCols);
		for (int s = 0; s < 4; s++) {
			cells[s] = (s < 2) ? row1cols[s] : row2cols[s%2];
		}
		return cells;
	}
};



//************** SOURCE MANAGER **************************/
#import <map>
#import "NumberedVoiceSynth.h"
#import "../Master/Master.h"
#import "../../Components/TabPaneViewer.h"
#import "../../Util/Observable.h"
#import "../../../JuceLibraryCode/JuceHeader.h"

class PluginProcessor;
class WaveSource;
class SourceFactory;

class SourceManager : public NumberedVoiceSynth {
public:
	static const int MAX_VOICES = 16;
	static const int NUM_SOURCES = 4;
	static vector<const SourceFactory*>& sourceFactories();

	PluginProcessor* plugin;
	vector<WaveSource*> sources {NUM_SOURCES};

	SourceManager(PluginProcessor* plugin);

	void setCurrentPlaybackSampleRate(int sampleRate);
	void renderNextBlock (
		AudioBuffer<float>& buffer,
		const MidiBuffer& midiMessages,
		int startSample,
		int numSamples
	);

	Component* getEditor();

	void updateVoiceCount(int newCount) override;
	void renderVoices (AudioBuffer<float>& buffer, int startSample, int numSamples) override {
		// do nothing
	}

	void startSourceVoices (int voiceNum, int midiNoteNumber, float velocity);
	void stopSourceVoices (int voiceNum, float velocity, bool allowTailOff);


protected:
	TabPaneViewer* editor = NULL;
	Component emptySource;
	MiniOverviews miniOverviews;

	NumberedVoice* createVoice(int voiceNum) override;
};


//************** SOURCE MANAGER SOUND **************************/

class SourceManagerSound : public SynthesiserSound {
	bool appliesToNote (int midiNoteNumber) override { return true; }
	bool appliesToChannel (int midiChannel) override { return true; }
};


//************** SOURCE MANAGER VOICE **************************/

class SourceManagerVoice : public NumberedVoice {
public:
	SourceManager* sourceManager;
	SourceManagerVoice(SourceManager* sourceManager, int voiceNum)
	: NumberedVoice(voiceNum), 
		sourceManager(sourceManager) 
	{};

	void startNote(int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition) override {
		sourceManager->startSourceVoices(voiceNum, midiNoteNumber, velocity);
	}
	
	void stopNote(float velocity, bool allowTailOff) override {
		sourceManager->stopSourceVoices(voiceNum, velocity, allowTailOff);
	}
};


//************** SOURCE MANAGER DEPENDENTS **************************/
#import "StaticallyRegisterInSourceManager.h"
#import "WaveSource.h"
