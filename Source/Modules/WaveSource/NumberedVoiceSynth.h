#pragma once

//************** NUMBERED VOICE **************************/
#import "../../../JuceLibraryCode/JuceHeader.h"

class NumberedVoice : public SynthesiserVoice {
public:
	int voiceNum;
	NumberedVoice(int voiceNum) :	voiceNum(voiceNum) {}
	bool canPlaySound(juce::SynthesiserSound *) override { return true; }
	
	void startNote(int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition) override { assert(false); }
	void stopNote(float velocity, bool allowTailOff) override { assert(false); }
	void pitchWheelMoved(int newPitchWheelValue) override { assert(false); }
	void controllerMoved(int controllerNumber, int newControllerValue) override { assert(false); }
	void renderNextBlock(AudioBuffer<float> &outputBuffer, int startSample, int numSamples) override { assert(false); }
};


//************** NUMBERED VOICE SYNTH **************************/

class NumberedVoiceSynth
	: public Synthesiser
{
public:
	virtual void updateVoiceCount(int newCount) {
		int voiceCount = getNumVoices();
		if (newCount < voiceCount) {
			for (int v = voiceCount-1; v >= newCount; v--) {
				getVoice(v)->stopNote(0.0f, false);
				removeVoice(v);
			}
		} else if (newCount > voiceCount) {
			for (int v = voiceCount; v < newCount; v++) {
				addVoice(createVoice(v));
			}
		}
	}
protected:
	virtual NumberedVoice* createVoice(int voiceNum) = 0;
};
