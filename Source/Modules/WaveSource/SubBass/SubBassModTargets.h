#pragma once

#import "../../Routing/UserInput/ModulationTarget.h"
#import "../../../Util/DesignatedInit.h"
#import "../../Routing/RegisterTargetsInRoutingManager.h"

struct SubBassModTargets : public RegisterTargetsInRoutingManager<SubBassModTargets> {
	static inline constexpr ModulationTarget oct1Mix = DesignatedInit(ModulationTarget,
    $.legacyId = "OCT_MIX_1",
    $.fullname = "Octave Mix 1",
    $.shortname = "Oct 1",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0,
    $.defaultValue = 0.1,
    $.max = 1
  );

  static inline constexpr ModulationTarget oct2Mix = DesignatedInit(ModulationTarget,
    $.legacyId = "OCT_MIX_2",
    $.fullname = "Octave Mix 2",
    $.shortname = "Oct 2",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0,
    $.defaultValue = 0.01,
    $.max = 1
  );

  static inline constexpr ModulationTarget oct3Mix = DesignatedInit(ModulationTarget,
    $.legacyId = "OCT_MIX_3",
    $.fullname = "Octave Mix 3",
    $.shortname = "Oct 3",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0,
    $.defaultValue = 0,
    $.max = 1
  );

  static inline constexpr ModulationTarget rootAmMix = DesignatedInit(ModulationTarget,
    $.legacyId = "ROOT_AM_MIX",
    $.fullname = "Root Amplitude Modulation Mix",
    $.shortname = "Root AM",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0,
    $.defaultValue = 0,
    $.max = 1
  );

  static inline constexpr ModulationTarget amOct = DesignatedInit(ModulationTarget,
    $.legacyId = "AM_OCT",
    $.fullname = "Amplitude Modulation Octave",
    $.shortname = "AM Oct",
    $.canBePerVoice = true,
    $.min = -3,
    $.baseline = 0,
    $.defaultValue = -1,
    $.max = 0,
    $.grainularity = 1
  );

	static inline vector<const ModulationTarget*>& modTargetList() {
    static vector<const ModulationTarget*> out = {
      &oct1Mix,
      &oct2Mix,
      &oct3Mix,
      &rootAmMix,
      &amOct
    };
		return out;
	};
};
