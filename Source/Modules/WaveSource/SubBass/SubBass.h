#pragma once

// Brain Dump
// Ability to mix in 1-4 Amp Modulated Octaves
// Frequency Bounds
// Bounding Methods - shortest interval, most centered, none
// Basic SmoothWave layout
// AM Mod tune - controls amps of all waves

// /************** SUB BASS **************************/
#import "../WaveSource.h"
#import "../StaticallyRegisterInSourceManager.h"
#import "../StableWave/SmoothWave/SmoothWaveModTargets.h"
#import "../StableWave/SmoothWave/SmoothWaveTables.h"
#import "../WaveRenderingBuffer.h"
#import <math.h>
#import "SubBassModTargets.h"

class SubBass 
: public WaveSource,
	public StaticallyRegisterInSourceManager<SubBass> {
public:
	static inline SmoothWaveTables waveTable = {};
	class MyFactory : public SourceFactory {
	public:
	  WaveSource* create(PluginProcessor* i_plugin, int sourceNum) const final { 
	    return new SubBass(i_plugin, sourceNum); 
	  }
	};
	static inline MyFactory factory = {};
	static float amOctToRatio(int octDrop) {
		return pow(0.5, abs(octDrop));
	}

	UserInputMod pulseWidth {SmoothWaveModTargets::pulseWidth, *this};
  UserInputMod expWidth {SmoothWaveModTargets::expWidth, *this};
  UserInputMod curve1 {SmoothWaveModTargets::curve1, *this};
  UserInputMod curve2 {SmoothWaveModTargets::curve2, *this};

  UserInputMod amOct {SubBassModTargets::amOct, *this};
  UserInputMod rootAmMix {SubBassModTargets::rootAmMix, *this};
  UserInputMod oct1Mix {SubBassModTargets::oct1Mix, *this};
  UserInputMod oct2Mix {SubBassModTargets::oct2Mix, *this};
  UserInputMod oct3Mix {SubBassModTargets::oct3Mix, *this};

	SubBass(PluginProcessor* i_plugin, int sourceNum) 
	: WaveSource(i_plugin, sourceNum)
	{}

	float getAmpUnita(float pos) {
		auto amRatio = amOctToRatio(amOct.get());
		auto amPos = fmod(pos * amRatio, 1.0f);
		pos /= min(amRatio, 1.0f);
		vector<float> mix = { 1, (float)oct1Mix.get(), (float)oct2Mix.get(), (float)oct3Mix.get()};
		vector<float> angles = {pos, pos*2, pos*4, pos*8};
		float out = 0;
		float span = 0;
		float amGain = abs(SubBass::waveTable.getAmpForParamsUnita(
			amPos,
			curve1.get(), curve2.get(),
			pulseWidth.get(), expWidth.get()
		));
		for (int w = 0; w < 4; w++) {
			out += SubBass::waveTable.getAmpForParamsUnita(
				fmod(angles[w], 1.0f),
				curve1.get(), curve2.get(),
				pulseWidth.get(), expWidth.get()
			) * mix[w] * amGain;
			span += mix[w];
		}

		return out/span;
	}

	Component* getFullView() override; //bottom
	NumberedVoice* createVoice(int voiceNum) override; //bottom

};


// /************** SUB BASS VOICE **************************/

class SubBassVoice : public WaveSourceVoice {
public:
	SubBass* subBass;
	struct WaveState {
		float pos = 0;
		float delta = 0;
	};

	enum WaveNum {
		ROOT = 0,
		OCT1,
		OCT2,
		OCT3,
		AM
	};

	// WaveState AM;
	// WaveState root;
	// WaveState oct1;
	// WaveState oct2;
	// WaveState oct3;

	vector<WaveState> waveStates {5};

	float amAngleDelta = 0;
	float angleDelta = 0;

	WaveRenderingBuffer renderBuffer;

	SubBassVoice(SubBass* i_synth, int voiceNum)
	: WaveSourceVoice(i_synth, voiceNum),
		renderBuffer(5),
		subBass(i_synth) 
	{}

	void stopNote(float velocity, bool allowTailOff) override { }

	void updateCurrentHz(int midiNum) override {
		WaveSourceVoice::updateCurrentHz(midiNum);
//		while (currentHz > 62) { currentHz /= 2; }
//		if (currentHz < 38) { currentHz *= 1.5; }
		// currentHzAngleDelta = currentHz / getSampleRate();
		auto rootDelta = currentHz / getSampleRate();
		waveStates[WaveNum::ROOT].delta = rootDelta;
		waveStates[WaveNum::OCT1].delta = rootDelta * 2;
		waveStates[WaveNum::OCT2].delta = rootDelta * 4;
		waveStates[WaveNum::OCT3].delta = rootDelta * 8;

		auto amRatio = pow(0.5, abs(subBass->amOct.get()));
		waveStates[WaveNum::AM].delta = rootDelta * amRatio;
	}

	void renderNextWavesToBlock(AudioBuffer<float>& outputBuffer, int numSamples) override {
		auto voiceAmpSamples = voiceAmps.getNextSamplesReadonly(numSamples);
		if (voiceAmpSamples->canBeIgnored(0.0)) { return; }

		renderBuffer.resizeSamples(numSamples, 1);

		auto& hasWave = renderBuffer.hasWave;
		hasWave[WaveNum::ROOT] = true;
		hasWave[WaveNum::OCT1] = subBass->oct1Mix.get() != 0;
		hasWave[WaveNum::OCT2] = subBass->oct2Mix.get() != 0;
		hasWave[WaveNum::OCT3] = subBass->oct3Mix.get() != 0;
		hasWave[WaveNum::AM] = subBass->amOct.get() != 0 && (
			subBass->rootAmMix.get() != 0 || hasWave[WaveNum::OCT1] || hasWave[WaveNum::OCT2] || hasWave[WaveNum::OCT3]
		);

		fillRenderBufferWithDeltas(renderBuffer);
		convertDeltasToAmps(renderBuffer);
		applyAmpEnvelope(renderBuffer, voiceAmpSamples);
		applyMixAndAM(renderBuffer);
		mixToOutput(outputBuffer, renderBuffer);
	}

	void fillRenderBufferWithDeltas(WaveRenderingBuffer& buffer) {
		auto& raw = buffer.raw();
		for (int w = 0; w < buffer.numWaves; w++) {
			if (buffer.hasWave[w]) {
				// TODO: make an iterator that does this
				auto iterSetup = buffer.waveIterationSetup(w);
				for (int s = iterSetup.rawStart; s < iterSetup.rawLimit; s += iterSetup.rawStepSize) {
					auto& state = waveStates[w];
					raw[s] = state.pos;
					state.pos += state.delta;
					state.pos -= (int)state.pos;
				}
			}
		}
	}

	void convertDeltasToAmps(WaveRenderingBuffer& buffer) {
		SubBass::waveTable.convertAngleDeltasToAmps(
			buffer, 
			subBass->curve1.get(),
			subBass->curve2.get(),
			subBass->pulseWidth.get(),
			subBass->expWidth.get()
		);
	}

	void applyMixAndAM(WaveRenderingBuffer& buffer) {
		auto amIter = buffer.waveIterationSetup((subBass->amOct.get() != 0) ? WaveNum::AM : WaveNum::ROOT);
		auto& raw = buffer.raw();
		for (int w = 0; w < buffer.numWaves; w++) {
			if (buffer.hasWave[w] && w != WaveNum::AM && w != WaveNum::ROOT) {
				float mix;
				switch (w) {
					case WaveNum::OCT1: mix = subBass->oct1Mix.get(); break;
					case WaveNum::OCT2: mix = subBass->oct2Mix.get(); break;
					case WaveNum::OCT3: mix = subBass->oct3Mix.get(); break;
				}

				// TODO: make an iterator that does this
				auto amSample = amIter.rawStart;
				auto iterSetup = buffer.waveIterationSetup(w);
				for (int s = iterSetup.rawStart; s < iterSetup.rawLimit; s += iterSetup.rawStepSize) {
					raw[s] *= mix * ((raw[amSample]/2) + 1);
					amSample += amIter.rawStepSize;
				}
			}
		}

		float mix = mix = subBass->rootAmMix.get();
		if (mix != 0) {
			auto rootIter = buffer.waveIterationSetup(WaveNum::ROOT);
			auto amSample = amIter.rawStart;
			for (int s = rootIter.rawStart; s < rootIter.rawLimit; s += rootIter.rawStepSize) {
				auto amMixin = raw[s] * mix * raw[amSample];
				raw[s] *= (1 - mix);
				raw[s] += amMixin;
				amSample += amIter.rawStepSize;
			}
		}
	}

	void mixToOutput(AudioBuffer<float>& output, WaveRenderingBuffer& buffer) {
		reduceToRoot(buffer);
		auto& raw = buffer.raw();
		auto rootIter = buffer.waveIterationSetup(WaveNum::ROOT);
		auto rootSample = rootIter.rawStart;
		for (int s = 0; s < buffer.numSamples; s++) {
			for (auto c = output.getNumChannels(); --c >= 0;) {
				output.addSample(c, s, raw[rootSample]);
			}
			rootSample += rootIter.rawStepSize;
		}
	}

	void reduceToRoot(WaveRenderingBuffer& buffer) {
		auto rootIter = buffer.waveIterationSetup(WaveNum::ROOT);
		auto& raw = buffer.raw();
		for (int w = 0; w < buffer.numWaves; w++) {
			if (buffer.hasWave[w] && w != WaveNum::ROOT && w != WaveNum::AM) {
				// TODO: make an iterator that does this
				auto rootSample = rootIter.rawStart;
				auto iterSetup = buffer.waveIterationSetup(w);
				for (int s = iterSetup.rawStart; s < iterSetup.rawLimit; s += iterSetup.rawStepSize) {
					raw[rootSample] += raw[s];
					rootSample += rootIter.rawStepSize;
				}
			}
		}
	}
};


// /************** SUB BASS FULL VIEW **************************/
#import "../StableWave/SmoothWave/CurveEditor.h"
#import "../../../Components/Grapher.h"

class SubBassFullView : public Component {
public:
	SubBass& synth;
	SmoothWaveCurveEditor curveEditor;
	Grapher waveGraph;
	unique_ptr<Slider> amOct;
	unique_ptr<Slider> rootAmMix;
	unique_ptr<Slider> oct1Mix;
	unique_ptr<Slider> oct2Mix;
	unique_ptr<Slider> oct3Mix;

	SubBassFullView(SubBass& synth)
	:	synth(synth),
		curveEditor(
			synth.pulseWidth.createHorizontalFader(),
			synth.expWidth.createHorizontalFader(),
			synth.curve1.createRotary(),
			synth.curve2.createRotary()
		),
		waveGraph([&](float pos) {
		 	return synth.getAmpUnita(pos);
		}),
		amOct(synth.amOct.createRotary()),
		rootAmMix(synth.rootAmMix.createRotary()),
		oct1Mix(synth.oct1Mix.createRotary()),
		oct2Mix(synth.oct2Mix.createRotary()),
		oct3Mix(synth.oct3Mix.createRotary())
	{
		addAndMakeVisible(curveEditor);
		addAndMakeVisible(waveGraph);
		addAndMakeVisible(amOct.get());
		addAndMakeVisible(rootAmMix.get());
		addAndMakeVisible(oct1Mix.get());
		addAndMakeVisible(oct2Mix.get());
		addAndMakeVisible(oct3Mix.get());

		auto doRepaint = [&](float){
  		const MessageManagerLock mmLock;
	  	waveGraph.repaint();
		};
		synth.pulseWidth.observe(doRepaint, false);
		synth.expWidth.observe(doRepaint, false);
		synth.curve1.observe(doRepaint, false);
		synth.curve2.observe(doRepaint, false);
		synth.amOct.observe(doRepaint, false);
		synth.rootAmMix.observe(doRepaint, false);
		synth.oct1Mix.observe(doRepaint, false);
		synth.oct2Mix.observe(doRepaint, false);
		synth.oct3Mix.observe(doRepaint, false);

	}

	void resized() override {
		auto pad = 3;

	  SectionedRectangle bounds(getLocalBounds());
	  bounds.padInwards(pad);

	  auto shapeAndLayers = bounds.splitHorizontallyAtRatio(5.0/6.0, pad);

	  auto graphAndInputs = shapeAndLayers.left.splitVerticallyAtRatio(0.5, pad);
		waveGraph.setBounds(graphAndInputs.top);
		curveEditor.setBounds(graphAndInputs.bottom);

		auto layerKnobs = shapeAndLayers.right.splitRows(5);
		amOct->setBounds(layerKnobs[0]);
		rootAmMix->setBounds(layerKnobs[1]);
		oct1Mix->setBounds(layerKnobs[2]);
		oct2Mix->setBounds(layerKnobs[3]);
		oct3Mix->setBounds(layerKnobs[4]);
//		sync.setBounds(0, 150, 40, 40);
	}


};



// /************** CIRCULAR DEPENDENCIES **************************/

inline Component* SubBass::getFullView() { 
	return new SubBassFullView(*this); 
}


inline NumberedVoice* SubBass::createVoice(int voiceNum) {
	return new SubBassVoice(this, voiceNum);
}
