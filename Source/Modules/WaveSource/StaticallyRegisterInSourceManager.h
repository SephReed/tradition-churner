#pragma once
using namespace std;

#include "SourceManager.h"
#include "WaveSource.h"
#include "../../Util/StaticallyRun.h"


class SourceFactory {
public:
	virtual ~SourceFactory() {}
  virtual WaveSource* create(PluginProcessor* i_plugin, int sourceNum) const = 0;
};


template<class CREATOR_TYPE>
class StaticallyRegisterInSourceManager {
public:
  struct StaticRunProxy {
    StaticRunProxy() {
      SourceManager::sourceFactories().push_back(&(CREATOR_TYPE::factory));
    }
  };

  static StaticRunProxy staticProxy;
  typedef ForceStaticProxyInit<StaticRunProxy&, staticProxy> __dummy;
} ;

template<class CREATOR_TYPE>
typename StaticallyRegisterInSourceManager<CREATOR_TYPE>::StaticRunProxy StaticallyRegisterInSourceManager<CREATOR_TYPE>::staticProxy;

