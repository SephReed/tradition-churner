#import "WaveSource.h"
#import "../Modifier/ModSampleSource/ModSampleSource.h"
#import "../../MasterEnums.h"
#import "../../PluginProcessor.h"

using namespace std;

// /************** MANAGER **************************/
#import "SourceManager.h"

vector<const SourceFactory*>& SourceManager::sourceFactories() {
	static vector<const SourceFactory*>* staticSourceFactories = new vector<const SourceFactory*>();
 	return *staticSourceFactories;
}

SourceManager::SourceManager(PluginProcessor* plugin): plugin(plugin) {
	// WaveSource::registerTargets(plugin->routingManager);

	// sources[0] = NULL;
	sources[0] = SourceManager::sourceFactories()[0]->create(plugin, 0);
	sources[1] = SourceManager::sourceFactories()[1]->create(plugin, 1);
	// sources[1] = NULL;
	sources[2] = NULL;
	sources[3] = NULL;

	addSound(new SourceManagerSound());
}

void SourceManager::setCurrentPlaybackSampleRate(int sampleRate) {
	Synthesiser::setCurrentPlaybackSampleRate(sampleRate);
	for (int i = 0; i < SourceManager::NUM_SOURCES; i++) {
		if (sources[i] != NULL) {
			sources[i]->setCurrentPlaybackSampleRate(sampleRate);
		}
	}
}

void SourceManager::updateVoiceCount(int newCount) {
	NumberedVoiceSynth::updateVoiceCount(newCount);
	for (auto source : sources) {
		if (source != NULL) {
			source->updateVoiceCount(newCount);
		}
	}
}

void SourceManager::renderNextBlock (
	AudioBuffer<float>& buffer,
	const MidiBuffer& midiMessages,
	int startSample,
	int numSamples
) {
	Synthesiser::renderNextBlock(buffer, midiMessages, startSample, numSamples);
	const MidiBuffer emptyBuffer;
	for (int i = 0; i < SourceManager::NUM_SOURCES; i++) {
		if (sources[i] != NULL) {
			sources[i]->renderNextBlock(buffer, emptyBuffer, startSample, numSamples);
		}
	}
}

Component* SourceManager::getEditor() {
	if (editor == NULL) {
		editor = new TabPaneViewer();
		editor->addPane("All Sources", &miniOverviews);
	   editor->addPane("Source A", sources[0]->getFullView());
	  // editor->addPane("Source A", &emptySource);
	  editor->addPane("Source B", sources[1]->getFullView());
	  // editor->addPane("Source B", &emptySource);
	  editor->addPane("Source C", &emptySource);
	  editor->addPane("Source D", &emptySource);
	  editor->selectIndex(1);
	}
	return editor;
}

NumberedVoice* SourceManager::createVoice(int voiceNum) {
	return new SourceManagerVoice(this, voiceNum);
}

void SourceManager::startSourceVoices (
	int voiceNum,
	int midiNoteNumber,
	float velocity
) {
	plugin->routingManager.retriggerVoiceModChains(voiceNum);
	for (int i = 0; i < NUM_SOURCES; i++) {
		auto source = sources[i];
		if (source != NULL) {
			source->getVoice(voiceNum)->startNote(midiNoteNumber, velocity, NULL, 0);
		}
	}
}	

	
void SourceManager::stopSourceVoices (int voiceNum, float velocity, bool allowTailOff) {
	plugin->routingManager.releaseVoiceModChains(voiceNum);
	for (int i = 0; i < NUM_SOURCES; i++) {
		if (sources[i] != NULL) {
			sources[i]->getVoice(voiceNum)->stopNote(velocity, allowTailOff);
		}
	}
	bool allNotesReleased = true;
	for (int v = 0; allNotesReleased && v < voices.size(); v++) {
		allNotesReleased = !(voices[v]->isKeyDown());
	}
	if (allNotesReleased) {
		plugin->routingManager.releaseVoicelessModChains();
	}
}



// /************** WAVE SOURCE **************************/

WaveSource::WaveSource(PluginProcessor* i_plugin, int sourceNum)
: plugin(i_plugin), 
	sourceNum(sourceNum),
	gainPreFx(createUserInputMod(WaveSourceModTargets::gainPreFx))
{}

// void WaveSource::registerTargets(RoutingManager& routingMgmt) {
// 	routingMgmt.addTargets(WaveSource::modTargets().list);
// }

UserInputMod WaveSource::createUserInputMod(const ModulationTarget& modTarget) {
	return UserInputMod(modTarget, *this);
}



// /************** WAVE SOURCE VOICE **************************/

WaveSourceVoice::WaveSourceVoice(WaveSource* i_synth, int voiceNum)
:	NumberedVoice(voiceNum),
	voiceAmps(&WaveSourceModTargets::voiceAmps, i_synth->sourceNum, voiceNum),
	gainPreFx(i_synth->gainPreFx.createEndpoint(voiceNum)),
	synth(i_synth)
{
//	setEnpointsVoiceSynthNums(endpoints);
	synth->plugin->routingManager.registerEndpoints(endpoints);
}

void WaveSourceVoice::setEnpointsVoiceSynthNums(std::vector<ModulationEndpoint*> endpoints) {
	for (auto endpoint : endpoints) {
		endpoint->modChainLoc.voiceNum = voiceNum;
		endpoint->modChainLoc.synthNum = synth->sourceNum;
	}
}

WaveSource* WaveSourceVoice::getSynth() { return synth; }

bool WaveSourceVoice::isVoiceActive() const {
	return voiceAmps.endingIncomplete();
}

void WaveSourceVoice::startNote (
	int midiNoteNumber,
	float velocity,
	SynthesiserSound* sound,
	int /*currentPitchWheelPosition*/
) {
	updateCurrentHz(midiNoteNumber);
}

void WaveSourceVoice::updateCurrentHz(int midiNum) {
	currentHz = MidiMessage::getMidiNoteInHertz (midiNum);
	currentHzAngleDelta = currentHz / getSampleRate();
}

// TODO: quit ignoring channels
void WaveSourceVoice::renderNextBlock(AudioBuffer<float> &outputBuffer, int startSample, int numSamples) {
	if (isVoiceActive() == false) {
		clearCurrentNote();
		return;
	}

	if (buffer.getNumSamples() < numSamples) { buffer.setSize(outputBuffer.getNumChannels(), numSamples); }
	buffer.clear();

	// ---- SOURCE -----
	renderNextWavesToBlock(buffer, numSamples);


	// ---- PRE FX ----
	auto rawBuffer = buffer.getArrayOfWritePointers();
	// GAIN
	auto preGainModulation = gainPreFx.getNextSamplesReadonly(numSamples);
	if (preGainModulation->canBeIgnored() == false) {
		auto preGainUnitas = preGainModulation->map(
			[](float sample){ return Decibels::decibelsToGain(sample); }
		);
		for (auto c = buffer.getNumChannels(); --c >= 0;) {
			for (int s = 0; s < numSamples; s++) {
		  	rawBuffer[c][s] *= preGainUnitas->get(s);
			}
		}
	}

//	// PAN
//	auto prePanModulation = panPreFx.getNextSamplesReadonly(numSamples);
//	if (prePanModulation->canBeIgnored() == false) {
//		assert(buffer.getNumChannels() == 2);
//		for (int s = 0; s < numSamples; s++) {
//			auto pan = prePanModulation[s];
//			rawBuffer[0][s] *= fmin(1 - pan, 1.0);
//			rawBuffer[1][s] *= fmin(1 + pan, 1.0);
//		}
//	}

	// ---- FX ----
	// apply/mix fx

	// ---- POST FX ----
	// apply post fx gain (per voice) and pan (per voice)
	
	// ---- WRITE OUT ----
	for (auto c = outputBuffer.getNumChannels(); --c >= 0;) {
		auto myBuffer = buffer.getReadPointer(c);
		for (int s = 0; s < numSamples; s++) {
	  	outputBuffer.addSample(c, startSample+s, myBuffer[s]);	
		}
	}
}

void WaveSourceVoice::applyAmpEnvelope(WaveRenderingBuffer& modMe, ModBuffer& voiceAmpModulation) {
	int rawIndex = 0;
	auto& raw = modMe.raw();
	for (int s = 0; s < modMe.numSamples; s++) {
		auto gain = voiceAmpModulation->get(s);
		for (int w = 0; w < modMe.numWaves; w++) {
			if (modMe.hasWave[w]) {
				raw[rawIndex] *= gain;
			}
			rawIndex += modMe.numChannels;
		}
	}
}


void WaveSourceVoice::renderNextWavesToBlock(AudioBuffer<float> &outputBuffer, int numSamples) {
	assert(false);
}
