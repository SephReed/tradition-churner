#pragma once

class WaveRenderingBuffer {
public:
	int numSamples = 0;
	int numWaves = 0;
	int numChannels = 2;
	int sampleEntrySize = 0;
	vector<float> data;
	vector<bool> hasWave;

	struct RawIterationSetup {
		int rawStart;
		int rawStepSize;
		int rawLimit;
	};

	WaveRenderingBuffer(int i_numWaves = 4): numWaves(i_numWaves), hasWave(i_numWaves, false) {}

	void resizeSamples(int i_numSamples, int i_numChannels = 2) {
		numSamples = i_numSamples;
		numChannels = i_numChannels;
		sampleEntrySize = numWaves * numChannels;
		data.resize(numSamples * sampleEntrySize);
	}

	float get(int waveNum, int sampleNum, int channel) {
		return data[toIndex(waveNum, sampleNum, channel)];
	}

	void set(int waveNum, int sampleNum, int channel, float newVal) {
		data[toIndex(waveNum, sampleNum, channel)] = newVal;
	}

	vector<float>& raw() { return data; }

	RawIterationSetup waveIterationSetup(int waveNum) { 
		return {
			waveNum * numChannels,
			numWaves * numChannels,
			(int)data.size()
		};
	}

protected:
	int toIndex(int waveNum, int sampleNum, int channel) {
		return (sampleNum * sampleEntrySize) + (waveNum * numChannels) + channel;
	}
};
