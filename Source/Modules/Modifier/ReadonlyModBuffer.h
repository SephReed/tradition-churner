#pragma once
using namespace std;


//************** MOD BUFFER PTR **************************/
class ReadonlyModBuffer;

class ModBufferPtr : public unique_ptr<ReadonlyModBuffer> {
public:
	ModBufferPtr(ReadonlyModBuffer* ptr): unique_ptr(ptr) {}
	float operator[](int sampleNum);
};



//************** READONLY MOD BUFFER **************************/
// TODO: Figure out how to pass lambdas directly (no std::function overhead)
class ReadonlyModBuffer {
public:
	int numSamples;

	virtual ~ReadonlyModBuffer(){}

	ReadonlyModBuffer(int numSamples) : numSamples(numSamples) {}
	float operator[](int samplenum) { return getSample(samplenum); }
	float get(int sampleNum) { return getSample(sampleNum); }

	virtual bool isSingleton() { return false; }
	bool sampleIsUnique(int sampleNum) { return (sampleNum == 0) || !isSingleton(); }
	ReadonlyModBuffer* nullIfIgnorable(float ignorableValue = 0) {
		return canBeIgnored(ignorableValue) ? NULL : this;
	}

	virtual bool canBeIgnored(float ignorableValue = 0) {
		return false;
	}

	virtual ModBufferPtr map(function<float(float)> converter) {
		assert(false);
		return ModBufferPtr(new ReadonlyModBuffer(numSamples));
	}
	
protected:
	virtual float getSample(int sampleNum) {
		assert(false);
		return 0;
	}
};



//************** IGNORABLE MOD BUFFER **************************/
struct IgnorablModBuffer : public ReadonlyModBuffer {
	IgnorablModBuffer(): ReadonlyModBuffer(0){}
	bool canBeIgnored(float ignorableValue) override { return true; }
	float getSample(int sampleNum) override {
		// Optimizations should be made to ignore this sample source
		assert(false);
	}
	ModBufferPtr map(function<float(float)> converter) override {
		assert(false);
	}
};



//************** SINGLE SAMPLE MOD BUFFER **************************/
// used for cases where modulation can be compiled once per buffer without noticing (e.g. knobs, really slow LFOs)
class SingleSampleModBuffer : public ReadonlyModBuffer {
public:
	SingleSampleModBuffer(int numSamples, float sample): ReadonlyModBuffer(numSamples), sample(sample) {}
	float getSample(int sampleNum) override { 
		return sample; 
	}
	bool isSingleton() override { return true; }
	bool canBeIgnored(float ignorableValue) override { 
		return ignorableValue == sample; 
	}
	ModBufferPtr map(function<float(float)> converter) override {
		return ModBufferPtr(new SingleSampleModBuffer(numSamples, converter(sample)));
//		return make_unique<SingleSampleModBuffer>(numSamples, converter(sample));
	}
protected:
	float sample;
};



//************** MULTI SAMPLE MOD BUFFER **************************/
class MultiSampleModBuffer : public ReadonlyModBuffer {
public:
	MultiSampleModBuffer(int numSamples, function<float(float)> initFn): ReadonlyModBuffer(numSamples) {
		for (int s = 0; s < numSamples; s++) {
			samples.push_back(initFn(s));
		}
	}
	float getSample(int sampleNum) override { 
		// no audio rate modulation should exist yet
		// assert(false);
		return samples[sampleNum]; 
	}
	ModBufferPtr map(function<float(float)> converter) override {
		return ModBufferPtr(new MultiSampleModBuffer(numSamples, [&](int sampleNum){
			return converter(samples[sampleNum]);
		}));
	}
protected:
	vector<float> samples;
};



//************** READONLY MULTI SAMPLE MOD BUFFER **************************/
class ReadonlyMultiSampleModBuffer : public ReadonlyModBuffer {
public:
	ReadonlyMultiSampleModBuffer(int numSamples, float* samples_raw): ReadonlyModBuffer(numSamples), samples(samples_raw, samples_raw+numSamples) {}
	float getSample(int sampleNum) override {
		// no audio rate modulation should exist yet
		// assert(false);
		return samples[sampleNum];
	}
	ModBufferPtr map(function<float(float)> converter) override {
		return ModBufferPtr(new MultiSampleModBuffer(numSamples, [&](int sampleNum){
			return converter(samples[sampleNum]);
		}));
	}
protected:
	vector<float> samples;
};
