#pragma once
#import "ModSampleSource/ModSampleSource.h"
#import "../../../JuceLibraryCode/JuceHeader.h"
using namespace std;




//************** MODIFIER **************************/
class ModSampleSource;
class ModifierManager;

class Modifier {
public:
	Modifier(ModifierManager& modManager, int modNum): modManager(modManager), modNum(modNum){}
	virtual ~Modifier(){}
	int getModNum() { return modNum; }
	virtual unique_ptr<ModSampleSource> createModSource() = 0;
	virtual Component* getFullView();
	virtual Component* getMiniView() = 0;
	virtual Component* getRoutingEditor();
	virtual Component* getModEditor() = 0;

protected:
	ModifierManager& modManager;
	int modNum;
};



//************** MODIFIER FULL VIEW **************************/
#import "../../Util/SectionedRectangle.h"

class ModifierFullView: public Component {
public:
	Component* editor;
	Component* routing;
	ModifierFullView(Modifier* mod) {
		editor = mod->getModEditor();
		routing = mod->getRoutingEditor();
		addAndMakeVisible(editor);
		addAndMakeVisible(routing);
	}

	void resized() override {
	  auto pad = 3;

	  SectionedRectangle bounds(getLocalBounds());
	  bounds.padInwards(pad);

	  auto editAndRoute = bounds.splitHorizontallyAtRatio(0.666, pad);
	  editor->setBounds(editAndRoute.left);
	  routing->setBounds(editAndRoute.right);
	}
};

//************** CLOSE CIRCULARITY **************************/

inline Component* Modifier::getFullView() {
	return new ModifierFullView(this);
}

//************** TEST MODIFIER **************************/
class TestModifier 
	: public Modifier
{
public:
	TestModifier(ModifierManager& modManager, int modNum = 5): Modifier(modManager, modNum) {}
	unique_ptr<ModSampleSource> createModSource();
	Component* getFullView() { return NULL; }
	Component* getMiniView() { return NULL; }
	Component* getModEditor() { return NULL; }
};




//************** DEPENDENTS **************************/
