#pragma once

#include "../../../../JuceLibraryCode/JuceHeader.h"
#import "../Modifier.h"
#include "../../../Util/ADSR/ADSR.h"
#include "../../../Util/Observable.h"

class ADSRMod 
	: public Modifier
{
public:
	ADSRMod(ModifierManager& modManager, int modNum);
	virtual ~ADSRMod(){}
	Observable<float> attack = {1};
	Observable<float> decay = {1};
	Observable<float> sustain = {0.9};
	Observable<float> release = {1.5};

	float getAmpUnita(float ratio);
	Component* getModEditor() override;
	Component* getMiniView() override;
	Component* createGraph();
	
	unique_ptr<ModSampleSource> createModSource() override;
};


class ADSRModSource : public ModSampleSource, public ADSR {
public:
	ADSRMod* parent; 
	ADSRModSource(ADSRMod* parent) : parent(parent) {
		auto updateParams = [=](float __){
			ADSR::Parameters params { 
				parent->attack.get(),
				parent->decay.get(),
				parent->sustain.get(),
				parent->release.get(),
			};
			setParameters(params);
		};
		parent->attack.observe(updateParams, false);
		parent->decay.observe(updateParams, false);
		parent->sustain.observe(updateParams, false);
		parent->release.observe(updateParams, false);
		updateParams(0);
	}

	void retrigger() override {
		ADSR::noteOn();
	}
	void release() override {
		ADSR::noteOff();
	}

 	bool isAudioRate() override { return true; }

	ModEndState getEndState() const override { 
		if (isActive()) {
			return ModEndState::ENDING_INCOMPLETE; 
		} else {
			return ModEndState::ENDING_COMPLETE; 
		}
	}

	void renderNextSamples(int numSamples, float* output) override {
		for (int s = 0; s < numSamples; s++) {
			output[s] = ADSR::getNextSample();
		}
	}
};



class ADSRModFullView : public Component, private Slider::Listener {
public:
	ADSRModFullView(ADSRMod* mod);
	ADSRMod* mod;

	Slider A;
	Slider D;
	Slider S;
	Slider R;
	Component* waveGraph;
	// Component* routingEditor;

	void paint (Graphics& g) override;
	void resized() override;

private:
	void sliderValueChanged (Slider* slider) override;
};
