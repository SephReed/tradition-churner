#include "ADSRMod.h"
#include "../../../Components/ComponentStyler.h"
#include "../../../Components/Grapher.h"
#include "../ModifierManager.h"

using namespace ComponentStyling;
using namespace std;


// /************** MOD **************************/

ADSRMod::ADSRMod(ModifierManager& modManager, int modNum): Modifier(modManager, modNum) {
	
}

Component* ADSRMod::getModEditor() {
	return new ADSRModFullView(this);
}

Component* ADSRMod::getMiniView() {
	assert(false);
	return NULL;
}

unique_ptr<ModSampleSource> ADSRMod::createModSource() {
	return make_unique<ADSRModSource>(this);
}

Component* ADSRMod::createGraph() {
	return new Grapher([this](float ratio){
		return this->getAmpUnita(ratio);
	}, 0 ,1);
}

float ADSRMod::getAmpUnita(float ratio) {
	auto a = attack.get();
	auto d = decay.get();
	auto hold = 1;
	auto r = release.get();

	auto s = sustain.get();
	// TODO: calc once?
	float range = a + d + hold + r;
	auto aEnd = a/range;
	auto dEnd = (a + d)/range;
	auto holdEnd = (a + d + hold)/range;
	auto rEnd = 1;

	if (ratio < aEnd) {
		return ratio/aEnd;

	} else if (ratio < dEnd) {
		auto span = (dEnd - aEnd);
		auto dRatio = (ratio - aEnd) / span;
		return (1-(dRatio * (1-s)));

	} else if (ratio < holdEnd) {
		return s;

	} else {
		auto span = (rEnd - holdEnd);
		auto rRatio = (ratio - holdEnd) / span;
		return s-(rRatio * s);
	}
}


// /************** FULL VIA **************************/


ADSRModFullView::ADSRModFullView(ADSRMod* mod) : mod(mod) {
	waveGraph = mod->createGraph();
	addAndMakeVisible(waveGraph);

//	setupCommonTrackSlider(A);
	A.addListener(this);
	A.setRange(0.0, 10.0, 0.01);
	addAndMakeVisible(A);

//	setupCommonTrackSlider(D);
	D.addListener(this);
	D.setRange(0.0, 10.0, 0.01);
	addAndMakeVisible(D);

//	setupCommonTrackSlider(S);
	S.addListener(this);
	S.setRange(0, 1, 0.01);
	S.setTextValueSuffix("%");
	addAndMakeVisible(S);

//	setupCommonTrackSlider(R);
	R.addListener(this);
	R.setRange(0.0, 15.0, 0.01);
	addAndMakeVisible(R);

	// routingEditor = mod->getRoutingEditor();
	// addAndMakeVisible(routingEditor);
}

void ADSRModFullView::paint (Graphics& g) {
	// g.fillAll(Colour(250, 40, 40));
};

void ADSRModFullView::resized() {
	auto halfWidth = getWidth()/2.0;
	auto halfHeight = getHeight()/2.0;
	auto pad = 3;

	//	Q (0, 0)
	waveGraph->setBounds(pad, pad, halfWidth -pad, halfHeight -pad);

	auto sliderHeight = 20.0;
	A.setBounds(pad, halfHeight, halfWidth, sliderHeight);
	D.setBounds(pad, halfHeight + sliderHeight, halfWidth, sliderHeight);
	S.setBounds(pad, halfHeight + 2*sliderHeight, halfWidth, sliderHeight);
	R.setBounds(pad, halfHeight + 3*sliderHeight, halfWidth, sliderHeight);

	// routingEditor->setBounds(halfWidth, 0, halfWidth, getHeight());
};

void ADSRModFullView::sliderValueChanged (Slider* slider) {
	auto value = slider->getValue();
	if (slider == &A) {
		mod->attack.set(value);
	} else if (slider == &D) {
		mod->decay.set(value);
	} else if (slider == &S) {
		mod->sustain.set(value);
	} else if (slider == &R) {
		mod->release.set(value);
	}
	waveGraph->repaint();
}
