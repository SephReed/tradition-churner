#pragma once

#import "LFO.h"

//************** LFO SEQUENCE **************************/

class LFOSequence
	: public LFO
{
public:
	vector<bool> steps;
	LFOSequence(ModifierManager& modManager, int modNum): LFO(modManager, modNum) {
		steps.resize(16);
	}
	virtual ~LFOSequence(){}

	Component* getModEditor() override;
	Component* getMiniView() override { return NULL; }
	int stepsTillSpanEnd(int startStep) {
		if (startStep >= steps.size()) { return 0; }

		int count = 1;
		while (startStep+count < steps.size() && steps[startStep + count] == false) {
			count++;
		}
		return count;
	}
	// Component* createGraph();
	
	unique_ptr<ModSampleSource> createModSource() override;
};


//************** LFO SEQUENCE SAMPLE SOURCE **************************/

class LFOSequenceSampleSource : public ModSampleSource {
public:
	LFOSequence& mod;
	int nextStepStart = -1;
	float angle = 0;
	float angleDelta = 0.01;
	// bool hasBeenCalled = false;

	LFOSequenceSampleSource(LFOSequence& mod)
		: mod(mod) 
	{
		
	}

	void startNextWave() {
		auto currentStep = nextStepStart;
		auto stepLength = mod.stepsTillSpanEnd(currentStep);
		if (stepLength == 0) {
			stepLength = mod.stepsTillSpanEnd(0);
			nextStepStart = 0;
		}
		angleDelta = 0.1 / stepLength;
		nextStepStart += stepLength;
		angle = fmod(angle, 1);
	}

	void retrigger() override {
		if (nextStepStart == -1 || true) {
			nextStepStart = 0;
			startNextWave();
		}
		// if (sync == false)
		// ADSR::noteOn();
	}
	void release() override {
//		ADSR::noteOff();
	}

 	bool isAudioRate() override { 
 		return false;
 		// return tune.get() > 100; 
 	}

	void renderNextSamples(int numSamples, float* output) override {
		assert(lastRenderedSample == -1);
		for (int s = 0; s < numSamples; s++) {
			output[s] = mod.getAmpUnita(angle);

			angle += angleDelta;
			if (angle >= 1) {
				startNextWave();
			}
		}
	}
};


//************** LFO SEQUENCE FULL VIEW **************************/
#import "../../../Components/SimpleSequencer.h"

class LFOSequenceFullView : public LFOFullView {
public:
	SimpleSequencer sequence;
	LFOSequenceFullView(LFOSequence& mod) : LFOFullView(mod) {
		addAndMakeVisible(sequence);
		sequence.stepChanges.observe([&](auto change){
			mod.steps[change.stepNum] = change.value;
		}, false);
		sequence.setNumSteps(mod.steps.size());
	}


	// void paint (Graphics& g) override {
	// 	g.fillAll(Colour(250, 40, 40));
	// }
	void resized() override {
	  auto pad = 3;

	  SectionedRectangle bounds(getLocalBounds());
	  bounds.padInwards(pad);

	  auto mainRows = bounds.splitVerticallyAtRatio(0.25, pad);

	  auto graphAndInputs = mainRows.top.splitHorizontallyAtRatio(0.5, pad);
		waveGraph.setBounds(graphAndInputs.left);
		curveEditor.setBounds(graphAndInputs.right);


		sequence.setBounds(mainRows.bottom);
	}

};



//************** CIRCULAR DEPENDENCY RESOLUTIONS **************************/

inline unique_ptr<ModSampleSource> LFOSequence::createModSource() {
	return make_unique<LFOSequenceSampleSource>(*this);
}

inline Component* LFOSequence::getModEditor() { return new LFOSequenceFullView(*this); }
