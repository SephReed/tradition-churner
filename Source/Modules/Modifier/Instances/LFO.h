#pragma once

#import "../Modifier.h"
#import "../../RangedUserInput/RangedUserInput.h"
#import "../../WaveSource/StableWave/SmoothWave/SmoothWaveModTargets.h"
#import "../../WaveSource/StableWave/SmoothWave/SmoothWaveTables.h"


class LFO  : public Modifier {
public:
	static inline SmoothWaveTables waveTable;

	LFO(ModifierManager& modManager, int modNum): Modifier(modManager, modNum) {}
	virtual ~LFO(){}
	RangedUserInput pulseWidth {SmoothWaveModTargets::pulseWidth};
	RangedUserInput expWidth {SmoothWaveModTargets::expWidth};
	RangedUserInput curve1 {SmoothWaveModTargets::curve1};
	RangedUserInput curve2 {SmoothWaveModTargets::curve2};
	Observable<double> tune = {1.5};
	Observable<bool> sync {false};

	float getAmpUnita(float pos) {
		return LFO::waveTable.getAmpForParamsUnita(
			pos,
			curve1.get(), curve2.get(),
			pulseWidth.get(), expWidth.get()
		);
	}
	// Component* getFullView() override;
	Component* getModEditor() override;
	Component* getMiniView() override { return NULL; }
	// Component* createGraph();
	
	unique_ptr<ModSampleSource> createModSource() override;
};


class LFOSampleSource : public ModSampleSource {
public:
	LFO& mod;
	float angle = 0;
	float angleDelta = 0.01;

	LFOSampleSource(LFO& mod)
		: mod(mod)
	{
		// auto updateParams = [=](float __){
		// 	ADSR::Parameters params { 
		// 		parent->attack.get(),
		// 		parent->decay.get(),
		// 		parent->sustain.get(),
		// 		parent->release.get(),
		// 	};
		// 	setParameters(params);
		// };
		// parent->attack.observe(updateParams, false);
		// parent->decay.observe(updateParams, false);
		// parent->sustain.observe(updateParams, false);
		// parent->release.observe(updateParams, false);
		// updateParams(0);
	}

	void retrigger() override {
		if (mod.sync.get()) {
			angle = 0;
		}
	}
	void release() override {
//		ADSR::noteOff();
	}

 	bool isAudioRate() override { 
 		return false;
 		// return tune.get() > 100; 
 	}

	void renderNextSamples(int numSamples, float* output) override {
		for (int s = 0; s < numSamples; s++) {
			// output[s] = sin(s/1000.0);
			output[s] = mod.getAmpUnita(angle);

			angle += angleDelta;
			angle = fmod(angle, 1);
		}
	}
};


//************** LFO FULL VIEW **************************/
#import "../../WaveSource/StableWave/SmoothWave/CurveEditor.h"
#import "../../../Util/SectionedRectangle.h"
#import "../../../Components/Grapher.h"
#import "../../../Components/BacklitButton.h"

class LFOFullView : public Component {
public:
	LFO& mod;
	SmoothWaveCurveEditor curveEditor;
	BacklitButton sync;

	LFOFullView(LFO& mod)
	: mod(mod),
		curveEditor(
			mod.pulseWidth.createRotary(),
			mod.expWidth.createRotary(),
			mod.curve1.createRotary(),
			mod.curve2.createRotary()
		),
		waveGraph([&](float pos) {
		 	return mod.getAmpUnita(pos);
//			return LFO::waveTable.getAmpForParamsUnita(
//				pos,
//				mod.curve1.get(), mod.curve2.get(),
//				mod.pulseWidth.get(), mod.expWidth.get()
//			);
		})
	{
		addAndMakeVisible(curveEditor);
		addAndMakeVisible(waveGraph);
		addAndMakeVisible(sync);
		sync.attachToObservable(mod.sync);
		auto doRepaint = [&](float){ repaint(); };
		mod.pulseWidth.observe(doRepaint);
		mod.expWidth.observe(doRepaint);
		mod.curve1.observe(doRepaint);
		mod.curve2.observe(doRepaint);
	}

	Grapher waveGraph;
	Component* routingEditor;

	void paint (Graphics& g) override {
		// g.fillAll(Colour(250, 40, 40));
	}
	void resized() override {
	  auto pad = 3;

	  SectionedRectangle bounds(getLocalBounds());
	  bounds.padInwards(pad);

	  auto graphAndInputs = bounds.splitVerticallyAtRatio(0.5, pad);
		waveGraph.setBounds(graphAndInputs.top);
		curveEditor.setBounds(graphAndInputs.bottom);

		sync.setBounds(0, 150, 40, 40);
	}

};



//************** CIRCULAR DEPENDENCY RESOLUTIONS **************************/

inline unique_ptr<ModSampleSource> LFO::createModSource() {
	return make_unique<LFOSampleSource>(*this);
}

inline Component* LFO::getModEditor() { return new LFOFullView(*this); }
