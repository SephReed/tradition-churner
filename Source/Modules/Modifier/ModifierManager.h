#pragma once

#import "Modifier.h"
#import "Instances/LFO.h"
#import "Instances/LFOSequence.h"
#import "Instances/ADSRMod.h"
#import "../../Components/TabPaneViewer.h"

using namespace std;

class PluginProcessor;

class ModifierManager {
public:
	static const int NUM_MODS = 6;
	PluginProcessor* plugin;
	vector<Modifier*> mods {NUM_MODS};

	ModifierManager(PluginProcessor* plugin) : plugin(plugin) {
		setModifier(0, new ADSRMod(*this, 0));
		setModifier(1, new LFO(*this, 1));
		setModifier(2, new LFOSequence(*this, 2));
	}

	Component* getEditor() {
		if (editor == NULL) {
			editor = new TabPaneViewer();
			// editor->addTabs({"AllMods", "Mod1", "Mod2", "Mod3", "Mod4", "Mod5", "Mod6"});
			editor->addPane("All Mods", &emptyPane);
			for (int m = 0; m < mods.size(); m++) {
				editor->addPane("Mod " + to_string(m+1), mods[m] ? mods[m]->getFullView() : NULL);
			}
		  editor->selectIndex(3);
		}
		return editor;
	}

	void setModifier(int modNum, Modifier* mod);

protected:
	TabPaneViewer* editor = NULL;
	Component emptyPane;
};


