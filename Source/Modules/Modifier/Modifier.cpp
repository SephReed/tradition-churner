#import "Modifier.h"
#import "ModSampleSource/ModSampleSource.h"
#import "ModifierManager.h"
#import "../../PluginProcessor.h"
using namespace std;

// /************** MODIFIER MANAGER **************************/

void ModifierManager::setModifier(int modNum, Modifier* mod) {
	if (mods[modNum]) { delete mods[modNum]; }

	mods[modNum] = mod;

	if (editor && mod) {
		editor->setPane(modNum + 1, mod->getFullView());
	}

	plugin->routingManager.routingSets[modNum].setModifier(mod);
}



// /************** MODIFIER **************************/

Component* Modifier::getRoutingEditor() {
	return modManager.plugin->routingManager.routingSets[modNum].createEditor();
}



// /************** MOD SAMPLE SOURCE **************************/
	
void ModSampleSource::resetCaching() {
	for (auto p = 0; p < mirrorSamplePos.size(); p++) {
		mirrorSamplePos[p] = 0;
	}
	lastRenderedSample = -1;
}

float ModSampleSource::getNextSampleFor(int mirrorId) {
	auto output = getNextSamplesFor(mirrorId, 1, NULL);
	return output->get(0);
}

ModBuffer ModSampleSource::getNextSamplesFor(int mirrorId, int numSamples, float* output) {
	if (canBeIgnored()) {
		return make_unique<IgnorablModBuffer>();
	}
	if (isAudioRate() == false && output == NULL) { 
		numSamples = 1;
	}

	bool noDependents = isUnmirrored();
	if (output && noDependents) {
		renderNextSamples(numSamples, output);
		return make_unique<ReadonlyMultiSampleModBuffer>(numSamples, output);
	}

	int startingSample = 0;
	if (noDependents == false) {
		startingSample = mirrorSamplePos[mirrorId];
		// It should never be pulling twice from the same source
		assert(startingSample == 0);
		mirrorSamplePos[mirrorId] += numSamples;
	} else {
		assert(lastRenderedSample == -1);
	}

	int samplesEnd = startingSample + numSamples;
	sharedSamplesMutex.lock();
	if (samplesEnd > lastRenderedSample + 1) {
		auto size = sharedSampleSource.size();
		if (samplesEnd > size) {
			sharedSampleSource.resize(samplesEnd);
			assert(sharedSampleSource.size() >= samplesEnd);
		}
		auto startingSample = &sharedSampleSource[lastRenderedSample+1];
		auto numSamplesNeeded = (samplesEnd - lastRenderedSample) - 1;
		renderNextSamples(numSamplesNeeded, startingSample);
		if (noDependents == false) {
			lastRenderedSample += numSamplesNeeded;
			assert(lastRenderedSample == (samplesEnd-1));
		}
	}
	sharedSamplesMutex.unlock();

	if (output != NULL) {
		for (int s = 0; s < numSamples; s++) {
			output[s] = sharedSampleSource[startingSample+s];
		}
	}

	if (numSamples == 1) {
		return make_unique<SingleSampleModBuffer>(numSamples, sharedSampleSource[startingSample]);
	} else if (output) {
		return make_unique<ReadonlyMultiSampleModBuffer>(numSamples, output);
	} else {
		return make_unique<ReadonlyMultiSampleModBuffer>(numSamples, &sharedSampleSource[startingSample]);
	}	
}

void ModSampleSource::renderNextSamples(int numSamples, float* output) {
	assert(false);
}

unique_ptr<ModSampleSourceMirror> ModSampleSource::createMirror() {
	assert(allowMirror);
	if (numDependents == 0) {
		mirrorSamplePos.push_back(0);
	}
	numDependents++;
	mirrorSamplePos.push_back(0);
	sharedSamplesMutex.lock();
	sharedSampleSource.reserve(1024);
	sharedSamplesMutex.unlock();
	assert(numDependents < 5);
	return make_unique<ModSampleSourceMirror>(this, numDependents);
}




// /************** TEST MODIFIER **************************/
#include "ModSampleSource/TestModSampleSource.h"

unique_ptr<ModSampleSource> TestModifier::createModSource() {
	return make_unique<TestModSampleSource>();
}



// /************** MOD CHAIN **************************/
#include "ModSampleSource/ModChain.h"

ModChain::ModChain() : ModSampleSource() {}

void ModChain::addSampleSource(unique_ptr<ModSampleSource> addMe) {
	assert(addMe.get());
	sampleSources.push_back(move(addMe));
}


void ModChain::acknowledgeRoute(ModulationRouting* route) {
	auto modSource = route->createModSource();
	routeToSourceMap[route] = modSource.get();
	addSampleSource(move(modSource));
}

void ModChain::forgetRoute(ModulationRouting* route) {
	auto modSource = routeToSourceMap[route];
	assert(modSource);
	for (int s = 0; s < sampleSources.size(); s++) {
		auto& checkMe = sampleSources[s];
		if (checkMe.get() == modSource) {
			sampleSources.erase(sampleSources.begin() + s);
			return;
		}
	}
	// should always find and remove a source
	assert(false);
}

void ModChain::retrigger() {
	for (auto& source : sampleSources) {
		if(source->allowRetriggerPermeate()) {
			source->retrigger();
		}
	}
}

void ModChain::release() {
	for (auto& source : sampleSources) {
		if(source->allowRetriggerPermeate()) {
			source->release();
		}
	}
}

// override these to defer to subchain if possible
float ModChain::getNextSample() { 
	auto bestSource = findBestSampleSource();
	return !bestSource ? 0 : bestSource->getNextSampleFor(0);
}

void ModChain::getNextSamples(int numSamples, float* output) {
	auto bestSource = findBestSampleSource();
	if (bestSource == this || bestSource == NULL) {
		getNextSamplesFor(0, numSamples, output);
	} else {
		bestSource->getNextSamples(numSamples, output);
	}
}
ModBuffer ModChain::getNextSamplesReadonly(int numSamples) {
	auto bestSource = findBestSampleSource();
	if (bestSource == this || bestSource == NULL) {
		return getNextSamplesFor(0, numSamples, NULL);
	} else {
		return bestSource->getNextSamplesReadonly(numSamples);
	}
}

ModSampleSource* ModChain::findBestSampleSource() {
	auto sourceCount = sampleSources.size();
	if (sourceCount == 0) {
		return NULL;
	} else if (sourceCount == 1) {
		return sampleSources[0].get();
	}
	return this;
}

void ModChain::renderNextSamples(int numSamples, float* output) {
	assert(output);
	// Find the first sample source without dependents and write it's samples directly to output
	// Skips creating an extra samples buffer and copying it
	int preOptimized = -1;
	for (int s = 0; s < sampleSources.size(); s++) {
		auto modSource = sampleSources[s].get();
		if (modSource->canBeIgnored() == false && modSource->isUnmirrored() && modSource->isAudioRate()) {
			preOptimized = s;
			modSource->getNextSamples(numSamples, output);
			break;
		}
	}

	// first touch is a write-over unless preOptimized
	bool untouched = preOptimized == -1;
	for (int s = 0; s < sampleSources.size(); s++) {
		auto modSource = sampleSources[s].get();
		if (s == preOptimized || modSource->canBeIgnored()) {
			// do nothing
		} else {
			if (modSource->isAudioRate()) {
				auto modSamples = modSource->getNextSamplesReadonly(numSamples);
				for (int s = 0; s < numSamples; s++) {
					if (untouched) {
						output[s] = modSamples->get(s);
					} else {
						output[s] += modSamples->get(s);
					}
				}
				untouched = false;
			} else {
				float value = modSource->getNextSample();
				if (value != 0) {
					for (int s = 0; s < numSamples; s++) {
						if (untouched) {
							output[s] = value;
						} else {
							output[s] += value;
						}
					}
					untouched = false;
				} else {
					untouched = true;
				}
			}
		}
	}
}


// if has any audioRate sources
bool ModChain::isAudioRate() {
	for (auto& modSource : sampleSources) {
		if (modSource->isAudioRate()) {
			return true;
		}
	}
	return false;
}

bool ModChain::canBeIgnored() {
	for (auto& modSource : sampleSources) {
		if (modSource->canBeIgnored() == false) {
			return false;
		}
	}
	return true;
}

bool ModChain::allowRetriggerPermeate() {
	return false;
}

ModEndState ModChain::getEndState() const {
	bool canEnd = false;
	for (auto& modSource : sampleSources) {
		auto endState = modSource->getEndState();
		if (endState == ModEndState::ENDING_INCOMPLETE) {
			return ModEndState::ENDING_INCOMPLETE;
		} else if (endState == ModEndState::ENDING_COMPLETE) {
			canEnd = true;
		}
	}
	return canEnd ? ModEndState::ENDING_COMPLETE : ModEndState::NO_ENDING;
}



// /************** MOD BUFFER PTR **************************/

float ModBufferPtr::operator[](int sampleNum) { return (*get())[sampleNum]; }
