#pragma once

// /************** RANGED MOD SAMPLE SOURCE **************************/
#import "ModSampleSource.h"

class RangedModSampleSource : public ModSampleSource {
public:
	unique_ptr<ModSampleSource> sampleSource;
	RangedModSampleSource(unique_ptr<ModSampleSource> sampleSource)
	:	sampleSource(move(sampleSource))
	{}

	void renderNextSamples(int numSamples, float* output) final {
		sampleSource->getNextSamples(numSamples, output);
		scaleSamples(numSamples, output);
	}

	virtual void scaleSamples(int numSamples, float* modMe) = 0;

	bool isAudioRate() override { return sampleSource->isAudioRate(); } 
	bool canBeIgnored() override { return sampleSource->canBeIgnored(); } 
	bool allowRetriggerPermeate() override { return sampleSource->allowRetriggerPermeate(); } 
	ModEndState getEndState() const override { return sampleSource->getEndState(); }
	void retrigger() override { sampleSource->retrigger(); } 
	void release() override {	sampleSource->release(); } 
};
