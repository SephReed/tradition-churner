#pragma once


// /************** TEST MOD SAMPLE SOURCE **************************/
#import "ModSampleSource.h"

class TestModSampleSource : public ModSampleSource {
public:
	TestModSampleSource(){}
	void renderNextSamples(int numSamples, float* output) override {
		if (endingIncomplete()) {
			const auto resetAngle = 4 * (2 * M_PI);
			if (held) {
				for (int s = 0; s < numSamples; s++) {
					if (testAngleDelta > resetAngle) {
						testAngleDelta = fmod(testAngleDelta, resetAngle);
						doubleTime = !doubleTime;
					}
					output[s] = (sin(testAngleDelta)/2) + 1;
					testAngleDelta += doubleTime ? .001 : .0005;
				}
			} else {
				for (int s = 0; s < numSamples; s++) {
					if (endingIncomplete()) {
						output[s] = (sin(testAngleDelta)/2) + 1;
						testAngleDelta += .00025;	
						if (testAngleDelta > resetAngle) {
							endState = ModEndState::ENDING_COMPLETE;
						}
					} else {
						output[s] = 0;
					}
				}
			}
		}
		
	}
	bool isAudioRate() override {
		return true;
	}

	void retrigger() override {
		testAngleDelta = 0;
	 	doubleTime = false;
	 	held = true;
	 	endState = ModEndState::ENDING_INCOMPLETE;
	}

	void release() override {
		testAngleDelta = fmod(testAngleDelta, 2 * M_PI);
	 	held = false;
	}

	ModEndState getEndState() const override {
		return endState;
	}

private:
	float testAngleDelta = 0;
	bool doubleTime = false;
	bool held = false;
	ModEndState endState = ModEndState::ENDING_COMPLETE;
};
