#pragma once
#import <assert.h>
#include <math.h>
#include <vector>
using namespace std;

#import "../../../Util/Observable.h"
#import "../ReadonlyModBuffer.h"
#import  "../ModRangeAndDirection.h"

enum class ModEndState {
	NO_ENDING,
	ENDING_INCOMPLETE,
	ENDING_COMPLETE
};



//#import "../../Routing/ModulationRouting.h"

using SampleChunk = vector<float>;
using ModBuffer = unique_ptr<ReadonlyModBuffer>;


// /************** MOD SAMPLE SOURCE **************************/
class ModSampleSourceMirror;

class ModSampleSource {
public:
	// const RangeAndDirection* rangeDir;
	
	// ModSampleSource(const RangeAndDirection* rangeDir): rangeDir(rangeDir) {}
	
	virtual ~ModSampleSource(){}

	void resetCaching();
	bool isUnmirrored() { return numDependents == 0; }

	// do nothing
	virtual void retrigger() {}
	virtual void release() {}

	virtual ModEndState getEndState() const { return ModEndState::NO_ENDING; }
	bool endingIncomplete() const { return (getEndState() == ModEndState::ENDING_INCOMPLETE); }
	virtual bool isAudioRate() { return false; }
	virtual bool canBeIgnored() { return getEndState() == ModEndState::ENDING_COMPLETE; }

	// TODO: make absolutely sure this logic holds
	virtual bool allowRetriggerPermeate() { return true; }

	// TODO: accept that this is about to be a hassle
	virtual float getNextSample() { return getNextSampleFor(0); }
	virtual void getNextSamples(int numSamples, float* output) { getNextSamplesFor(0, numSamples, output); }
	virtual ModBuffer getNextSamplesReadonly(int numSamples) { return getNextSamplesFor(0, numSamples, NULL); }
	
	float getNextSampleFor(int mirrorId);
	virtual ModBuffer getNextSamplesFor(int mirrorId, int numSamples, float* output);
	virtual void renderNextSamples(int numSamples, float* output);
	unique_ptr<ModSampleSourceMirror> createMirror();

protected:
	bool allowMirror = true;
	int numDependents = 0;
	vector<int> mirrorSamplePos;
	int lastRenderedSample = -1;
	mutex sharedSamplesMutex;
	vector<float> sharedSampleSource;
};




// /************** MOD SAMPLE SOURCE MIRROR **************************/

class ModSampleSourceMirror : public ModSampleSource {
public:
	ModSampleSourceMirror(ModSampleSource* masterSource, int mirrorId)
	: mirrorId(mirrorId),
		masterSource(masterSource)
	{}

	bool isAudioRate() override { return masterSource->isAudioRate(); } 
	bool canBeIgnored() override { return masterSource->canBeIgnored(); } 
	bool allowRetriggerPermeate() override { return masterSource->allowRetriggerPermeate(); } 
	ModEndState getEndState() const override { return masterSource->getEndState(); }

	// should these ever happen?
	void retrigger() override {
		assert(false);
		return masterSource->retrigger();
	} 

	void release() override {
		assert(false);
		return masterSource->release();
	} 

	float getNextSample() override { return masterSource->getNextSampleFor(mirrorId); }
	void getNextSamples(int numSamples, float* output) override { getNextSamplesFor(mirrorId, numSamples, output); }
	ModBuffer getNextSamplesReadonly(int numSamples) override { return getNextSamplesFor(mirrorId, numSamples, NULL); }

	ModBuffer getNextSamplesFor(int mirrorId, int numSamples, float* output) override {
		return masterSource->getNextSamplesFor(mirrorId, numSamples, output);
	}

protected:
	int mirrorId = 0;
	ModSampleSource* masterSource = NULL;
};


