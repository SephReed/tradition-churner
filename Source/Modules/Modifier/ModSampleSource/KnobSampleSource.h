#pragma once

// /************** KNOB SAMPLE SOURCE **************************/
#import "ModSampleSource.h"

class KnobSampleSource : public ModSampleSource {
public:
	Observable<double>* knob;
	KnobSampleSource(Observable<double>* knob) 
	:	knob(knob)
	{}

	void renderNextSamples(int numSamples, float* output) override {
		auto value = knob->get();
		for (int s = 0; s < numSamples; s++) {
			output[s] = value;
		}
	}

	bool canBeIgnored() override {
		return false;
	}
};

