#pragma once

// /************** MOD CHAIN **************************/
#import <map>
#import "ModSampleSource.h"

class ModulationRouting;

class ModChain : public ModSampleSource {
public:
	std::string debugName;
	ModChain();
	void retrigger() override;
	void release() override;
	void addSampleSource(unique_ptr<ModSampleSource> addMe);
	void acknowledgeRoute(ModulationRouting* route);
	void forgetRoute(ModulationRouting* route);
	float getNextSample() override;
	void getNextSamples(int numSamples, float* output) override;
	ModBuffer getNextSamplesReadonly(int numSamples) override;
	void renderNextSamples(int numSamples, float* output) override;
	bool isAudioRate() override;
	bool canBeIgnored() override;
	bool allowRetriggerPermeate() override;
 	ModEndState getEndState() const override;


private:
	vector<unique_ptr<ModSampleSource>> sampleSources;
	map<ModulationRouting*, ModSampleSource*> routeToSourceMap;
	ModSampleSource* findBestSampleSource();
};
