#pragma once
using namespace std;

// TODO: Move this to Components


//************** ROTARY PAINT LAYER **************************/
#import "../../Components/ComponentStyler.h"
using namespace ComponentStyling;
using namespace TraditionChurnerStyling;

class RangeEditor;

class RotaryPaintLayer : public Component {
public:
	static inline double getMaxRadius(Rectangle<float> bounds) {
		return jmin (bounds.getWidth(), bounds.getHeight()) / 2.0f;
	}
	static inline double getRotaryTrackRadius(Rectangle<float> bounds) {
		return getRotaryTrackRadius(getMaxRadius(bounds));
	}

	static double getRotaryTrackRadius(double maxRadius); //bottom
	static inline Slider::RotaryParameters throttleRange = {3.1415*8/6, 3.1415*(12 +4)/6, true};
	
	RangeEditor& slider;
	RotaryPaintLayer(RangeEditor& slider): slider(slider) {}
	void paint (Graphics& g) override; //bottom
};


//************** STRAIGHT SLIDER PAINT LAYER **************************/
class StraightSliderPaintLayer : public Component {
public:
	RangeEditor& slider;
	StraightSliderPaintLayer(RangeEditor& slider): slider(slider) {}
	void paint (Graphics& g) override; //bottom
};





//************** RANGE EDITOR L&F HACK **************************/
class RangeEditorLFHack : public LookAndFeel_V4 {
public:
	juce::Slider::SliderLayout getSliderLayout (Slider& i_slider) override;
};

//************** RANGE EDITOR **************************/
#include "../../Components/ComponentStyler.h"
#include <math.h>
using namespace ComponentStyling;

class RangeEditor : public Slider {
public:
	struct Styling {
		static inline PixelARGB& trackColor = TC_Color::fgFade4;
		static inline PixelARGB& trackFillColor = TC_Color::accentStart;
		static inline PixelARGB thumbColor = {255, 255, 255, 255};
		static inline double trackWidth = 1;
		static inline double trackFillWidth = 2;
		static inline double ometerOverhang = 3;
	};
	
	static inline RangeEditorLFHack& lookAndFeel() {
		static auto out = new RangeEditorLFHack();
		return *out;
	}

	RangedUserInput* userInput;
	UserInputModType type;
	bool ignoreNextUpdate = false;
	float textWidth = 50;

	RangeEditor(RangedUserInput* userInput, UserInputModType type)
	: userInput(userInput),
		type(type)
	{
		userInput->observe([&](float newVal){
			const MessageManagerLock mmLock;
			setValue(newVal);
		});
		
		auto& params = userInput->params;
		auto min = params.min;
		auto max = params.max;
		setRange(min, max, params.grainularity);
		setDoubleClickReturnValue(true, params.defaultValue);

		addAndMakeVisible(label);
		label.setInterceptsMouseClicks(false, false);
		label.setFont(Font(TEXT_HEIGHT));
		label.setJustificationType(Justification(Justification::Flags::centred));
		label.setText(string(params.shortname), NotificationType::dontSendNotification);

		if (type == UserInputModType::ROTARY) {
			sliderLayer = make_unique<RotaryPaintLayer>(*this);
			setRotaryParameters(RotaryPaintLayer::throttleRange);
			setSliderStyle(Slider::RotaryVerticalDrag);
		} else if (type == UserInputModType::HORIZONTAL_SLIDER) {
			sliderLayer = make_unique<StraightSliderPaintLayer>(*this);
		}

		assert(sliderLayer.get());
		addAndMakeVisible(sliderLayer.get());
		sliderLayer->setInterceptsMouseClicks(false, false);
		setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
		setLookAndFeel(&RangeEditor::lookAndFeel());
	}

	void valueChanged() override {
		cout << "SLIDER " << getValue() << endl;
		if (ignoreNextUpdate == false) {
			userInput->set(getValue());
		} else {
			ignoreNextUpdate = false;
		}
		repaint();
	}

	void resized () override {
		Slider::resized();
		assert(sliderLayer.get());
		auto bounds = getLocalBounds();
		if (type == UserInputModType::ROTARY) {
			sliderLayer->setBounds(bounds);
			auto trackRadius = RotaryPaintLayer::getRotaryTrackRadius(bounds.toFloat());
			auto arcBottom = trackRadius * 1.9;
			label.setBounds(0, max(arcBottom - (TEXT_HEIGHT/2), (trackRadius + 5)), getWidth(), TEXT_HEIGHT);
			
		} else if (type == UserInputModType::HORIZONTAL_SLIDER) {
			label.setBounds(0, 0, textWidth, bounds.getHeight());
			sliderLayer->setBounds(textWidth, 0, bounds.getWidth() - textWidth, bounds.getHeight());
		}
	}

	void paint (Graphics& g) override {
		Component::paint(g);
	};
	
protected:
	Label label;
	// RotaryPaintLayer sliderLayer {*this};
	unique_ptr<Component> sliderLayer;
};


inline juce::Slider::SliderLayout RangeEditorLFHack::getSliderLayout (Slider& i_slider) {
	auto slider =  dynamic_cast<RangeEditor*> (&i_slider);
	if (slider->type == UserInputModType::HORIZONTAL_SLIDER) {
		int splitPos = slider->textWidth;
		int width = slider->getWidth();
		int height = slider->getHeight();
		return {
			{splitPos, 0, width-splitPos, height},
			{0,0, splitPos, height}
		};
	}
	return LookAndFeel_V4::getSliderLayout(i_slider);
}








// /************** SLIDER PAINT LAYER **************************/


inline double RotaryPaintLayer::getRotaryTrackRadius(double maxRadius) {
	using Style = RangeEditor::Styling;
	return maxRadius - (Style::trackFillWidth * 0.5f) - Style::ometerOverhang;
}

inline void RotaryPaintLayer::paint(Graphics& g) {
	auto rotaryParams = slider.getRotaryParameters();
	auto trackStartAngle = rotaryParams.startAngleRadians;
	auto trackEndAngle = rotaryParams.endAngleRadians;
	
	auto bounds = getBounds().toFloat();		
	auto radius = RotaryPaintLayer::getMaxRadius(bounds);
	auto arcRadius = RotaryPaintLayer::getRotaryTrackRadius(radius);

	Path backgroundArc;
	backgroundArc.addCentredArc (
		bounds.getCentreX(),bounds.getCentreY(),
			arcRadius, arcRadius,
			0.0f,
			trackStartAngle, trackEndAngle,
			true
		);
	
	using Styling = RangeEditor::Styling;
	g.setColour (Styling::trackColor);
	g.strokePath (backgroundArc, PathStrokeType (Styling::trackWidth, PathStrokeType::curved, PathStrokeType::rounded));

	auto sliderPos = (float) slider.valueToProportionOfLength (slider.getValue());
	jassert (sliderPos >= 0 && sliderPos <= 1.0f);
	auto baseline = slider.userInput->params.baseline;
	auto baselinePos = (float) slider.valueToProportionOfLength(baseline);
	auto baselineAngle = trackStartAngle + baselinePos * (trackEndAngle - trackStartAngle);
	auto toAngle = trackStartAngle + sliderPos * (trackEndAngle - trackStartAngle);

	if (slider.isEnabled()) {
	  Path valueArc;
	  valueArc.addCentredArc (
			bounds.getCentreX(),bounds.getCentreY(),
			arcRadius, arcRadius,
			0.0f,
			baselineAngle, toAngle,
			true
		);

	  g.setColour(Styling::trackFillColor);
	  g.strokePath(valueArc, PathStrokeType (Styling::trackFillWidth, PathStrokeType::curved, PathStrokeType::rounded));

	  auto glow = Colour(Styling::trackFillColor).withAlpha(0.05f);
	  g.setColour(glow);
	  for (int s = 0; s < 5; s++) {
			g.strokePath (valueArc, PathStrokeType (Styling::trackFillWidth + s, PathStrokeType::curved, PathStrokeType::rounded));
	  }
	}


	// ometer
	auto thumbWidth = Styling::trackFillWidth * 2;
	Point<float> centerPoint (bounds.getCentreX(), bounds.getCentreY());
	g.setColour(Styling::thumbColor);
	g.fillEllipse(
		Rectangle<float> (thumbWidth, thumbWidth).withCentre(centerPoint)
	);

	// auto ometerLength = arcRadius + (1*ROTARY_FILL_WIDTH);
	auto ometerLength = radius - Styling::trackWidth;
	auto magicForwardToAngle = toAngle + .05;
	Point<float> ometerTipPoint (
		bounds.getCentreX() + ometerLength * std::cos(magicForwardToAngle - MathConstants<float>::halfPi),
		bounds.getCentreY() + ometerLength * std::sin(magicForwardToAngle - MathConstants<float>::halfPi)
	);
	Path ometerLine;
	ometerLine.startNewSubPath(centerPoint);
	ometerLine.lineTo(ometerTipPoint);
	g.strokePath (ometerLine, PathStrokeType (Styling::trackWidth, PathStrokeType::curved, PathStrokeType::rounded));
}




inline void StraightSliderPaintLayer::paint(Graphics& g) {
	using Styling = RangeEditor::Styling;
	auto bounds = getLocalBounds().toFloat();
	
	auto y = bounds.getCentreY();
	g.setColour (Styling::trackColor);
	g.drawLine({bounds.getX(), y, bounds.getRight(), y}, Styling::trackWidth);
	
	auto baseline = slider.userInput->params.baseline;
	auto baselinePos = (float) slider.valueToProportionOfLength(baseline);
	auto sliderPos = (float) slider.valueToProportionOfLength (slider.getValue());
	
	auto baseX = (baselinePos * bounds.getWidth()) + bounds.getX();
	auto sliderX = (sliderPos * bounds.getWidth()) + bounds.getX();
	g.setColour(Styling::trackFillColor);
	g.drawLine({baseX, y, sliderX, y}, Styling::trackFillWidth);

	g.setColour(Styling::thumbColor);
	auto thumbWidth = 2;
	auto overhang = 5;
	g.fillRect(
		(int) sliderX - (thumbWidth/2),
		(int) y - (overhang + Styling::trackWidth),
		(int) thumbWidth,
		(int)   (2 * overhang) + Styling::trackWidth
	);
}

