#pragma once

#include <string_view>

struct RangeParams {
	string_view legacyId;
	string_view fullname;
	string_view shortname;
	float min = -1; 
	float baseline = 0;
	float max = 1;
	float defaultValue = 0;
	float grainularity = 0.01;
};
 