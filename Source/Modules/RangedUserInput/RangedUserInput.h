#pragma once

#import "RangeParams.h"


//************** USER INPUT MOD TYPE **************************/
enum class UserInputModType {
	ROTARY,
	VERTICAL_SLIDER,
	HORIZONTAL_SLIDER,
};



//************** MODLESS USER INPUT **************************/
class RangedUserInput :  public Observable<double> {
public:
	const RangeParams& params;
	RangedUserInput(const RangeParams& params)
	: Observable<double>(params.defaultValue),
		params(params)
	{}

	virtual unique_ptr<Slider> createRotary() {
		return createSlider(UserInputModType::ROTARY);
	}
	virtual unique_ptr<Slider> createHorizontalFader() {
		return createSlider(UserInputModType::HORIZONTAL_SLIDER);
	}
	virtual unique_ptr<Slider> createVerticalFader() {
		return createSlider(UserInputModType::VERTICAL_SLIDER);
	}

	virtual unique_ptr<Slider> createSlider(UserInputModType type);
};




#import "RangeEditor.h"

inline unique_ptr<Slider> RangedUserInput::createSlider(UserInputModType type) {
	return make_unique<RangeEditor>(this, type);
}

