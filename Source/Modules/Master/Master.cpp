#include "Master.h"
#include "../../PluginProcessor.h"

Master::Master(PluginProcessor* plugin): plugin(plugin) {
	plugin->routingManager.addTarget(&(Master::GAIN));
	plugin->routingManager.registerEndpoint(&gain);
	// gain.addSampleSource(routingManager.getModChainMirrorForEndpoint(&gain));

	voiceCount.observe([&](int newCount){
		plugin->routingManager.updateVoiceCount(newCount);
		plugin->sourceManager.updateVoiceCount(newCount);
	});
}

void Master::renderNextBlock (
	AudioBuffer<float>& buffer,
	const MidiBuffer& midiMessages,
	int startSample,
	int numSamples
) {
	plugin->sourceManager.renderNextBlock(buffer, midiMessages, startSample, numSamples);

	auto gainModulation = gain.getNextSamplesReadonly(numSamples);
	if (gainModulation->canBeIgnored(0.0) == false) {
		auto gainUnitas = gainModulation->map([](float sample){ return Decibels::decibelsToGain(sample);});
		for (auto c = buffer.getNumChannels(); --c >= 0;) {
			auto modMe = buffer.getWritePointer(c, startSample);
			for(int s = 0; s < numSamples; s++) {
				modMe[s] *= gainUnitas->get(s);
			}
		}
	}

	// if (gain.canBeIgnored()) {
	// 	// do nothing
	// } else if (gain.isAudioRate() == false) {
	// 	auto sample = gain.getNextSample();
	// 	if (sample != 0) {
	// 		auto gainUnita = Decibels::decibelsToGain(sample);
	// 		buffer.applyGain(startSample, numSamples, gainUnita);
	// 	}

	// } else {
	// 	auto modulation = gain.getNextSamplesReadonly(numSamples);
	// 	// TODO: make this not self fill
	// 	float gainUnitas[numSamples];
	// 	// std::vector<float> gainUnitas(numSamples);
	// 	for(int s = 0; s < numSamples; s++) {
	// 		gainUnitas[s] = Decibels::decibelsToGain((*modulation)[s]);
	// 	}
	// 	for (auto c = buffer.getNumChannels(); --c >= 0;) {
	// 		auto modMe = buffer.getWritePointer(c, startSample);
	// 		for(int s = 0; s < numSamples; s++) {
	// 			modMe[s] *= gainUnitas[s];
	// 		}
	// 	}
		
	// }
}


Component* Master::getEditor() {
	return new MasterEditor(this);
}
