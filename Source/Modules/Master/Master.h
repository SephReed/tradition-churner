#pragma once
#include <memory>
using namespace std;

#import "../../MasterEnums.h"
#import "../Routing/ModulationEndpoint.h"
#import "../../Util/DesignatedInit.h"

class PluginProcessor;

class Master {
public:
	PluginProcessor* plugin;
	
	static inline ModulationTarget GAIN = DesignatedInit(
 		ModulationTarget,
// 		$.id = ModTargetId::MASTER_GAIN,
 		$.fullname = "Master Gain",
 		$.shortname = "Gain",
 		$.canBePerSynth = false,
 		$.canBePerVoice = false,
 		$.optimizeForAllSynth = true,
	  $.min = -70,
	  $.max = 10
 	);
	ModulationEndpoint gain{&(Master::GAIN), DesignatedInit(
		ModLocation,
		$.type = ModLocationType::MASTER_ALL
	), true};

	Observable<int> voiceCount = {4};

	Master(PluginProcessor* plugin);

	void renderNextBlock (
		AudioBuffer<float>& buffer,
		const MidiBuffer& midiMessages,
		int startSample,
		int numSamples
	);

	Component* getEditor();
};


class MasterEditor : public Component {
public:
	Master* master;
	unique_ptr<Slider> gain;

	void paint (Graphics& g) override {
		// g.fillAll(Colour(250, 40, 40));
	}
	void resized() override {
		
	}
	MasterEditor(Master* master)
		: master(master),
			gain(master->gain.userInput->createRotary())
	{
		gain->setBounds(5, 5, 50, 50);
		addAndMakeVisible(gain.get());
	}
};
