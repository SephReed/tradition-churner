#pragma once
using namespace std;



//************** MODULATION TARGETS **************************/
#import "../Routing/UserInput/ModulationTarget.h"
#import "../../Util/DesignatedInit.h"

struct CropperModTargets : public RegisterTargetsInRoutingManager<CropperModTargets> {

  static inline constexpr ModulationTarget cropCeiling = DesignatedInit(ModulationTarget,
    $.legacyId = "CROPPER_CEILING",
    $.fullname = "Cropper Ceiling",
    $.shortname = "Ceil",
    $.canBePerVoice = true,
    $.min = 0.01,
    $.baseline = 1,
    $.defaultValue = 1,
    $.max = 1
  );

  static inline constexpr ModulationTarget ampLoss = DesignatedInit(ModulationTarget,
    $.legacyId = "CROPPER_AMP_LOSS",
    $.fullname = "Cropper Amplitude Loss",
    $.shortname = "Amp Loss",
    $.canBePerVoice = true,
    $.min = 0,
    $.baseline = 0,
    $.defaultValue = 0.33,
    $.max = 1
  );

  static inline std::vector<const ModulationTarget*>& modTargetList() {
    static std::vector<const ModulationTarget*> output = {
      &cropCeiling,
      &ampLoss
    };
    return output;
  };
};


//************** CROPPER MODES **************************/
enum class CropperMode {
  FULL_DROP,
  MIN_DROP,
  BOUNCE,
  BOUNCE_DROP
};



//************** CROPPER PROCESSING LAYER **************************/
#import "ProcessingLayer.h"
class Cropper;

class CropperLayer: public ProcessingLayer {
public:
  Cropper& factory;

  ModulationEndpoint cropCeiling;
  ModBuffer cropCeilingSamples;

  ModulationEndpoint ampLoss;
  ModBuffer ampLossSamples;

	virtual ~CropperLayer(){}
  CropperLayer(Cropper& factory, int voiceNum);

  void resetCache(int numSamples) final {
    cropCeilingSamples = cropCeiling.getNextSamplesReadonly(numSamples);
    ampLossSamples = ampLoss.getNextSamplesReadonly(numSamples);
  }

  void processSamples(WaveRenderingBuffer& modMe) final;

  void registerEndpoints(RoutingManager& routeMgmt) final {
    routeMgmt.registerEndpoint(&cropCeiling);
    routeMgmt.registerEndpoint(&ampLoss);
  }
};


//************** CROPPER **************************/
#include "../Routing/RegisterTargetsInRoutingManager.h"

class Cropper
: public ProcessingLayerFactory
{
public:
  UserInputMod cropCeiling;
  UserInputMod ampLoss;
	Observable<CropperMode> mode { CropperMode::BOUNCE };
	
  Cropper(WaveSource& source)
  : ProcessingLayerFactory(source),
		cropCeiling(CropperModTargets::cropCeiling, source),
    ampLoss(CropperModTargets::ampLoss, source)
	{}

  unique_ptr<ProcessingLayer> createProcessingLayer(int voiceNum = -1);

  float processSampleWithoutModulation(float amp) final {
//    return fmod(amp, cropCeiling.get());
		return processSample(amp, cropCeiling.get(), mode.get(), ampLoss.get());
  }
	
  float inline processSample(float sample, float cropAmount, CropperMode mode, float ampLoss) {
  	if (cropAmount >= 1) return sample;
		if (mode == CropperMode::FULL_DROP) {
			sample = fmod(sample, cropAmount);

		} else if (mode == CropperMode::MIN_DROP) {
			bool isCropped = abs(sample) > cropAmount;
			sample = fmod(sample, cropAmount);
			if (isCropped && cropAmount > 0.5) {
				auto bottom = cropAmount - (1 - cropAmount);
				sample += sample > 0 ? bottom : -bottom;
			}

		} else if (mode == CropperMode::BOUNCE || mode == CropperMode::BOUNCE_DROP) {
			int bounces = floor(abs(sample) / cropAmount);
			sample = fmod(sample, cropAmount);

			if (bounces % 2 == 1) {
				sample *= -1;
				if (mode == CropperMode::BOUNCE) {
					sample += sample < 0 ? cropAmount : -cropAmount;
				}
			}

		} else {
			assert(false);
		}
		
    if (ampLoss < 1) {
      auto drop = 1 - cropAmount;
      auto gainTo = 1 - (drop * ampLoss) ;
      auto scale = gainTo / cropAmount;
      sample *= scale;
    }
		return sample;
	}
};



//************** CROPPER EDITOR **************************/
class CropperEditor: public Component {
public:
  Cropper& cropper;
  unique_ptr<Slider> ceil;
  unique_ptr<Slider> ampLoss;

  CropperEditor(Cropper& cropper)
  : cropper(cropper),
    ceil(cropper.cropCeiling.createRotary()),
    ampLoss(cropper.ampLoss.createRotary())
  {
    addAndMakeVisible(ceil.get());
    addAndMakeVisible(ampLoss.get());
  }

  void resized() override {
    ceil->setBounds(0, 0, 50, 50);
    ampLoss->setBounds(0, 50, 50, 50);
  }
};



//************** INLINE CPP **************************/
#include "../../PluginProcessor.h"

inline CropperLayer::CropperLayer(Cropper& factory, int voiceNum) 
: ProcessingLayer(voiceNum),
  cropCeiling(factory.cropCeiling.createEndpoint(voiceNum)),
  ampLoss(factory.ampLoss.createEndpoint(voiceNum)),
  factory(factory)
{
//  factory.source.plugin->routingManager.registerEndpoint(&cropCeiling);
}

inline void CropperLayer::processSamples(WaveRenderingBuffer& modMe) {
	auto currentMode = factory.mode.get();
  auto& raw = modMe.raw();
  int rawIndex = 0;
	for (int s = 0; s < modMe.numSamples; s++) {
		auto cropAmount = cropCeilingSamples->get(s);
		if (cropAmount < 1) {
      for (int w = 0; w < modMe.numWaves; w++) {
        if (modMe.hasWave[w]) {
          for (int c = 0; c < modMe.numChannels; c++) {
            raw[rawIndex] = factory.processSample(raw[rawIndex], cropAmount, currentMode, ampLossSamples->get(s));
            rawIndex++;
          }
        } else {
          rawIndex += modMe.numChannels;
        }
      }
		}
	}
}


inline unique_ptr<ProcessingLayer> Cropper::createProcessingLayer(int voiceNum) {
  return make_unique<CropperLayer>(*this, voiceNum);
}
