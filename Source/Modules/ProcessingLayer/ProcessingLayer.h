#pragma once
using namespace std;

#include "../WaveSource/WaveSource.h"
#include "../WaveSource/WaveRenderingBuffer.h"

class RoutingManager;

class ProcessingLayer {
public:
	int voiceNum;
	ProcessingLayer(int voiceNum = -1): voiceNum(voiceNum) {}
	virtual ~ProcessingLayer(){}
	virtual void resetCache(int numSamples) = 0;
	virtual void processSamples(WaveRenderingBuffer& modMe) = 0;
	virtual void registerEndpoints(RoutingManager& routeMgmt) = 0;
	
};

class ProcessingLayerFactory {
public:
	WaveSource& source;
	ProcessingLayerFactory(WaveSource& source) : source(source) {

	}
	virtual ~ProcessingLayerFactory(){}
	virtual float processSampleWithoutModulation(float sample) = 0;
	virtual unique_ptr<ProcessingLayer> createProcessingLayer(int voiceNum = -1) = 0;
};
