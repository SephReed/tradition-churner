#### Heirarchy

- PluginProcessor
	- SourceManager
		- WaveSource
			- ::StableWaveSource
				- ::SmoothWaveSource
			- UserInputMod
			- WaveSourceVoice
				- ModulationEndpoint
				
	- RoutingManager
		- RoutingSet
			- ModulationRouting
		- TCParameterManager
			- TCParameter
			- ModulationTarget
		- ModBundleCatergories

	- ModifierManager
		- Modifier
			- ::ADSR
			- ::LFO
				- ::LFOSequence
			- ModSampleSource
				- ::ModChain
				- ModLocation
				- ReadonlyModBuffer