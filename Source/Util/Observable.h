#pragma once
using namespace std;

#include <mutex>
#include <vector>
#include <functional>
#include <iostream>

template <typename TYPE>
class Observable {
public:
	Observable<TYPE>(TYPE startValue): value(startValue) {}
	Observable<TYPE>(TYPE startValue, std::function<void(TYPE)> lookyLoo, bool skipFirst = false): value(startValue) {
		observe(lookyLoo, skipFirst);
	}
	TYPE get() const { return value; }
	void setSilently(TYPE newVal) {
		value = newVal;
	}
	void set(TYPE newVal) {
//		std::cout << "OBSERVABLE " << newVal << std::endl;
		bool changed = value != newVal;
		value = newVal;
		for (auto lookyLoo : observers) {
			lookyLoo(value);
		}
		if (changed) {
			for (auto listener : changeListeners) {
				listener();
			}
		}
	}
	void observe(std::function<void(TYPE)> lookyLoo, bool triggerImmediately = true) {
		addObserverMutex.lock();
		observers.push_back(lookyLoo);
		addObserverMutex.unlock();
		if (triggerImmediately) { lookyLoo(value); }
	}
	void onChange(std::function<void()> listener, bool triggerImmediately = false) {
		changeListenerMutex.lock();
		changeListeners.push_back(listener);
		changeListenerMutex.unlock();
		if (triggerImmediately) { listener(); }
	}

private:
	TYPE value;
	std::mutex addObserverMutex;
	std::vector<std::function<void(TYPE)>> observers;
	std::mutex changeListenerMutex;
	std::vector<std::function<void()>> changeListeners;
};



// template <typename TYPE>
// class ObservableArray {
// public:
// 	using ArrayChangeObserver = std::function<void(TYPE, bool)>;
// 	using TypeList = std::vector<TYPE>;

// 	ObservableArray<TYPE>(TypeList startValues)
// 		: values(startValues) {}
	
// 	ObservableArray<TYPE>(
// 		TypeList startValues, 
// 		ArrayChangeObserver lookyLoo, 
// 		bool skipFirst = false
// 	)	: values(startValues)
// 	{
// 		observe(lookyLoo, skipFirst);
// 	}

// 	TYPE get(int index) { return values[index]; }
	
// 	int size() {
// 		return values.size();
// 	}

// 	void set(int index, TYPE newVal) {
// 		values[index] = newVal;
// 		for (auto obsv : observers) {
// 			obsv(newVal, true);
// 		}
// 	}

// 	void removeItemAtIndex(int index) {
// 		TYPE removeMe = values[index];
// 		values[index] = NULL;
// 		for (auto obsv : observers) {
// 			obsv(removeMe, false);
// 		}
// 	}

// 	// void removeItem(TYPE removeMe) {
// 	// 	removeItemAtIndex(values.indexOf(removeMe));
// 	// }

// 	void observe(ArrayChangeObserver lookyLoo, bool triggerImmediately = true) {
// //		addObserverMutex.lock();
// 		observers.push_back(lookyLoo);
// //		addObserverMutex.unlock();
// 		if (triggerImmediately) {
// 			for (auto value : values) {
// 				lookyLoo(value, true);
// 			}
// 		}
// 	}

// private:
// 	TypeList values;
// //	std::mutex addObserverMutex;
// 	std::vector<ArrayChangeObserver> observers;
// };
