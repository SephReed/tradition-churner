#pragma once
using namespace std;


template <typename CREATOR_TYPE, CREATOR_TYPE>
struct ForceStaticProxyInit { };

// template<class CREATOR_TYPE, class RUN_CONSTRUCTOR>
// class RegisterTargetsInRoutingManager {
// public:
//   struct StaticRunProxy {
//     StaticRunProxy() {
//       RoutingManager::addModTargetList(CREATOR_TYPE::modTargets.list);
//     }
//   };

//   static StaticRunProxy staticProxy;
//   typedef ForceStaticProxyInit<StaticRunProxy&, staticProxy> __dummy;
// };

// template<class CREATOR_TYPE>
// typename RegisterTargetsInRoutingManager<CREATOR_TYPE>::StaticRunProxy RegisterTargetsInRoutingManager<CREATOR_TYPE>::staticProxy;