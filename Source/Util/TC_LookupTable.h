#pragma once


class TC_LookupTable {
public:
	struct LookupSample {
		float value;
		float dyToNext = 0;
	};

	float min;
	float max;
	int numSamples;
	float range;
	float stepSize;
	vector<LookupSample> samples;

	TC_LookupTable(function<float(float)> gen, int i_numSamples, float i_min = 0, float i_max = 1)
	: min(i_min),
		max(i_max),
		numSamples(i_numSamples),
		range(max-min),
		stepSize(range / (numSamples - 1))
	{
		assert(numSamples > 1);
		assert(min < max);
		assert(abs((stepSize * (numSamples-1)) - i_max) < 0.0001);

		for (int s = 0; s < numSamples; s++) {
			samples.push_back({gen(min + (s * stepSize)), 0});
		}
		for (int s = 0; s < numSamples; s++) {
			if (s < numSamples-1) {
				auto nextSample = samples[s+1];
				samples[s].dyToNext = nextSample.value - samples[s].value;
			}
		}
	}

	float operator[](float value) const {
		float indexFloat = (value - min) / stepSize;
		int intIndex = indexFloat;
		auto& sample = samples[intIndex];
		return sample.value + (sample.dyToNext * (indexFloat - intIndex));
	}
};
