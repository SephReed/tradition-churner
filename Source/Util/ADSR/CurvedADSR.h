#pragma once

#import "ADSR.h"

class CurvedADSR
 : public ADSR
{
	public:
		CurvedADSR() {}

		struct Parameters: public ADSR::Parameters {
			float curve = 0.2f;
		};

		void reset() {
			ADSR::reset();
			releaseStartGain = -1.0f;
			releaseLossLinear = 0.0f;
			releaseLossCurved = 1.0f;
			linearEndGain = 1.0f;
		}
	
		// void noteOff() {
		// 	if (currentState != State::idle) {
		// 		if (releaseRate > 0.0f) {
		// 			currentState = State::release;
		// 		}
		// 		else
		// 			reset();
		// 	}
		// }

		float getNextSample() {
			if (currentState == State::release) {
				if (releaseStartGain == -1) {
					releaseStartGain = envelopeVal;
				}

				if (releaseLossLinear < linearEndGain) {
					releaseLossLinear += releaseRateLinear;
				}

				releaseLossCurved *= releaseRateCurved;
				envelopeVal = (releaseStartGain * releaseLossCurved) - releaseLossLinear;
				
				if (envelopeVal <= 0.00000001f) { 
					reset(); 
				}
				return envelopeVal;

			} else {
				return ADSR::getNextSample();
			}
		}

		void setParameters (const Parameters& newParameters) {
			currentParameters = newParameters;
			sustainLevel = newParameters.sustain;
			calculateRates(newParameters);
		}

	private:
		static constexpr float linearPortion = 0.5; // the linear drop is hidden in the first half of release
		static constexpr float standardCurve = 0.2f;
		static constexpr float minimumGainThreshold = 0.01f;
		// standard length(Z): the length of a standardCurve which ends at gain = minimumGainThreshold.
		static const float computeStandardLength() { return log(minimumGainThreshold)/log(standardCurve); }
	
		const float standardLength = CurvedADSR::computeStandardLength();

		// full equation:  gain = C^Zx - xC^Z,  
		// where C is the curve constant (0.2 is linear decibel drop)
		// and Z is width of the curve (in this case, where gain=0.01 when C=0.2)
		virtual void calculateRates (const Parameters& parameters) {
			ADSR::calculateRates(parameters);

			if (parameters.release > 0.0f) {
				calculateReleaseRates(parameters);
			}
		}

		void calculateReleaseRates(const Parameters& parameters) {
			float mix = pow(parameters.curve, standardLength);
			linearEndGain = mix;
			float inverseTotal = 1 / (sr * parameters.release);
			releaseRateLinear = mix * inverseTotal * CurvedADSR::linearPortion;
			releaseRateCurved = pow(mix, inverseTotal);
		}

		float releaseRateLinear = 0.0f;
		float releaseRateCurved = 0.0f;

		// keep track of linear and curved loss separately
		float releaseStartGain = 0.0f;
		float linearEndGain = 0.0f;
		float releaseLossLinear = 0.0f;
		float releaseLossCurved = 0.0f;
};
