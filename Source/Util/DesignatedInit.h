#pragma once

#define DesignatedInit(T, ...)\
	[]{ T ${}; __VA_ARGS__; return $; }()