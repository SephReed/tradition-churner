#pragma once
#import "../../JuceLibraryCode/JuceHeader.h"

struct VerticalSplits;
struct HorizontalSplits;

class SectionedRectangle: public Rectangle<int> {
public:
	using Rectangle::Rectangle;

	SectionedRectangle (const Rectangle& copyMe): Rectangle(copyMe) {}

	void padInwards(int padding) {
		// translate(padding, padding);
		reduce(padding*2, padding*2);
	}

	HorizontalSplits splitHorizontallyAtRatio(double ratio, int gap = 0);

	VerticalSplits splitVerticallyAtRatio(double ratio, int gap = 0);

	vector<SectionedRectangle> splitCols(int numCols, int gap = 0) {
		float width = (getWidth() - ((numCols-1) * gap)) / numCols;
		float x = getX();
		vector<SectionedRectangle> out;
		for (int c = 0; c < numCols; c++) {
			out.push_back(SectionedRectangle(x, getY(), width, getHeight()));
			x += width + gap;
		}
		return out;
	}

	vector<SectionedRectangle> splitRows(int numRows, int gap = 0) {
		float height = (getHeight() - ((numRows-1) * gap)) / numRows;
		float y = getY();
		vector<SectionedRectangle> out;
		for (int r = 0; r < numRows; r++) {
			out.push_back(SectionedRectangle(getX(), y, getWidth(), height));
			y += height + gap;
		}
		return out;
	}


};

struct VerticalSplits {
	SectionedRectangle top;
	SectionedRectangle bottom;
};

struct HorizontalSplits {
	SectionedRectangle left;
	SectionedRectangle right;
};



inline HorizontalSplits SectionedRectangle::splitHorizontallyAtRatio(double ratio, int gap) {
	assert(ratio > 0 && ratio < 1);
	auto halfGap = gap/2.0;
	auto leftWidth = (getWidth() * ratio) - halfGap;
	auto rightWidth = (getWidth() - leftWidth) - halfGap;

	HorizontalSplits out;
	out.left = SectionedRectangle(getX(), getY(), leftWidth, getHeight());
	out.right = SectionedRectangle(getRight() - rightWidth, getY(), rightWidth, getHeight());
	return out;
}

inline VerticalSplits SectionedRectangle::splitVerticallyAtRatio(double ratio, int gap) {
	assert(ratio > 0 && ratio < 1);
	auto halfGap = gap/2.0;
	auto topHeight = (getHeight() * ratio) - halfGap;
	auto bottomHeight = (getHeight() - topHeight) - halfGap;

	VerticalSplits out;
	out.top = SectionedRectangle(getX(), getY(), getWidth(), topHeight);
	out.bottom = SectionedRectangle(getX(), getBottom() - bottomHeight, getWidth(), bottomHeight);
	return out;
}
