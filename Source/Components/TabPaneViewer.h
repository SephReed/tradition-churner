#pragma once
#include "./ComponentStyler.h"

using namespace ComponentStyling;
using namespace std;


class Tab: public Component {
public:
	struct Styling {
	 	static inline PixelARGB tabBg = TC_Color::bg;
		static inline PixelARGB selectedTabBg = TC_Color::bg;
		static inline PixelARGB unselectedTabBg = TC_Color::bgBold1;

		static inline PixelARGB tabFg = TC_Color::fg;
		static inline PixelARGB hoveredTabFg = TC_Color::fgFade1;
		static inline PixelARGB unselectedTabFg = TC_Color::fgFade3;
	};

	Tab(string i_text): text(i_text) {
		setBufferedToImage(true);
	}
	void resized() override {}
	void paint(Graphics& g) override {
		g.fillAll(Styling::tabBg);
		auto bounds = g.getClipBounds();
		if (isSelected) {
			g.fillAll(Styling::selectedTabBg);
			float top = bounds.getTopLeft().getY();
			float left = bounds.getTopLeft().getX();
			float width = bounds.getWidth();
			float height = 3;
			ColourGradient gradient = {
				TC_Color::accentStart, left, top,
				TC_Color::accentEnd, left+width, top+height,
				true
			};
			g.setGradientFill(gradient);
			g.fillRect(Rectangle<float>(left, top, width, height));
			
		} else {
			g.fillAll(Styling::unselectedTabBg);
		}

//		TC_Colors::Enum textColor = TC_Colors::fg;
//		if (isHovered) {
//			textColor = getColorFromId(TC_UI_Ids::hoveredTabFg);
//		} else if (isSelected == false) {
//			textColor = getColorFromId(TC_UI_Ids::unselectedTabFg);
//		}
//		g.setColour(findColour(textColor));
		g.setColour(isHovered ? Styling::hoveredTabFg : (isSelected ? Styling::tabFg : Styling::unselectedTabFg));
		g.setFont(10.0f);
		g.drawFittedText(text, 0, 0, getWidth(), 25, Justification::centred, 1);
	}
	void onClick(const function<void()> &i_onSelect) {
		onSelect = i_onSelect;
	}
	void setSelected(bool shouldBeSelected) {
		isSelected = shouldBeSelected;
		repaint();
	}
	void mouseEnter(const MouseEvent &event) override { isHovered = true; repaint(); }
	void mouseExit(const MouseEvent &event) override { isHovered = false; repaint(); }
 	 void mouseDown(const MouseEvent &event) override { onSelect(); }
 	bool getIsSelected() { return isSelected; }
	
private:
	string text;
	bool isSelected = false;
	bool isHovered = false;
	function<void()> onSelect = []() { return 0; };
};




class TabPaneViewer: public Component {
public: 
	TabPaneViewer(){ setBufferedToImage(true); }

	void addTabs(vector<string> tabNames) {
		for (auto& tabName : tabNames) {
			Tab* tab = new Tab(tabName);
			addAndMakeVisible(tab);
			TabPane tabPane = {tab, NULL};
			tab->onClick([&, tabPane](void) {
	      viewTabPane(tabPane);
	    });
			tabPanes.push_back(tabPane);
	    resized();
		}
	}

	void addPane(string tabText, Component* pane) {
		addAndMakeVisible(pane);
		Tab* tab = new Tab(tabText);
		addAndMakeVisible(tab);
		const TabPane tabPane = {tab, pane};
    tab->onClick([this, tabPane](void) {
      this->viewTabPane(tabPane);
    });

    if (pane && (tabPanes.size() != 0 || selectedTabPane.tab != NULL)) {
    	pane->setVisible(false);
    }
		tabPanes.push_back(tabPane);
		resized();
	}

	void setPane(int target, Component* pane) {
		assert(target < tabPanes.size());
		auto tabPane = tabPanes[target];
		if (tabPane.pane) { removeChildComponent(tabPane.pane); }
		tabPane.pane = pane;
		addChildComponent(tabPane.pane);
		if (tabPane.tab->getIsSelected() && tabPane.pane) {
			tabPane.pane->setVisible(true);
		}
	}

	void selectIndex(int index) {
		viewTabPane(tabPanes[index]);
	}

	void paint(Graphics& g) override {
		// g.fillAll(Colour(250, 40, 40));
	}

	void resized() override {
		auto bounds = getLocalBounds();
		if (bounds.getWidth() == 0 || bounds.getHeight() == 0) { return; }
		// bounds.setTop(bounds.getY() + 20);
		Rectangle<int> paneBounds {0, 20, bounds.getWidth(), bounds.getHeight() - 20};
		int tabWidth = bounds.getWidth() / tabPanes.size();
		int excess = bounds.getWidth() % tabWidth;
		for (int tp = 0; tp < tabPanes.size(); ++tp){
			auto& tabPane = tabPanes[tp];
			if (tabPane.pane) { tabPane.pane->setBounds(paneBounds); }
			if (tp == 0) {
				tabPane.tab->setBounds(0, 0, tabWidth + excess, 20);
			} else {
				tabPane.tab->setBounds((tp*tabWidth) + excess, 0, tabWidth, 20);
			}
		}
	}

private:
	struct TabPane {
		Tab* tab = NULL;
		Component* pane = NULL;
	};
	vector<TabPane> tabPanes;
	TabPane selectedTabPane;
	void viewTabPane(const TabPane &viewMe) {
		if (selectedTabPane.tab != nullptr) {
			selectedTabPane.tab->setSelected(false);
			if (selectedTabPane.pane) { selectedTabPane.pane->setVisible(false); }
		}
		selectedTabPane = viewMe;
		selectedTabPane.tab->setSelected(true);
		if (selectedTabPane.pane) { selectedTabPane.pane->setVisible(true); }
		repaint();
	}
};
