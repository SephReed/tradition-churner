#pragma once
#include <map>
#include "../../JuceLibraryCode/JuceHeader.h"

using namespace juce;

namespace ComponentStyling {
	struct TC_Color {
		static inline PixelARGB accentStart {255, 80, 227, 194};
		static inline PixelARGB accentEnd 	{255, 80, 227, 151};
		static inline PixelARGB accentFade 	{255, 188, 255, 203};

		static inline PixelARGB bgBold1 		{100, 0, 0, 0};
		static inline PixelARGB bg 					{255, 40, 40, 40};
		static inline PixelARGB bgFade1 		{20, 255, 255, 255};

		static inline PixelARGB fg 					{255, 255, 255, 255};
		static inline PixelARGB fgFade1 		{255, 200, 200, 200};
		static inline PixelARGB fgFade2 		{255, 160, 160, 160};
		static inline PixelARGB fgFade3 		{255, 120, 120, 120};
		static inline PixelARGB fgFade4 		{255, 80, 80, 80};
	};


	namespace TraditionChurnerStyling {
		static int TEXT_HEIGHT = 10;

		static int BIG_KNOB_DIAMETER = 45;
		static int MED_KNOB_DIAMETER = 35;
	}
};

