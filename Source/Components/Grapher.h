#pragma once

#import "ComponentStyler.h"

using namespace ComponentStyling;

class Grapher : public Component {
public:
	std::function<float(float)> fx;
	float min;
	float max;

	Grapher(std::function<float(float)> i_fx, float i_min = -1, float i_max = 1)
		: fx(i_fx), min(i_min), max(i_max) 
	{}

	void paint (Graphics& g) override {
		// g.fillAll (Colours::wheat);
		auto pad = 3;
		auto bounds = g.getClipBounds();
		float top = bounds.getTopLeft().getY() + pad;
		float left = bounds.getTopLeft().getX() + pad;
		float width = bounds.getWidth() - (2*pad);
		float height = bounds.getHeight() - (2*pad);
		ColourGradient gradient = {
			TC_Color::accentStart, left, top,
			TC_Color::accentEnd, left+width, top+height,
			true
		};
		g.setGradientFill(gradient);

		auto fillMe = Rectangle<float>(left, top, width, height);
		g.fillRoundedRectangle(fillMe, 12);
		g.setColour(TC_Color::accentFade);
		g.drawRoundedRectangle(fillMe, 12, 1);

		gradient.multiplyOpacity(0.05);
		g.setGradientFill(gradient);

		for (int s = 0; s < pad; s++) {
			fillMe.setPosition(fillMe.getX() -1, fillMe.getY() -1);
			fillMe.setSize(fillMe.getWidth() +2, fillMe.getHeight() +2);
			g.fillRoundedRectangle(fillMe, 12);
		}

    Path myPath;
    auto margin = 10;
    auto marginedHeight = height - (2* margin);
//    myPath.startNewSubPath (-2, halfHeight);
		auto fullWidth = bounds.getWidth();
		float range = max - min;
    for (int x = 0; x < fullWidth; x++) {
      double ratioX = x / (double)fullWidth;
      // double y = (double)(wave->getFrameAtCyclePos(ratio) * halfHeight) + halfHeight;
      // sin(ratio * 3.1415 * 2)
      double ratioResult = fx(ratioX);
      double ratioY = (ratioResult - min) / range;
      double y = (marginedHeight * (1-ratioY)) + (pad + margin);
			
			if (x == 0) {
				myPath.startNewSubPath (x, y);
			} else {
      	myPath.lineTo(x, y);
			}
    }
//		myPath.lineTo(width+2, halfHeight);
    g.setColour(TC_Color::bg);
    g.strokePath (myPath, PathStrokeType(5.0f));
	}

	void resized() override {
	}
};
