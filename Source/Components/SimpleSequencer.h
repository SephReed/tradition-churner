#pragma once

#include <list>

class SimpleSequencer: public Component {
public:
	struct StepChange {
		int stepNum;
		bool value;
		bool operator!=(StepChange checkMe) { return checkMe.stepNum != stepNum || checkMe.value != value; }
		StepChange(int stepNum, bool value): stepNum(stepNum), value(value) {}
	};
	Observable<StepChange> stepChanges {StepChange(-1, false)};
	list<BacklitButton> buttons;

	SimpleSequencer(int numSteps = 8) {
		setNumSteps(numSteps);
	}

	void setNumSteps(int numSteps) {
		int oldSize = (int)buttons.size();
		assert(numSteps > oldSize);
		buttons.resize(numSteps);
		
		auto iter = buttons.begin();
		for (int b = 0; b < buttons.size(); b++) {
			auto& button = *iter;
			if (b >= oldSize) {
				addAndMakeVisible(button);
				button.onClick = [&, b](){
					stepChanges.set(StepChange(b, button.getToggleState()));
				};
			}
			++iter;
		}
	}

	void resized() override {
		auto width = getWidth();
		auto numButtons = buttons.size();
		auto gap = 2;
		auto gapLoss = (numButtons - 1) * gap;
		auto buttonWidth = (width - gapLoss)/buttons.size();

		auto left = getX();
		for (auto& button : buttons) {
			button.setBounds(left, getY(), buttonWidth, buttonWidth);
			left += buttonWidth + gap;
		}
	}


};
