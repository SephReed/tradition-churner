#pragma once

#import "ComponentStyler.h"

using namespace ComponentStyling;

class BacklitButton: public Button {
public:
	static constexpr int BORDER_WIDTH = 2;
	static constexpr int BORDER_RADIUS = 5;

	bool ignoreNextUpdate = false;
	Observable<bool>* watchMe = NULL;

	BacklitButton() : Button("Backlit Button") {
		setClickingTogglesState(true);
	}

	// BacklitButton(BacklitButton&& egg) noexcept: Button("Backlit Button") {};
	
	void paintButton(juce::Graphics &g, bool isHovered, bool isActive) final {
		auto isSelected = getToggleState();

		auto bounds = getLocalBounds().toFloat();
		bounds.reduce(BORDER_WIDTH/2, BORDER_WIDTH/2);
		if (isHovered || isSelected) {
			ColourGradient gradient = {
				TC_Color::accentStart, bounds.getX(), bounds.getY(),
				TC_Color::accentEnd, bounds.getRight(), bounds.getBottom(),
				true
			};

			if (isHovered) { gradient.multiplyOpacity(isSelected ? 0.8 : 0.2); }
			g.setGradientFill(gradient);
		} else {
			g.setColour(TC_Color::bgFade1);
		}
		g.fillRoundedRectangle(bounds, BORDER_RADIUS);	

		g.setColour(isSelected ? TC_Color::accentFade : TC_Color::fgFade4);
		g.drawRoundedRectangle(bounds, BORDER_RADIUS, BORDER_WIDTH);
	}

	void attachToObservable(Observable<bool>& i_watchMe) {
		watchMe = &i_watchMe;
		watchMe->observe([&](bool newVal){
//			ignoreNextUpdate = true;
			setToggleState(newVal, NotificationType::sendNotification);
		});
	}

	void clicked() override {
		cout << "BUTTON " << getToggleState() << endl;
//		if (ignoreNextUpdate == false && watchMe) {
//			watchMe->set(getToggleState());
//		} else {
//			ignoreNextUpdate = false;
//		}
		if (watchMe) {
			watchMe->set(getToggleState());
		}
		repaint();
	}
	
};
