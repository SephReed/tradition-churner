#pragma once

//TODO: clean up memory links and bad pointers
//TODO: use const wherever possible

#include "../JuceLibraryCode/JuceHeader.h"
#include "Modules/WaveSource/SourceManager.h"
#import "Modules/WaveSource/SubBass/SubBass.h"

#include "Modules/Routing/RoutingManager.h"
#include "Modules/Modifier/ModifierManager.h"

class PluginProcessor  
	:	public AudioProcessor
{
public:
	//==============================================================================
	PluginProcessor();
	~PluginProcessor();
  TestModifier testMod;
  RoutingManager routingManager{this};
  SourceManager sourceManager{this};
  ModifierManager modifierManager{this};
  Master master{this};

	
	//==============================================================================
	void prepareToPlay (double sampleRate, int samplesPerBlock) override;
	void releaseResources() override;
	bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
	void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

	//==============================================================================
	AudioProcessorEditor* createEditor() override;
	bool hasEditor() const override;

	//==============================================================================
	const String getName() const override;

	bool acceptsMidi() const override;
	bool producesMidi() const override;
	bool isMidiEffect() const override;
	double getTailLengthSeconds() const override;

	//==============================================================================
	int getNumPrograms() override;
	int getCurrentProgram() override;
	void setCurrentProgram (int index) override;
	const String getProgramName (int index) override;
	void changeProgramName (int index, const String& newName) override;

	//==============================================================================
	void getStateInformation (MemoryBlock& destData) override;
	void setStateInformation (const void* data, int sizeInBytes) override;

private:
	//==============================================================================
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginProcessor)
};


#include "Modules/WaveSource/StableWave/StableWaveSynth.h"
