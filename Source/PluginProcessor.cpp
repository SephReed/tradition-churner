#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Modules/WaveSource/StableWave/SmoothWave/SmoothWave.h"
#include "MasterEnums.h"
#include "Modules/Routing/UserInput/TCParameter.h"


//==============================================================================
PluginProcessor::PluginProcessor()
: AudioProcessor (BusesProperties().withOutput("Output", AudioChannelSet::stereo(), true)),
	testMod(modifierManager)
{ 
	// routingManager.createRouting(&testMod, master.gain.target->id);
  
  // routingManager.createRouting(&testMod, ModTargetId::DETUNE_SPREAD, true);
  // auto curveRoute = routingManager.createRouting(&testMod, ModTargetId::CURVE_1, true);
  // curveRoute->range.set(10);
  auto route = routingManager.routingSets[0][0];
  route->perVoice.set(true);
  route->target.set(&WaveSourceModTargets::voiceAmps);
	
//  routingManager.createRouting(modifierManager.mods[0], ModTargetId::VOICE_AMPS, true);

  // auto curveRoute = routingManager.createRouting(modifierManager.mods[2], ModTargetId::CURVE_1, true);
  // curveRoute->range.set(60);

  // auto ceilRoute = routingManager.createRouting(modifierManager.mods[2], ModTargetId::CROP_CEIL, true);
  // ceilRoute->range.set(0.5);

  // for (int s = 0; s < Globals::NUM_SOURCES; s++) {
  //   for (auto entry : RoutingManager::modTargets()) {
  //     addParameter(new TCParameter(entry.second, "S" + to_string(s+1)));
  //   }
  // }

  routingManager.parameterManager.forEachParameter([&](TCParameter* ideParam){
    addParameter(ideParam);
  });
} //clubtrees

PluginProcessor::~PluginProcessor() { }



//==============================================================================
const String PluginProcessor::getName() const                { return "Tradition Churner"; }
bool  PluginProcessor::acceptsMidi() const                   { return true; }
bool  PluginProcessor::producesMidi() const                  { return false; }
bool  PluginProcessor::isMidiEffect() const                  { return false; }
double PluginProcessor::getTailLengthSeconds() const         { return 0.0; }
int   PluginProcessor::getNumPrograms()                      { return 1; } // min(1)
int   PluginProcessor::getCurrentProgram()                   { return 0; }
void  PluginProcessor::setCurrentProgram(int index)          { }
const String PluginProcessor::getProgramName(int index)      { return {}; }
void  PluginProcessor::changeProgramName(int index, const String& newName) { }

//==============================================================================
void PluginProcessor::prepareToPlay(double sampleRate, int samplesPerBlock) {
  sourceManager.setCurrentPlaybackSampleRate(sampleRate);
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void PluginProcessor::releaseResources() {
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

bool PluginProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const {
  bool allowsMono = layouts.getMainOutputChannelSet() == AudioChannelSet::mono();
  bool allowsStereo = layouts.getMainOutputChannelSet() == AudioChannelSet::stereo();
  return allowsMono || allowsStereo;
}

void PluginProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages) {
  auto numSamples = buffer.getNumSamples();
  for (auto i = getTotalNumInputChannels(); i < getTotalNumOutputChannels(); ++i) {
    buffer.clear (i, 0, numSamples);
  }
  
  routingManager.resetModChainCaches();
  master.renderNextBlock(buffer, midiMessages, 0, numSamples);
}

//==============================================================================
bool PluginProcessor::hasEditor() const {
  return true;
}

AudioProcessorEditor* PluginProcessor::createEditor() {
  return new PluginEditor(*this);
}

//==============================================================================
void PluginProcessor::getStateInformation(MemoryBlock& destData) {
  // You should use this method to store your parameters in the memory block.
  // You could do that either as raw data, or use the XML or ValueTree classes
  // as intermediaries to make it easy to save and load complex data.
}

void PluginProcessor::setStateInformation(const void* data, int sizeInBytes) {
  // You should use this method to restore your parameters from this memory block,
  // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
  return new PluginProcessor();
}
