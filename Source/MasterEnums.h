// #pragma once
// #import <string>
// #import <vector>
// #import "Util/PP_EACH.h"

// using namespace std;

// #define PrependEnumClass(NEXT_ENUM)\
// 	ModTargetId::NEXT_ENUM,

// #define EnumAndVector(ENUM_NAME, ...)\
//   enum class ENUM_NAME { __VA_ARGS__ };\
//  	static vector<ENUM_NAME> ENUM_NAME ## List { PP_EACH(PrependEnumClass, __VA_ARGS__) ModTargetId::IGNORE };

// //vector<ENUM_NAME> ENUM_NAME ## List = {};
// //	vector<ENUM_NAME> ENUM_NAMEList { __VA_ARGS__ };

// EnumAndVector(ModTargetId,
// 	UNSET,
// 	MASTER_GAIN,
// 	MASTER_TUNE,
// 	VOICE_AMPS,
// 	SYNTH_GAIN_PRE_FX,
// 	SYNTH_GAIN_POST_FX,
// 	SYNTH_TUNE,
// 	SYNTH_PAN,
// 		UPPER_MIX,
// 		UPPER_INTERVAL,
// 		DETUNE,
// 		DETUNE_SPREAD,
// 		LOWER_WIDTH_UNITA,
// 		UPPER_WIDTH_UNITA,
// 			PULSE_WIDTH,
// 			EXPANSION_WIDTH,
// 			CURVE_1,
// 			CURVE_2,
// 	CROP_CEIL,
// 	CROP_AMP_LOSS,
// 	IGNORE
// )

// //enum class ModTargetId { MASTER_GAIN, MASTER_TUNE, VOICE_AMPS, SYNTH_GAIN_PRE_FX, SYNTH_GAIN_POST_FX, SYNTH_TUNE, SYNTH_PAN, UPPER_MIX, UPPER_INTERVAL, DETUNE, DETUNE_SPREAD, LOWER_WIDTH_UNITA, UPPER_WIDTH_UNITA, PULSE_WIDTH, EXPANSION_WIDTH, CURVE_1, CURVE_2 };

// //vector<ModTargetId> modTargetIdList = {};
// //
// //auto test = ModTargetId::MASTER_TUNE;
// //modTargetIdList.reserve(100);
// //enum class ModTargetId {
// //	FIRST,
// //
// //	// Master
// //	MASTER_GAIN,
// //	MASTER_TUNE,
// //
// //	// WaveSource
// //	VOICE_AMPS,
// //	SYNTH_GAIN_PRE_FX,
// //	SYNTH_GAIN_POST_FX,
// //	SYNTH_TUNE,
// //	SYNTH_PAN,
// //
// //		// StableWave
// //		UPPER_MIX,
// //		UPPER_INTERVAL,
// //		DETUNE,
// //		DETUNE_SPREAD,
// //		LOWER_WIDTH_UNITA,
// //		UPPER_WIDTH_UNITA,
// //
// //			// SmoothWave
// //			PULSE_WIDTH,
// //			EXPANSION_WIDTH,
// //			CURVE_1,
// //			CURVE_2,
// //
// //  LAST
// //};
// //


// struct EnumToString {
// 	static const inline string toString(ModTargetId targetId) {
// 		switch (targetId) {
// 			case ModTargetId::UNSET: return "Unset"; break;
// 			case ModTargetId::MASTER_GAIN: return "Master Gain"; break;
// 			case ModTargetId::MASTER_TUNE: return "Master Tune"; break;

// 			case ModTargetId::VOICE_AMPS: return "Voice Amps"; break;

// 			case ModTargetId::SYNTH_GAIN_PRE_FX: return "Gain (Pre FX)"; break;
// 			case ModTargetId::SYNTH_GAIN_POST_FX: return "Gain (Post FX)"; break;
// 			case ModTargetId::SYNTH_TUNE: return "Tune"; break;
// 			case ModTargetId::SYNTH_PAN: return "Pan"; break;
			
// 			case ModTargetId::UPPER_MIX: return "Upper Mix"; break;
// 			case ModTargetId::UPPER_INTERVAL: return "Upper Interval"; break;
// 			case ModTargetId::DETUNE: return "Detune"; break;
// 			case ModTargetId::DETUNE_SPREAD: return "Detune Spread"; break;
// 			case ModTargetId::LOWER_WIDTH_UNITA: return "Lower Width (unita)"; break;
// 			case ModTargetId::UPPER_WIDTH_UNITA: return "Upper Width (unita)"; break;
			
// 			case ModTargetId::PULSE_WIDTH: return "Pulse Width"; break;
// 			case ModTargetId::EXPANSION_WIDTH: return "Expansion Width"; break;
// 			case ModTargetId::CURVE_1: return "Curve 1"; break;
// 			case ModTargetId::CURVE_2: return "Curve 2"; break;
// 			case ModTargetId::CROP_CEIL: return "Crop Ceiling"; break;
// 			case ModTargetId::CROP_AMP_LOSS: return "Crop Amplitude Loss"; break;
// 			case ModTargetId::IGNORE: return "Ignore"; break;
// 		}
// 	}
// };



// enum class SourceType {
// 	SMOOTH_WAVE
// };


// enum class ModType {
// 	ADSR
// };
