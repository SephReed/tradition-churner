#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Components/ComponentStyler.h"
#include "Util/Observable.h"

using namespace ComponentStyling;

//==============================================================================
PluginEditor::PluginEditor (PluginProcessor& p) 
	: AudioProcessorEditor (&p), 
		processor (p)
{

//  setLookAndFeel(getDefaultLookAndFeel());


	auto sources = processor.sourceManager.getEditor();
	addAndMakeVisible(sources);
	sources->setBounds(0, 0, 400, 250);

  auto mods = processor.modifierManager.getEditor();
  addAndMakeVisible(mods);
  mods->setBounds(0, 250, 600, 250);
	
	
	auto master = processor.master.getEditor();
	addAndMakeVisible(master);
	master->setBounds(600, 250, 200, 250);


  setSize(800, 500);
}

PluginEditor::~PluginEditor() { }


//==============================================================================
void PluginEditor::paint (Graphics& g) {
	g.fillAll(Colour(40, 40, 40));
}

void PluginEditor::resized() {
  
}
