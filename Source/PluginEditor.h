#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

class PluginEditor 
: public AudioProcessorEditor
{
public:
	PluginEditor (PluginProcessor&);
	~PluginEditor();

	//==============================================================================
	void paint (Graphics&) override;
	void resized() override;

private:

	PluginProcessor &processor;
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginEditor)
};
